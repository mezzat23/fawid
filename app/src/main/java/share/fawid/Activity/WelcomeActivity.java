package share.fawid.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import me.relex.circleindicator.CircleIndicator;
import share.fawid.Activity.Customer.HomeActivity;
import share.fawid.Adapter.Slidesadapter;
import share.fawid.Api.APIModel;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Welcome;
import share.fawid.R;

public class WelcomeActivity extends AppCompatActivity {

    private ViewPager pager;
    private CircleIndicator indicator;
    private CardView go;
    int page = 0;
    ArrayList<Welcome.DataBean> slidersBeans = new ArrayList<>();
    Slidesadapter slidesadapter;
    private ProgressBar progress;
    private TextView txtnext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        initView();
        onclick();
        gethome();
    }

    private void initView() {
        pager = findViewById(R.id.pager);
        indicator = findViewById(R.id.indicator);
        go = findViewById(R.id.go);
        slidesadapter = new Slidesadapter(this, slidersBeans, this);
        pager.setAdapter(slidesadapter);
        indicator.setViewPager(pager);
        slidesadapter.registerDataSetObserver(indicator.getDataSetObserver());
        progress = findViewById(R.id.progress);
        LoginSession.Addwelcome(this, true);
        if (getString(R.string.lang).equals("ar")) {
            pager.setRotationY(180);
        }
        txtnext = findViewById(R.id.txtnext);
    }

    private void onclick() {
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (pager.getCurrentItem() == slidersBeans.size()-1){
                   Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                   i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                   startActivity(i);
                   finish();
               }else {
                   pager.setCurrentItem(pager.getCurrentItem()+1);
               }
            }
        });
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == slidersBeans.size()-1){
                    txtnext.setText(getString(R.string.let_s_start));
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void gethome() {
        progress.setVisibility(View.VISIBLE);
        RequestParams requestParams = new RequestParams();
        APIModel.postMethod(WelcomeActivity.this, "pages", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(WelcomeActivity.this, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        gethome();
                    }
                });
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.e("res", responseString);
                try {
                    Type dataType = new TypeToken<Welcome>() {
                    }.getType();
                    Welcome data = new Gson().fromJson(responseString, dataType);
                    slidersBeans.clear();
                    slidersBeans.addAll(data.data);
                    slidesadapter.notifyDataSetChanged();
                } catch (Exception e) {

                }

                progress.setVisibility(View.GONE);
            }
        });
    }
}
