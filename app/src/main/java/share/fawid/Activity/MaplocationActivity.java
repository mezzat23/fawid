package share.fawid.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import share.fawid.Helper.GPSTracker;
import share.fawid.R;

public class MaplocationActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveListener {
    private static final String TAG = "dd";
    public GoogleMap mMap;
    private Marker mMarcadorActual;
    private FusedLocationProviderClient mFusedLocationClient;
    String lat = "", lon = "";
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 456;
    int REQUEST_LOCATION = 123;
    private CardView save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maplocation);
        initView();
        setRequestLocation();
        onclick();
    }

    private void initView() {
        SupportMapFragment mapFragment =
                (SupportMapFragment)
                        getSupportFragmentManager().findFragmentById(R.id.workerMap);
        mapFragment.getMapAsync(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(MaplocationActivity.this);
        save = findViewById(R.id.save);
    }

    @Override
    public void onCameraIdle() {
        MarkerOptions options = new MarkerOptions()
                .position(mMap.getCameraPosition().target);
        mMap.addMarker(options);

// move the camera zoom to the location
        lat = String.valueOf(mMap.getCameraPosition().target.latitude);
        lon = String.valueOf(mMap.getCameraPosition().target.longitude);
    }

    @Override
    public void onCameraMove() {
        mMap.clear();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mMap.clear();
        lat = String.valueOf(latLng.latitude);
        lon = String.valueOf(latLng.longitude);
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title("");
        mMap.addMarker(options);

// move the camera zoom to the location
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(MaplocationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MaplocationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.clear();
        mMap.setOnMapClickListener(this);
        mMap.setMyLocationEnabled(true);
        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveListener(this);
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(MaplocationActivity.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            mMap.clear();
                            if (getIntent().hasExtra("lat")) {
                                lat = getIntent().getStringExtra("lat");
                                lon = getIntent().getStringExtra("lng");
                            } else {
                                lat = String.valueOf(location.getLatitude());
                                lon = String.valueOf(location.getLongitude());
                            }
                            LatLng latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
                            MarkerOptions options = new MarkerOptions()
                                    .position(latLng)
                                    .title("my location");
                            mMap.addMarker(options);


// move the camera zoom to the location
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));

                            // Logic to handle location object
                        }
                    }
                });
    }

    private void setRequestLocation() {
        if (ContextCompat.checkSelfPermission(MaplocationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||

                ContextCompat.checkSelfPermission(MaplocationActivity.this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED ||

                ContextCompat.checkSelfPermission(MaplocationActivity.this, Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MaplocationActivity.this,

                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,

                            Manifest.permission.ACCESS_COARSE_LOCATION,

                            Manifest.permission.BLUETOOTH,

                            Manifest.permission.BLUETOOTH_ADMIN}, REQUEST_LOCATION);

        } else {

            System.out.println("Location permissions available, starting location");


        }
    }

    @Override

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == REQUEST_LOCATION) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                System.out.println("Location permissions granted, starting location");
                GPSTracker gpsTracker = new GPSTracker(MaplocationActivity.this);
                mMap.clear();
                lat = String.valueOf(gpsTracker.getLatitude());
                lon = String.valueOf(gpsTracker.getLongitude());
                LatLng latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                MarkerOptions options = new MarkerOptions()
                        .position(latLng)
                        .title("my location");
                mMap.addMarker(options);
// move the camera zoom to the location
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
            }

        }

    }
    private void onclick(){
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra("lat", lat);
                i.putExtra("lng", lon);
                setResult(RESULT_OK, i);
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
       super.onBackPressed();
    }
}