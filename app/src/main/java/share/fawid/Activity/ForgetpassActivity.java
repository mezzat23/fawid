package share.fawid.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import share.fawid.Api.APIModel;
import share.fawid.Helper.Dialogs;
import share.fawid.R;

public class ForgetpassActivity extends AppCompatActivity {

    private CardView back;
    private EditText phone;
    private CardView reg;
    private ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetpass);
        initView();
        onclick();
    }

    private void initView() {
        back = findViewById(R.id.back);
        phone = findViewById(R.id.phone);
        reg = findViewById(R.id.reg);
        progress = findViewById(R.id.progress);
    }
    private void onclick(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phone.getText().toString().trim().equals("")){
                    phone.setError(getString(R.string.required));
                }else {
                    forgetpass();
                }
            }
        });
    }
    private void forgetpass(){
        String url = "edituser?phone=" + "966" + phone.getText().toString();
        APIModel.getMethod(ForgetpassActivity.this, url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(ForgetpassActivity.this, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        forgetpass();
                    }
                });

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.e("data",responseString);
                try {
                    JSONObject jo = new JSONObject(responseString);
                    Dialogs.showToast(jo.getString("message"),ForgetpassActivity.this);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                onBackPressed();
//                Intent i = new Intent(getApplicationContext(),VerifycodeActivity.class);
//                startActivity(i);
            }
        });
    }
}
