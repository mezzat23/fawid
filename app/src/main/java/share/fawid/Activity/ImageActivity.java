package share.fawid.Activity;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import share.fawid.R;
import uk.co.senab.photoview.PhotoViewAttacher;

public class ImageActivity extends AppCompatActivity {

    private ImageView img;
    PhotoViewAttacher pAttacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        initView();
    }

    private void initView() {
        img = (ImageView) findViewById(R.id.img);
        Picasso.get().load(getIntent().getStringExtra("img")).into(img);
        pAttacher = new PhotoViewAttacher(img);
        pAttacher.update();
    }
}
