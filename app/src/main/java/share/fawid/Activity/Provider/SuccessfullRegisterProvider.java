package share.fawid.Activity.Provider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import share.fawid.Helper.LoginSession;
import share.fawid.R;

public class SuccessfullRegisterProvider extends AppCompatActivity {

    private ImageView img;
    private CardView reg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_successfull_register_provider);
        initView();
        onclick();
    }

    private void initView() {
        img = findViewById(R.id.img);
        reg = findViewById(R.id.reg);
        new Thread(){
            @Override
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    if (LoginSession.setdata(SuccessfullRegisterProvider.this).data.get(0).type==1){
                        Intent i = new Intent(SuccessfullRegisterProvider.this, HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }else {
                        Intent i = new Intent(SuccessfullRegisterProvider.this, share.fawid.Activity.Customer.HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }
                }
            }
        }.start();
    }
    private void onclick(){
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginSession.setdata(SuccessfullRegisterProvider.this).data.get(0).type==1){
                    Intent i = new Intent(SuccessfullRegisterProvider.this, HomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                }else {
                    Intent i = new Intent(SuccessfullRegisterProvider.this, share.fawid.Activity.Customer.HomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                }
            }
        });
    }
}
