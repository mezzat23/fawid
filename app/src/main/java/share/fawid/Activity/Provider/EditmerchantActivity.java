package share.fawid.Activity.Provider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.material.tabs.TabLayout;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

import share.fawid.Adapter.ViewPagerAdapter;
import share.fawid.Fragment.Provider.InfoFragment;
import share.fawid.Fragment.Provider.LinksFragment;
import share.fawid.Fragment.Provider.ServicesFragment;
import share.fawid.Helper.camera.Camera;
import share.fawid.Models.Addimage;
import share.fawid.R;

import static share.fawid.Helper.camera.Camera.getRealPathFromURI;
import static share.fawid.Helper.camera.Camera.myBitmap;

public class EditmerchantActivity extends AppCompatActivity {

    private CardView back;
    private TabLayout tabs;
    private ViewPager pager;
    ViewPagerAdapter adapter ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editmerchant);
        initView();
        onclick();
    }

    private void initView() {
        back = findViewById(R.id.back);
        tabs = findViewById(R.id.tabs);
        pager = findViewById(R.id.pager);
        setupViewPager(pager);
        tabs.post(new Runnable() {
            @Override
            public void run() {
                tabs.setupWithViewPager(pager);
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new InfoFragment(), getString(R.string.info));
        adapter.addFragment(new ServicesFragment(), getString(R.string.services));
        adapter.addFragment(new LinksFragment(), getString(R.string.links));
        viewPager.setAdapter(adapter);
    }
    private void onclick(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    @SuppressLint("MissingSuperCall")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        InfoFragment fragment = (InfoFragment) adapter.getItem(0);

        // if the result is capturing Image
        if (requestCode == Camera.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
//-----------------------------------------------------------------------------------------------------------------------------------
                myBitmap = (Bitmap) data.getExtras().get("data");
                Uri tempUri = Camera.getImageUri(EditmerchantActivity.this, myBitmap);
//                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File finalFile = new File(getRealPathFromURI(tempUri));
//                myBitmap = BitmapFactory.decodeFile(sourceFile.getAbsolutePath());
                if (fragment.imgs.size() >= 1) {
                    Collections.reverse(fragment.imgs);
                    fragment.imgs.add(String.valueOf(finalFile));
                    Collections.reverse(fragment.imgs);
                } else {
                    fragment.imgs.add(String.valueOf(finalFile));
                    Collections.reverse(fragment.imgs);
                }
                if (fragment.bitmap.size() >= 2) {
                    Addimage addimage = new Addimage();
                    addimage.bitmap = myBitmap;
                    addimage.check = true;
                    Collections.reverse(fragment.bitmap);
                    fragment.bitmap.add(addimage);
                    Collections.reverse(fragment.bitmap);
                } else {
                    Addimage addimage = new Addimage();
                    addimage.bitmap = myBitmap;
                    addimage.check = true;
                    fragment.bitmap.add(addimage);
                    Collections.reverse(fragment.bitmap);
                }
                fragment.imageaddadapter.notifyDataSetChanged();
            } else if (resultCode == RESULT_CANCELED) {

                // user cancelled Image capture
                Toast.makeText(EditmerchantActivity.this,
                        R.string.camera_closed, Toast.LENGTH_SHORT)
                        .show();

            } else {
                // failed to capture image
                Toast.makeText(EditmerchantActivity.this,
                        R.string.failed_to_open_camera, Toast.LENGTH_SHORT)
                        .show();
            }

        } else if (requestCode == Camera.PICK_PHOTO_FOR_AVATAR && resultCode == RESULT_OK) {
            if (data == null) {
                Toast.makeText(EditmerchantActivity.this,
                        " Failed to select picture", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            Uri selectedImageUri = data.getData();
            try {
                myBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            File finalFile = new File(getRealPathFromURI(selectedImageUri));
            if (fragment.imgs.size() >= 1) {
                Collections.reverse(fragment.imgs);
                fragment.imgs.add(String.valueOf(finalFile));
                Collections.reverse(fragment.imgs);
            } else {
                fragment.imgs.add(String.valueOf(finalFile));
                Collections.reverse(fragment.imgs);
            }
            if (fragment.bitmap.size() >= 2) {
                Addimage addimage = new Addimage();
                addimage.bitmap = myBitmap;
                addimage.check = true;
                Collections.reverse(fragment.bitmap);
                fragment.bitmap.add(addimage);
                Collections.reverse(fragment.bitmap);
            } else {
                Addimage addimage = new Addimage();
                addimage.bitmap = myBitmap;
                addimage.check = true;
                fragment.bitmap.add(addimage);
                Collections.reverse(fragment.bitmap);
            }
            fragment.imageaddadapter.notifyDataSetChanged();
            //Setting the Bitmap to ImageView

        } else if (requestCode == 458 && resultCode == RESULT_OK) {
            Place place = Autocomplete.getPlaceFromIntent(data);
            fragment.lat = String.valueOf(place.getLatLng().latitude);
            fragment.lng = String.valueOf(place.getLatLng().longitude);
        }else if (requestCode == 111 && resultCode == RESULT_OK) {
            fragment.lat = data.getStringExtra("lat");
            fragment.lng = data.getStringExtra("lng");
        }

        try {


        } catch (OutOfMemoryError a) {
            a.printStackTrace();
        } catch (NullPointerException a) {
            a.printStackTrace();
        } catch (RuntimeException a) {
            a.printStackTrace();
        }


    }
}
