package share.fawid.Activity.Provider;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import share.fawid.Activity.TermsActivity;
import share.fawid.Adapter.Cityadapter;
import share.fawid.Api.APIModel;
import share.fawid.Helper.Dialogs;
import share.fawid.Helper.camera.Camera;
import share.fawid.Models.City;
import share.fawid.R;

import static share.fawid.Helper.camera.Camera.getRealPathFromURI;
import static share.fawid.Helper.camera.Camera.myBitmap;

public class RegisterproviderActivity extends AppCompatActivity {

    private CardView back;
    private EditText fullname;
    private EditText phone;
    private LinearLayout city;
    private EditText password;
    private LinearLayout upload;
    private ImageView img;
    private CardView reg;
    private ProgressBar progress;
    private TextView txtcity;
    private EditText edit;
    private RecyclerView list;
    private CardView save;
    Dialog dialog;
    Cityadapter cityadapter;
    ArrayList<City.DataBean> cities = new ArrayList<>();
    ArrayList<City.DataBean> cities1 = new ArrayList<>();
    public int city_id = 0;
    ArrayList<String> city_ids = new ArrayList<>();
    String imgstring = "";
    private CheckBox check;
    private TextView terms;
    boolean is_check = false;
    int x = 0;
    private ImageView imgpass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registerprovider);
        initView();
        onclick();
    }

    private void initView() {
        back = findViewById(R.id.back);
        fullname = findViewById(R.id.fullname);
        phone = findViewById(R.id.phone);
        city = findViewById(R.id.city);
        password = findViewById(R.id.password);
        upload = findViewById(R.id.upload);
        img = findViewById(R.id.img);
        reg = findViewById(R.id.reg);
        progress = findViewById(R.id.progress);
        txtcity = findViewById(R.id.txtcity);
        Camera.activity = this;
        if (getString(R.string.lang).equals("ar")) {
            fullname.setGravity(Gravity.CENTER | Gravity.RIGHT);
            phone.setGravity(Gravity.CENTER | Gravity.RIGHT);
            password.setGravity(Gravity.CENTER | Gravity.RIGHT);
        }
        check = findViewById(R.id.check);
        terms = findViewById(R.id.terms);
        imgpass = findViewById(R.id.imgpass);
    }

    private void onclick() {
        imgpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (x == 0){
                    password.setTransformationMethod(null);
                    imgpass.setBackgroundResource(R.drawable.ic_eye);
                    x = 1 ;
                }else {
                    x = 0 ;
                    password.setTransformationMethod(new PasswordTransformationMethod());
                    imgpass.setBackgroundResource(R.drawable.ic_eye1);
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Camera.cameraOperation();
            }
        });
        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getcity();
            }
        });
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterproviderActivity.this, TermsActivity.class);
                startActivity(i);
            }
        });
        check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                is_check = isChecked;
            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean is_good = true;

                if (fullname.getText().toString().trim().equals("")) {
                    fullname.setError(getString(R.string.required));
                    is_good = false;
                }
                if (phone.getText().toString().trim().equals("")) {
                    phone.setError(getString(R.string.required));
                    is_good = false;
                }
                if (is_check == false) {
                    Dialogs.showToast(getString(R.string.terms_agree), RegisterproviderActivity.this);
                    is_good = false;
                }
                if (phone.getText().length() < 9) {
                    phone.setError(getString(R.string.number_9digits));
                    is_good = false;
                }
                if (phone.getText().length() > 10) {
                    phone.setError(getString(R.string.number_9digits));
                    is_good = false;
                }
                if (phone.getText().length() == 10 && !phone.getText().toString().substring(0, 1).equals("0")) {
                    phone.setError(getString(R.string.number_9digits));
                    is_good = false;
                }
                if (password.getText().toString().trim().equals("")) {
                    password.setError(getString(R.string.required));
                    is_good = false;
                }
                if (city_ids.size() == 0) {
                    Dialogs.showToast(getString(R.string.select_city), RegisterproviderActivity.this);
                    is_good = false;
                }
                if (is_good) {
                    reg(RegisterproviderActivity.this);
                }
            }
        });
    }

    private void getcity() {
        APIModel.getMethod(RegisterproviderActivity.this, "cites", new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<City>() {
                }.getType();
                City data = new Gson().fromJson(responseString, dataType);
                cities.clear();
                cities1.clear();
                cities.addAll(data.data);
                cities1.addAll(data.data);
                showdialogcity();

            }
        });
    }

    private void showdialogcity() {
        dialog = new Dialog(RegisterproviderActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_city);
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int width = display.getWidth();
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        back = dialog.findViewById(R.id.back);
        list = dialog.findViewById(R.id.list);
        save = dialog.findViewById(R.id.save);
        edit = dialog.findViewById(R.id.edit);
        LinearLayoutManager layoutManage1 = new LinearLayoutManager(RegisterproviderActivity.this);
        list.setLayoutManager(layoutManage1);
        cityadapter = new Cityadapter(cities, this, 1);
        list.setAdapter(cityadapter);
        if (city_id > 0) {
            for (int i = 0; i < cities.size(); i++) {
                if (city_id == Integer.parseInt(cities.get(i).id)) {
                    cityadapter.pos = i;
                }
            }
        }
        cityadapter.notifyDataSetChanged();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit.getText().toString().equals("")) {
                    cities.clear();
                    cities.addAll(cities1);
                    cityadapter.notifyDataSetChanged();
                } else {
                    cities.clear();
                    for (int i = 0; i < cities1.size(); i++) {
                        if (cities1.get(i).name.contains(edit.getText().toString())) {
                            cities.add(cities1.get(i));
                        }
                    }
                    cityadapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                city_ids.clear();
                String x = "";
                for (int i = 0; i < cities.size(); i++) {
                    if (cities.get(i).check) {
                        if (city_ids.size() == 0) {
                            x = cities.get(i).name;
                        } else {
                            x = x + " , " + cities.get(i).name;
                        }
                        city_ids.add(cities.get(i).id);
                    }
                }
                if (city_ids.size() > 0) {
                    txtcity.setText(x);
                }
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void reg(final Context context) {
        progress.setVisibility(View.VISIBLE);
        RequestParams requestParams = new RequestParams();
        requestParams.put("phone", "966" + phone.getText().toString());
        Log.e("dd", requestParams.toString());
        APIModel.postMethod((Activity) context, "pcheck", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(context, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        reg(context);
                    }
                });

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Intent i = new Intent(RegisterproviderActivity.this, Registerprovider1Activity.class);
                i.putExtra("name", fullname.getText().toString());
                i.putExtra("pass", password.getText().toString());
                i.putExtra("city", city_ids);
                i.putExtra("phone", "966" + phone.getText().toString());
                i.putExtra("img", imgstring);
                startActivity(i);


                progress.setVisibility(View.GONE);
            }
        });
    }

    @SuppressLint("MissingSuperCall")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // if the result is capturing Image
        if (requestCode == Camera.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
//-----------------------------------------------------------------------------------------------------------------------------------
                myBitmap = (Bitmap) data.getExtras().get("data");
                Uri tempUri = Camera.getImageUri(RegisterproviderActivity.this, myBitmap);
//                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File finalFile = new File(getRealPathFromURI(tempUri));
//                myBitmap = BitmapFactory.decodeFile(sourceFile.getAbsolutePath());
                imgstring = String.valueOf(finalFile);
                img.setImageBitmap(myBitmap);
            } else if (resultCode == RESULT_CANCELED) {

                // user cancelled Image capture
                Toast.makeText(RegisterproviderActivity.this,
                        R.string.camera_closed, Toast.LENGTH_SHORT)
                        .show();

            } else {
                // failed to capture image
                Toast.makeText(RegisterproviderActivity.this,
                        R.string.failed_to_open_camera, Toast.LENGTH_SHORT)
                        .show();
            }

        } else if (requestCode == Camera.PICK_PHOTO_FOR_AVATAR && resultCode == RESULT_OK) {
            if (data == null) {
                Toast.makeText(RegisterproviderActivity.this,
                        " Failed to select picture", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            Uri selectedImageUri = data.getData();
            try {
                myBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            File finalFile = new File(getRealPathFromURI(selectedImageUri));
            imgstring = String.valueOf(finalFile);
            //Setting the Bitmap to ImageView

        }

        try {

            img.setImageBitmap(myBitmap);

        } catch (OutOfMemoryError a) {
            a.printStackTrace();
        } catch (NullPointerException a) {
            a.printStackTrace();
        } catch (RuntimeException a) {
            a.printStackTrace();
        }


    }
}
