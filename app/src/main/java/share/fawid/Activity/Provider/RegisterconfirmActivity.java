package share.fawid.Activity.Provider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

import cz.msebera.android.httpclient.Header;
import share.fawid.Api.APIModel;
import share.fawid.Helper.Dialogs;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Login;
import share.fawid.R;

import static share.fawid.Helper.LoginSession.loginFile;

public class RegisterconfirmActivity extends AppCompatActivity {

    private CardView back;
    private EditText num6;
    private EditText num5;
    private EditText num4;
    private EditText num3;
    private EditText num2;
    private EditText num1;
    private TextView resend;
    private CardView reg;
    private ProgressBar progress;
    private TextView num;
    private TextView type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registerconfirm);
        initView();
        onclick();
    }

    private void initView() {
        back = findViewById(R.id.back);
        num6 = findViewById(R.id.num6);
        num5 = findViewById(R.id.num5);
        num4 = findViewById(R.id.num4);
        num3 = findViewById(R.id.num3);
        num2 = findViewById(R.id.num2);
        num1 = findViewById(R.id.num1);
        resend = findViewById(R.id.resend);
        reg = findViewById(R.id.reg);
        progress = findViewById(R.id.progress);
        num = findViewById(R.id.num);
        num1.requestFocus();
        num.setText(getString(R.string.verify_your_account_by_typing_the_code_n_sent_to) + " " + LoginSession.setdata(this).data.get(0).phone);
        type = findViewById(R.id.type);
        if (LoginSession.setdata(this).data.get(0).type == 1){
            type.setText(getString(R.string.service_provider));
        }else {
            type.setText(getString(R.string.user));

        }
    }

    private void onclick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        num2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL) {

                    Log.e("50505050", "onKey: ");
                    if (num2.getText().toString().equals("")) {
                        num1.requestFocus();
                    }
                }
                return false;
            }
        });
        num3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_

                if (keyCode == KeyEvent.KEYCODE_DEL) {

                    Log.e("50505050", "onKey: ");

                    if (num3.getText().toString().equals("")) {
                        num2.requestFocus();
                    }
                }
                return false;
            }
        });

        num4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_

                if (keyCode == KeyEvent.KEYCODE_DEL) {

                    Log.e("50505050", "onKey: ");

                    if (num4.getText().toString().equals("")) {
                        num3.requestFocus();
                    }
                }
                return false;
            }
        });


        num5.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_

                if (keyCode == KeyEvent.KEYCODE_DEL) {

                    Log.e("50505050", "onKey: ");

                    if (num5.getText().toString().equals("")) {
                        num4.requestFocus();
                    }
                }
                return false;
            }
        });

        num6.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_

                if (keyCode == KeyEvent.KEYCODE_DEL) {

                    if (num6.getText().toString().equals("")) {
                        num5.requestFocus();
                    }
                }
                return false;
            }
        });
        num1.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 == 1) {
                    num2.requestFocus();
                    //        firstEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else if (i2 == 0) {
                    num1.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
        });
        num2.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence charSequence, int i, int i1, final int i2) {
                if (i2 == 1) {
                    num3.requestFocus();
                    //        firstEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else if (i2 == 0) {
                    num1.requestFocus();
                }

                num2.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                        if (keyCode == KeyEvent.KEYCODE_DEL) {

                            Log.e("50505050", "onKey: ");
                            if (i2 == 0) {
                                num1.requestFocus();
                            }
                        }
                        return false;
                    }
                });


            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
        });
        num3.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence charSequence, int i, int i1, final int i2) {
                if (i2 == 1) {
                    num4.requestFocus();
                    //        firstEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else if (i2 == 0) {
                    num2.requestFocus();
                }

                num3.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                        if (keyCode == KeyEvent.KEYCODE_DEL) {

                            Log.e("50505050", "onKey: ");

                            if (i2 == 0) {
                                num2.requestFocus();
                            }
                        }
                        return false;
                    }
                });

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
        });
        num4.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 == 1) {
                    num5.requestFocus();
                    //        firstEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else if (i2 == 0) {
                    num3.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
        });

        num5.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 == 1) {
                    num6.requestFocus();
                    //        firstEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else if (i2 == 0) {
                    num4.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
        });
        num6.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 == 1) {

                    //        firstEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean is_good = true;
                if (num1.getText().toString().trim().equals("")) {
                    num1.setError(getString(R.string.required));
                    is_good = false;
                }
                if (num2.getText().toString().trim().equals("")) {
                    num2.setError(getString(R.string.required));
                    is_good = false;
                }
                if (num3.getText().toString().trim().equals("")) {
                    num3.setError(getString(R.string.required));
                    is_good = false;
                }
                if (num4.getText().toString().trim().equals("")) {
                    num4.setError(getString(R.string.required));
                    is_good = false;
                }
                if (num5.getText().toString().trim().equals("")) {
                    num5.setError(getString(R.string.required));
                    is_good = false;
                }
                if (num6.getText().toString().trim().equals("")) {
                    num6.setError(getString(R.string.required));
                    is_good = false;
                }
                if (is_good) {
                    reg(RegisterconfirmActivity.this);
                }
            }
        });
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resend(RegisterconfirmActivity.this);
            }
        });
    }

    private void reg(final Context context) {
        progress.setVisibility(View.VISIBLE);
        RequestParams requestParams = new RequestParams();
        String x = num1.getText().toString() + num2.getText().toString() + num3.getText().toString() + num4.getText().toString() + num5.getText().toString() + num6.getText().toString();
        Log.e("dd", requestParams.toString());
        APIModel.getMethod((Activity) context, "providers?id=" + LoginSession.setdata(RegisterconfirmActivity.this).data.get(0).id + "&code=" + x, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(context, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        reg(context);
                    }
                });

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                loginFile = context.getSharedPreferences("LOGIN_FILE", Context.MODE_PRIVATE);
                String x = loginFile.getString("json", "");
                Login data = null;
                Type dataType = new TypeToken<Login>() {
                }.getType();
                data = new Gson().fromJson(x, dataType);
                data.data.get(0).active_staus = "yes";
                SharedPreferences.Editor editor = loginFile.edit();
                editor.putString("json", new Gson().toJson(data));
                editor.putInt("type", 2);
                editor.apply();
                LoginSession.setdata(context);
                Intent i = new Intent(RegisterconfirmActivity.this, SuccessfullRegisterProvider.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                progress.setVisibility(View.GONE);
            }
        });
    }

    private void resend(final Context context) {
        progress.setVisibility(View.VISIBLE);
        RequestParams requestParams = new RequestParams();

        APIModel.getMethod((Activity) context, "resendcode/" + LoginSession.setdata(RegisterconfirmActivity.this).data.get(0).id, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(context, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        resend(context);
                    }
                });

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.e("res",responseString);
                try {
                    JSONObject jo = new JSONObject(responseString);
                    Dialogs.showToast(jo.getString("message"), RegisterconfirmActivity.this);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progress.setVisibility(View.GONE);
            }
        });
    }
}
