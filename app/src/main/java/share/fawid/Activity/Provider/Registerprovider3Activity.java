package share.fawid.Activity.Provider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;
import share.fawid.Api.APIModel;
import share.fawid.Helper.Dialogs;
import share.fawid.Helper.LoginSession;
import share.fawid.R;

import static share.fawid.Helper.LoginSession.loginFile;

public class Registerprovider3Activity extends AppCompatActivity {

    private CardView back;
    private EditText facebook;
    private EditText Twitter;
    private EditText Snapchat;
    private EditText Instagram;
    private EditText Linkedin;
    private EditText youtube;
    private EditText website;
    private CardView reg;
    private ProgressBar progress;
    private EditText whatsapp;
    private EditText skype;
    private EditText tambler;
    private EditText phone;
    int click = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registerprovider3);
        initView();
        onclick();
    }

    private void initView() {
        back = findViewById(R.id.back);
        facebook = findViewById(R.id.facebook);
        Twitter = findViewById(R.id.Twitter);
        Snapchat = findViewById(R.id.Snapchat);
        Instagram = findViewById(R.id.Instagram);
        Linkedin = findViewById(R.id.Linkedin);
        youtube = findViewById(R.id.youtube);
        website = findViewById(R.id.website);
        reg = findViewById(R.id.reg);
        progress = findViewById(R.id.progress);
        whatsapp = findViewById(R.id.whatsapp);
        skype = findViewById(R.id.skype);
        tambler = findViewById(R.id.tambler);
        phone = findViewById(R.id.phone);
        if (getString(R.string.lang).equals("ar")){
            facebook.setGravity(Gravity.CENTER | Gravity.RIGHT);
            Twitter.setGravity(Gravity.CENTER | Gravity.RIGHT);
            Snapchat.setGravity(Gravity.CENTER | Gravity.RIGHT);
            Instagram.setGravity(Gravity.CENTER | Gravity.RIGHT);
            Linkedin.setGravity(Gravity.CENTER | Gravity.RIGHT);
            youtube.setGravity(Gravity.CENTER | Gravity.RIGHT);
            website.setGravity(Gravity.CENTER | Gravity.RIGHT);
            whatsapp.setGravity(Gravity.CENTER | Gravity.RIGHT);
            skype.setGravity(Gravity.CENTER | Gravity.RIGHT);
            tambler.setGravity(Gravity.CENTER | Gravity.RIGHT);
            phone.setGravity(Gravity.CENTER | Gravity.RIGHT);
        }
    }

    private void onclick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!facebook.getText().toString().trim().equals("")){
                   click++;
                }
                if (!Twitter.getText().toString().trim().equals("")){
                    click++;
                }
                if (!Snapchat.getText().toString().trim().equals("")){
                    click++;
                }
                if (!Instagram.getText().toString().trim().equals("")){
                    click++;
                }
                if (!Linkedin.getText().toString().trim().equals("")){
                    click++;
                }
                if (!youtube.getText().toString().trim().equals("")){
                    click++;
                }
                if (!website.getText().toString().trim().equals("")){
                    click++;
                }
                if (!whatsapp.getText().toString().trim().equals("")){
                    click++;
                }
                if (!skype.getText().toString().trim().equals("")){
                    click++;
                }
                if (!tambler.getText().toString().trim().equals("")){
                    click++;
                }
                if (!phone.getText().toString().trim().equals("")){
                    click++;
                }
                if (click >= 3){
                    reg(Registerprovider3Activity.this);
                }else {
                    Dialogs.showToast(getString(R.string.add_3contacts),Registerprovider3Activity.this);
                }
            }
        });
    }

    private void reg(final Context context) {
        progress.setVisibility(View.VISIBLE);
        RequestParams requestParams = new RequestParams();
        requestParams.put("full_name", getIntent().getStringExtra("name"));
        requestParams.put("password", getIntent().getStringExtra("pass"));
        requestParams.put("city_id", getIntent().getStringArrayListExtra("city"));
        requestParams.put("phone", getIntent().getStringExtra("phone"));
        try {
            requestParams.put("image", new File(getIntent().getStringExtra("img")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (!getIntent().getStringExtra("des").equals("")) {
            requestParams.put("desc", getIntent().getStringExtra("des"));
        }
        if (!getIntent().getStringExtra("lat").equals("")) {
            requestParams.put("map_lat", getIntent().getStringExtra("lat"));
        }
        if (!getIntent().getStringExtra("lng").equals("")) {
            requestParams.put("map_lng", getIntent().getStringExtra("lng"));
        }
        for (int i = 0; i < getIntent().getStringArrayListExtra("imgs").size(); i++) {
            try {
                requestParams.put("image_file[" + i + "]", new File(getIntent().getStringArrayListExtra("imgs").get(i)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
        if (getIntent().getStringArrayListExtra("cats").size() > 0) {
            requestParams.put("cat_id", getIntent().getStringArrayListExtra("cats"));
        }
        if (getIntent().getStringArrayListExtra("subcats").size() > 0) {
            requestParams.put("sub_cat_id", getIntent().getStringArrayListExtra("subcats"));
        }
        if (!facebook.getText().toString().trim().equals("")) {
            requestParams.put("facebook", facebook.getText().toString());
        }
        if (!Twitter.getText().toString().trim().equals("")) {
            requestParams.put("twitter", Twitter.getText().toString());
        }
        if (!Linkedin.getText().toString().trim().equals("")) {
            requestParams.put("linkedin", Linkedin.getText().toString());
        }
        if (!youtube.getText().toString().trim().equals("")) {
            requestParams.put("youtube", youtube.getText().toString());
        }
        if (!website.getText().toString().trim().equals("")) {
            requestParams.put("website", website.getText().toString());
        }
        if (!Snapchat.getText().toString().trim().equals("")) {
            requestParams.put("snapchat", Snapchat.getText().toString());
        }
        if (!Instagram.getText().toString().trim().equals("")) {
            requestParams.put("insatgram", Instagram.getText().toString());
        }
        if (!skype.getText().toString().trim().equals("")) {
            requestParams.put("skype", skype.getText().toString());
        }
        if (!whatsapp.getText().toString().trim().equals("")) {
            requestParams.put("whatsapp", whatsapp.getText().toString());
        }
        if (!tambler.getText().toString().trim().equals("")) {
            requestParams.put("tambler", tambler.getText().toString());
        }
        if (!phone.getText().toString().trim().equals("")) {
            requestParams.put("moble", phone.getText().toString());
        }
        if (!getIntent().getStringExtra("address").equals("")) {
            requestParams.put("address", getIntent().getStringExtra("address"));
        }
        if (!getIntent().getStringExtra("address2").equals("")) {
            requestParams.put("address2", getIntent().getStringExtra("address2"));
        }
        if (!getIntent().getStringExtra("nearstaddress").equals("")) {
            requestParams.put("nearest_place", getIntent().getStringExtra("nearstaddress"));
        }
        if (!getIntent().getStringExtra("type").equals("")) {
            requestParams.put("member_type", getIntent().getStringExtra("type"));
        }
        requestParams.put("platform", "2");
        requestParams.put("registrationid", FirebaseInstanceId.getInstance().getToken());
        Log.e("dd", requestParams.toString());
        APIModel.postMethod((Activity) context, "providers", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(context, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        reg(context);
                    }
                });

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                loginFile = context.getSharedPreferences("LOGIN_FILE", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = loginFile.edit();
                editor.putString("json", responseString);
                editor.putInt("type", 2);
                editor.apply();
                LoginSession.setdata(context);
                Intent i = new Intent(Registerprovider3Activity.this, RegisterconfirmActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                progress.setVisibility(View.GONE);
            }
        });
    }

}
