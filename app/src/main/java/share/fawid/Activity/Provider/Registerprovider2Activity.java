package share.fawid.Activity.Provider;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import share.fawid.Adapter.Jobadapter;
import share.fawid.Api.APIModel;
import share.fawid.Models.Catogaries;
import share.fawid.R;

public class Registerprovider2Activity extends AppCompatActivity {

    private CardView back;
    private EditText edit;
    private RecyclerView list;
    private CardView reg;
    private ProgressBar progress;
    Jobadapter jobadapter;
    ArrayList<Catogaries.DataBean> catogaries = new ArrayList<>();
    ArrayList<Catogaries.DataBean> catogaries1 = new ArrayList<>();
    ArrayList<String> cats = new ArrayList<>();
    ArrayList<String> subcats = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registerprovider2);
        initView();
        onclick();
        getcatogary();
    }

    private void initView() {
        back = findViewById(R.id.back);
        edit = findViewById(R.id.edit);
        list = findViewById(R.id.list);
        reg = findViewById(R.id.reg);
        progress = findViewById(R.id.progress);
        LinearLayoutManager layoutManage1 = new LinearLayoutManager(Registerprovider2Activity.this);
        list.setLayoutManager(layoutManage1);
        jobadapter = new Jobadapter(catogaries);
        list.setAdapter(jobadapter);
    }

    private void onclick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit.getText().toString().equals("")) {
                    catogaries.clear();
                    catogaries.addAll(catogaries1);
                    jobadapter.notifyDataSetChanged();
                } else {
                    catogaries.clear();
                    for (int i = 0; i < catogaries1.size(); i++) {
                        if (catogaries1.get(i).title.contains(edit.getText().toString())) {
                            catogaries.add(catogaries1.get(i));
                        }
                    }
                    jobadapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < catogaries.size(); i++) {
//                    if (catogaries.get(i).sub_cats.size() > 0){
                    if (catogaries.get(i).check) {
                        cats.add(catogaries.get(i).id);
                    }
                    for (int j = 0; j < catogaries.get(i).sub_cats.size(); j++) {
                        if (catogaries.get(i).sub_cats.get(j).check == true) {
                            subcats.add(catogaries.get(i).sub_cats.get(j).id);
                        }
                    }
                    for (int j = 0; j < catogaries.get(i).sub_cats.size(); j++) {
                        for (int ii = 0; ii < cats.size(); ii++) {
                            if (catogaries.get(i).id.equals(cats.get(ii))) {
                                break;
                            }
                            if (ii == cats.size() - 1) {
                                if (catogaries.get(i).sub_cats.get(j).check == true) {
                                    cats.add(catogaries.get(i).id);
                                    break;
                                }
                            }
                        }

                    }

//                    }
//                    else {
//
//                    }
                }
                Intent i = new Intent(Registerprovider2Activity.this, Registerprovider3Activity.class);
                i.putExtra("name",getIntent().getStringExtra("name"));
                i.putExtra("pass",getIntent().getStringExtra("pass"));
                i.putExtra("city",getIntent().getStringArrayListExtra("city"));
                i.putExtra("phone",getIntent().getStringExtra("phone"));
                i.putExtra("img",getIntent().getStringExtra("img"));
                i.putExtra("des",getIntent().getStringExtra("des"));
                i.putExtra("lat",getIntent().getStringExtra("lat"));
                i.putExtra("lng",getIntent().getStringExtra("lng"));
                i.putExtra("address",getIntent().getStringExtra("address"));
                i.putExtra("address2",getIntent().getStringExtra("address2"));
                i.putExtra("nearstaddress",getIntent().getStringExtra("nearstaddress"));
                i.putExtra("type",getIntent().getStringExtra("type"));
                i.putExtra("imgs",getIntent().getStringArrayListExtra("imgs"));
                i.putExtra("cats",cats);
                i.putExtra("subcats",subcats);
                startActivity(i);
            }
        });
    }

    private void getcatogary() {
        APIModel.getMethod(Registerprovider2Activity.this, "categories", new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Catogaries>() {
                }.getType();
                Catogaries data = new Gson().fromJson(responseString, dataType);
                catogaries.clear();
                catogaries1.clear();
                catogaries.addAll(data.data);
                catogaries1.addAll(data.data);
                jobadapter.notifyDataSetChanged();
            }
        });
    }

}
