package share.fawid.Activity.Provider;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;

import cz.msebera.android.httpclient.Header;
import share.fawid.Api.APIModel;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Clicks;
import share.fawid.R;

public class StatisticsActivity extends AppCompatActivity {

    private CardView back;
    private LinearLayout lin;
    private TextView call;
    private TextView whatsapp;
    private TextView instagram;
    private TextView skype;
    private TextView linkedin;
    private TextView twitter;
    private TextView facebook;
    private TextView tambler;
    private TextView snapchat;
    private TextView website;
    private TextView youtube;
    private TextView email;
    private ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        initView();
        getdata();
        onclick();
    }

    private void initView() {
        back = findViewById(R.id.back);
        lin = findViewById(R.id.lin);
        call = findViewById(R.id.call);
        whatsapp = findViewById(R.id.whatsapp);
        instagram = findViewById(R.id.instagram);
        skype = findViewById(R.id.skype);
        linkedin = findViewById(R.id.linkedin);
        twitter = findViewById(R.id.twitter);
        facebook = findViewById(R.id.facebook);
        tambler = findViewById(R.id.tambler);
        snapchat = findViewById(R.id.snapchat);
        website = findViewById(R.id.website);
        youtube = findViewById(R.id.youtube);
        email = findViewById(R.id.email);
        progress = findViewById(R.id.progress);
    }
    private void getdata() {
        String url = "clicks/" + LoginSession.setdata(this).data.get(0).id;
        APIModel.getMethod(StatisticsActivity.this, url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
                lin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Clicks>() {
                }.getType();
                Clicks data = new Gson().fromJson(responseString, dataType);
                call.setText(data.data.get(0).mobile_clicks);
                whatsapp.setText(data.data.get(0).whatsapp_clicks);
                instagram.setText(data.data.get(0).insatgram_clicks);
                skype.setText(data.data.get(0).skype_clicks);
                snapchat.setText(data.data.get(0).snapchat_clicks);
                twitter.setText(data.data.get(0).twitter_clicks);
                tambler.setText(data.data.get(0).tambler_clicks);
                facebook.setText(data.data.get(0).facebook_clicks);
                email.setText(data.data.get(0).email_clicks);
                youtube.setText(data.data.get(0).youtube_clicks);
                linkedin.setText(data.data.get(0).linkedin_clicks);
                website.setText(data.data.get(0).website_clicks);
            }
        });

    }
    private void onclick(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}