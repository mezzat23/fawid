package share.fawid.Activity.Provider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import share.fawid.Activity.MaplocationActivity;
import share.fawid.Adapter.Imageaddadapter;
import share.fawid.Helper.camera.Camera;
import share.fawid.Models.Addimage;
import share.fawid.R;

import static share.fawid.Helper.camera.Camera.getRealPathFromURI;
import static share.fawid.Helper.camera.Camera.myBitmap;

public class Registerprovider1Activity extends AppCompatActivity {

    private CardView back;
    private EditText des;
    private RecyclerView list;
    private CardView reg;
    private ProgressBar progress;
    public ArrayList<Addimage> bitmap = new ArrayList<>();
    public ArrayList<String> imgs = new ArrayList<>();
    Imageaddadapter imageaddadapter;
    private LinearLayout map;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 458;
    String lat = "", lng = "";
    private EditText address;
    private EditText address2;
    private EditText nearstaddress;
    private RadioGroup groub;
    private RadioButton individual;
    private RadioButton company;
    String type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registerprovider1);
        initView();
        onclick();
    }

    private void initView() {
        Places.initialize(this, "AIzaSyAgRHIRXw8irYIvIP6aIoGdyDzmQFAD4bo");
        back = findViewById(R.id.back);
        des = findViewById(R.id.des);
        list = findViewById(R.id.list);
        reg = findViewById(R.id.reg);
        progress = findViewById(R.id.progress);
        Camera.activity = this;
        bitmap.clear();
        Addimage addimage = new Addimage();
        addimage.check = false;
        bitmap.add(addimage);
        LinearLayoutManager layoutManage1 = new GridLayoutManager(Registerprovider1Activity.this, 5);
        layoutManage1.setReverseLayout(true);
        list.setLayoutManager(layoutManage1);
        imageaddadapter = new Imageaddadapter(bitmap, this);
        list.setAdapter(imageaddadapter);
        map = findViewById(R.id.map);
        address = findViewById(R.id.address);
        address2 = findViewById(R.id.address2);
        nearstaddress = findViewById(R.id.nearstaddress);
        groub = findViewById(R.id.groub);
        individual = findViewById(R.id.individual);
        company = findViewById(R.id.company);
        if (getString(R.string.lang).equals("ar")){
            des.setGravity(Gravity.CENTER | Gravity.RIGHT);
            address.setGravity(Gravity.CENTER | Gravity.RIGHT);
            address2.setGravity(Gravity.CENTER | Gravity.RIGHT);
            nearstaddress.setGravity(Gravity.CENTER | Gravity.RIGHT);
        }
    }

    private void onclick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean is_good = true;
                if (des.getText().toString().trim().equals("")) {
                    des.setError(getString(R.string.required));
                    is_good = false;
                }
                if (is_good) {
                    Intent i = new Intent(Registerprovider1Activity.this, Registerprovider2Activity.class);
                    i.putExtra("name", getIntent().getStringExtra("name"));
                    i.putExtra("pass", getIntent().getStringExtra("pass"));
                    i.putExtra("city", getIntent().getStringArrayListExtra("city"));
                    i.putExtra("phone", getIntent().getStringExtra("phone"));
                    i.putExtra("img", getIntent().getStringExtra("img"));
                    i.putExtra("des", des.getText().toString());
                    i.putExtra("address", address.getText().toString());
                    i.putExtra("address2", address2.getText().toString());
                    i.putExtra("nearstaddress", nearstaddress.getText().toString());
                    i.putExtra("type", type);
                    i.putExtra("lat", lat);
                    i.putExtra("lng", lng);
                    i.putExtra("imgs", imgs);
                    startActivity(i);
                }


            }
        });
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
//
//// Start the autocomplete intent.
//                Intent intent = new Autocomplete.IntentBuilder(
//                        AutocompleteActivityMode.FULLSCREEN, fields)
//                        .setTypeFilter(TypeFilter.ADDRESS)
//                        .build(Registerprovider1Activity.this);
//                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                Intent i = new Intent(Registerprovider1Activity.this, MaplocationActivity.class);
                if (!lat.equals("")) {
                    i.putExtra("lat", lat);
                    i.putExtra("lng", lng);
                }
                startActivityForResult(i, 111);
            }
        });
        groub.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.individual) {
                    type = "1";
                } else if (checkedId == R.id.company) {
                    type = "2";
                }
            }
        });
    }

    public void getimage() {
        Camera.cameraOperation();
    }

    @SuppressLint("MissingSuperCall")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // if the result is capturing Image
        if (requestCode == Camera.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
//-----------------------------------------------------------------------------------------------------------------------------------
                myBitmap = (Bitmap) data.getExtras().get("data");
                Uri tempUri = Camera.getImageUri(Registerprovider1Activity.this, myBitmap);
//                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File finalFile = new File(getRealPathFromURI(tempUri));
//                myBitmap = BitmapFactory.decodeFile(sourceFile.getAbsolutePath());
                if (imgs.size() >= 1) {
                    Collections.reverse(imgs);
                    imgs.add(String.valueOf(finalFile));
                    Collections.reverse(imgs);
                } else {
                    imgs.add(String.valueOf(finalFile));
                    Collections.reverse(imgs);
                }
                if (bitmap.size() >= 2) {
                    Addimage addimage = new Addimage();
                    addimage.bitmap = myBitmap;
                    addimage.check = true;
                    Collections.reverse(bitmap);
                    bitmap.add(addimage);
                    Collections.reverse(bitmap);
                } else {
                    Addimage addimage = new Addimage();
                    addimage.bitmap = myBitmap;
                    addimage.check = true;
                    bitmap.add(addimage);
                    Collections.reverse(bitmap);
                }
                imageaddadapter.notifyDataSetChanged();
            } else if (resultCode == RESULT_CANCELED) {

                // user cancelled Image capture
                Toast.makeText(Registerprovider1Activity.this,
                        R.string.camera_closed, Toast.LENGTH_SHORT)
                        .show();

            } else {
                // failed to capture image
                Toast.makeText(Registerprovider1Activity.this,
                        R.string.failed_to_open_camera, Toast.LENGTH_SHORT)
                        .show();
            }

        } else if (requestCode == Camera.PICK_PHOTO_FOR_AVATAR && resultCode == RESULT_OK) {
            if (data == null) {
                Toast.makeText(Registerprovider1Activity.this,
                        " Failed to select picture", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            Uri selectedImageUri = data.getData();
            try {
                myBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            File finalFile = new File(getRealPathFromURI(selectedImageUri));
            if (imgs.size() >= 1) {
                Collections.reverse(imgs);
                imgs.add(String.valueOf(finalFile));
                Collections.reverse(imgs);
            } else {
                imgs.add(String.valueOf(finalFile));
                Collections.reverse(imgs);
            }
            if (bitmap.size() >= 2) {
                Addimage addimage = new Addimage();
                addimage.bitmap = myBitmap;
                addimage.check = true;
                Collections.reverse(bitmap);
                bitmap.add(addimage);
                Collections.reverse(bitmap);
            } else {
                Addimage addimage = new Addimage();
                addimage.bitmap = myBitmap;
                addimage.check = true;
                bitmap.add(addimage);
                Collections.reverse(bitmap);
            }
            imageaddadapter.notifyDataSetChanged();
            //Setting the Bitmap to ImageView

        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE && resultCode == RESULT_OK) {
            Place place = Autocomplete.getPlaceFromIntent(data);
            lat = String.valueOf(place.getLatLng().latitude);
            lng = String.valueOf(place.getLatLng().longitude);
        }if (requestCode == 111 && resultCode == RESULT_OK) {
            lat = data.getStringExtra("lat");
            lng = data.getStringExtra("lng");
        }




    }
}
