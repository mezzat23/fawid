package share.fawid.Activity.Provider;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import share.fawid.Fragment.Provider.HomeFragment;
import share.fawid.Fragment.Provider.MerchantFragment;
import share.fawid.Fragment.Provider.WalletFragment;
import share.fawid.R;

public class HomeActivity extends AppCompatActivity {

    private FrameLayout container;
    public FrameLayout linhome;
    public FrameLayout linmerchant;
    public FrameLayout linwallet;
    private FrameLayout home;
    private ImageView imghome;
    private TextView txthome;
    private FrameLayout Merchant;
    private ImageView imgMerchant;
    private TextView txtMerchant;
    private FrameLayout Wallet;
    private ImageView imgWallet;
    private TextView txtWallet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home2);
        initView();
        onclick();
    }

    private void initView() {
        container = findViewById(R.id.container);
        linhome = findViewById(R.id.linhome);
        linmerchant = findViewById(R.id.linmerchant);
        linwallet = findViewById(R.id.linwallet);
        home = findViewById(R.id.home);
        imghome = findViewById(R.id.imghome);
        txthome = findViewById(R.id.txthome);
        Merchant = findViewById(R.id.Merchant);
        imgMerchant = findViewById(R.id.imgMerchant);
        txtMerchant = findViewById(R.id.txtMerchant);
        Wallet = findViewById(R.id.Wallet);
        imgWallet = findViewById(R.id.imgWallet);
        txtWallet = findViewById(R.id.txtWallet);
        Fragment fragment = new HomeFragment();
        replaceFragment(fragment);
    }
    public void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

//        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
//        }
    }

    public void replaceFragment1(Fragment fragment, Bundle bundle) {
        String backStateName = fragment.getClass().getName();
        FragmentManager manager = getSupportFragmentManager();
//        if (fragment.getArguments() !=null){
//            fragment.getArguments().clear();
//        }
        fragment.setArguments(bundle);
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
//        }
    }

    private void onclick() {
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new HomeFragment();
                replaceFragment(fragment);
            }
        });
        Merchant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new MerchantFragment();
                replaceFragment(fragment);
            }
        });
        Wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new WalletFragment();
                replaceFragment(fragment);
            }
        });
    }


}
