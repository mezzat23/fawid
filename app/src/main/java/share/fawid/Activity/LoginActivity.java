package share.fawid.Activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import share.fawid.Adapter.ViewPagerAdapter;
import share.fawid.Fragment.LoginFragment;
import share.fawid.Fragment.RegisterclientFragment;
import share.fawid.R;

public class LoginActivity extends AppCompatActivity {

    private TabLayout tabs;
    private ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        tabs = findViewById(R.id.tabs);
        pager = findViewById(R.id.pager);
        setupViewPager(pager);
        tabs.post(new Runnable() {
            @Override
            public void run() {
                tabs.setupWithViewPager(pager);
            }
        });
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new LoginFragment(), getString(R.string.login));
        adapter.addFragment(new RegisterclientFragment(), getString(R.string.sign_up));
        viewPager.setAdapter(adapter);
    }
}
