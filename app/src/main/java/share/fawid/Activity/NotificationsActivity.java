package share.fawid.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import share.fawid.Adapter.Notificationsadapter;
import share.fawid.Api.APIModel;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Notification;
import share.fawid.R;

public class NotificationsActivity extends AppCompatActivity {

    private CardView back;
    private RecyclerView list;
    private ProgressBar progress;
    Notificationsadapter notificationsadapter;
    ArrayList<Notification.DataBean> notifications = new ArrayList<>();
    public int page = 0;
    private boolean mLoading = false;
    LinearLayoutManager layoutManage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        initView();
        getdata();
        onclick();
        makescroll();
    }

    private void initView() {
        back = findViewById(R.id.back);
        list = findViewById(R.id.list);
        progress = findViewById(R.id.progress);
        LinearLayoutManager layoutManage = new LinearLayoutManager(this);
        list.setLayoutManager(layoutManage);
        notificationsadapter = new Notificationsadapter(notifications);
        list.setAdapter(notificationsadapter);
    }
    private void onclick(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    private void makescroll() {
        list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int totalItemCount = layoutManage.getItemCount();
                int visibleItemCount = layoutManage.findLastVisibleItemPosition();
                if (!mLoading && visibleItemCount >= totalItemCount - 1 && page > 0) {
                    mLoading = true;
                    getdata();
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void getdata() {
        String url = "notifications/" + LoginSession.setdata(this).data.get(0).id + "/" + page;

        APIModel.getMethod(NotificationsActivity.this, url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Notification>() {
                }.getType();
                Notification data = new Gson().fromJson(responseString, dataType);
                if (page == 0) {
                    notifications.clear();
                }
                if (data.data.size() > 0) {
                    page++;
                    mLoading = false;
                }
                notifications.addAll(data.data);
                notificationsadapter.notifyDataSetChanged();
            }
        });

    }
}