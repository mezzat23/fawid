package share.fawid.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import share.fawid.R;

public class VerifycodeActivity extends AppCompatActivity {

    private CardView back;
    private EditText num6;
    private EditText num5;
    private EditText num4;
    private EditText num3;
    private EditText num2;
    private EditText num1;
    private TextView resend;
    private CardView reg;
    private ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifycode);
        initView();
    }

    private void initView() {
        back = findViewById(R.id.back);
        num6 = findViewById(R.id.num6);
        num5 = findViewById(R.id.num5);
        num4 = findViewById(R.id.num4);
        num3 = findViewById(R.id.num3);
        num2 = findViewById(R.id.num2);
        num1 = findViewById(R.id.num1);
        resend = findViewById(R.id.resend);
        reg = findViewById(R.id.reg);
        progress = findViewById(R.id.progress);
    }
    private void onclick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        num2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL) {

                    Log.e("50505050", "onKey: ");
                    if (num2.getText().toString().equals("")) {
                        num1.requestFocus();
                    }
                }
                return false;
            }
        });
        num3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_

                if (keyCode == KeyEvent.KEYCODE_DEL) {

                    Log.e("50505050", "onKey: ");

                    if (num3.getText().toString().equals("")) {
                        num2.requestFocus();
                    }
                }
                return false;
            }
        });

        num4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_

                if (keyCode == KeyEvent.KEYCODE_DEL) {

                    Log.e("50505050", "onKey: ");

                    if (num4.getText().toString().equals("")) {
                        num3.requestFocus();
                    }
                }
                return false;
            }
        });


        num5.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_

                if (keyCode == KeyEvent.KEYCODE_DEL) {

                    Log.e("50505050", "onKey: ");

                    if (num5.getText().toString().equals("")) {
                        num4.requestFocus();
                    }
                }
                return false;
            }
        });

        num6.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_

                if (keyCode == KeyEvent.KEYCODE_DEL) {

                    if (num6.getText().toString().equals("")) {
                        num5.requestFocus();
                    }
                }
                return false;
            }
        });
        num1.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 == 1) {
                    num2.requestFocus();
                    //        firstEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else if (i2 == 0) {
                    num1.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
        });
        num2.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence charSequence, int i, int i1, final int i2) {
                if (i2 == 1) {
                    num3.requestFocus();
                    //        firstEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else if (i2 == 0) {
                    num1.requestFocus();
                }

                num2.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                        if (keyCode == KeyEvent.KEYCODE_DEL) {

                            Log.e("50505050", "onKey: ");
                            if (i2 == 0) {
                                num1.requestFocus();
                            }
                        }
                        return false;
                    }
                });


            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
        });
        num3.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence charSequence, int i, int i1, final int i2) {
                if (i2 == 1) {
                    num4.requestFocus();
                    //        firstEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else if (i2 == 0) {
                    num2.requestFocus();
                }

                num3.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                        if (keyCode == KeyEvent.KEYCODE_DEL) {

                            Log.e("50505050", "onKey: ");

                            if (i2 == 0) {
                                num2.requestFocus();
                            }
                        }
                        return false;
                    }
                });

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
        });
        num4.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 == 1) {
                    num5.requestFocus();
                    //        firstEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else if (i2 == 0) {
                    num3.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
        });

        num5.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 == 1) {
                    num6.requestFocus();
                    //        firstEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else if (i2 == 0) {
                    num4.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
        });
        num6.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 == 1) {

                    //        firstEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean is_good = true;
                if (num1.getText().toString().trim().equals("")) {
                    num1.setError(getString(R.string.required));
                    is_good = false;
                }
                if (num2.getText().toString().trim().equals("")) {
                    num2.setError(getString(R.string.required));
                    is_good = false;
                }
                if (num3.getText().toString().trim().equals("")) {
                    num3.setError(getString(R.string.required));
                    is_good = false;
                }
                if (num4.getText().toString().trim().equals("")) {
                    num4.setError(getString(R.string.required));
                    is_good = false;
                }
                if (num5.getText().toString().trim().equals("")) {
                    num5.setError(getString(R.string.required));
                    is_good = false;
                }
                if (num6.getText().toString().trim().equals("")) {
                    num6.setError(getString(R.string.required));
                    is_good = false;
                }
                if (is_good){
                    String x = num1.getText().toString() + num2.getText().toString() + num3.getText().toString() + num4.getText().toString() + num5.getText().toString() + num6.getText().toString();
                    Intent i = new Intent(getApplicationContext(),ResetpasswordActivity.class);
                    i.putExtra("code",x);
                    startActivity(i);
                }
            }
        });
    }

}
