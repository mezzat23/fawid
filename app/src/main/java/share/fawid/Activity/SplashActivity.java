package share.fawid.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Locale;

import share.fawid.Activity.Provider.HomeActivity;
import share.fawid.Activity.Provider.RegisterconfirmActivity;
import share.fawid.Helper.Language;
import share.fawid.Helper.LoginSession;
import share.fawid.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        LoginSession.setdata(SplashActivity.this);
        new Thread(){
            @Override
            public void run() {
                try {
                    sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
                    String lang = prefs.getString("lang", "er");
                    if (lang .equals("er")) {
                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Language.showdialog(SplashActivity.this);

                            }
                        }, 500);
                    }else {
                        getLang();
                        getdata();
                    }
                }
            }
        }.start();
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    void getLang() {
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);
            String lang = prefs.getString("lang", getString(R.string.lang));
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            android.content.res.Configuration conf = res.getConfiguration();
            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR1){
                conf.setLocale(new Locale(lang.toLowerCase()));
            } else {
                conf.locale = new Locale(lang.toLowerCase());
            }
            res.updateConfiguration(conf, dm);

        } catch (NullPointerException a) {
            a.printStackTrace();
        } catch (RuntimeException a) {
            a.printStackTrace();
        }

    }
    private void getdata(){
        if (LoginSession.getwelcome(SplashActivity.this) == true){
            if (LoginSession.isLogin){
                if (LoginSession.setdata(SplashActivity.this).data.get(0).type == 1){
                    if (LoginSession.setdata(SplashActivity.this).data.get(0).active_staus.equals("yes")){
                        Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }else {
                        Intent i = new Intent(SplashActivity.this, RegisterconfirmActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                }else {
                    if (LoginSession.setdata(SplashActivity.this).data.get(0).active_staus.equals("yes")){
                        Intent i = new Intent(SplashActivity.this, share.fawid.Activity.Customer.HomeActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        SplashActivity.this.startActivity(i);
                        finish();
                    }else {
                        Intent i = new Intent(SplashActivity.this, RegisterconfirmActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        SplashActivity.this.startActivity(i);
                        finish();
                    }
                }

            }else {
                Intent i = new Intent(SplashActivity.this, share.fawid.Activity.Customer.HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                SplashActivity.this.startActivity(i);
                finish();
            }
        }else {
            Intent i = new Intent(SplashActivity.this, WelcomeActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            SplashActivity.this.startActivity(i);
            finish();
        }
    }
}
