package share.fawid.Activity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;

import cz.msebera.android.httpclient.Header;
import share.fawid.Api.APIModel;
import share.fawid.Models.Pages;
import share.fawid.R;

public class AboutusActivity extends AppCompatActivity {

    private CardView back;
    private TextView text;
    private CardView reg;
    private ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);
        initView();
        onclick();
        getdata();
    }

    private void initView() {
        back = findViewById(R.id.back);
        text = findViewById(R.id.text);
        reg = findViewById(R.id.reg);
        progress = findViewById(R.id.progress);

    }
    private void onclick(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    private void getdata() {
        String url = "pages/18" ;

        APIModel.getMethod(AboutusActivity.this, url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Pages>() {
                }.getType();
                Pages data = new Gson().fromJson(responseString, dataType);
                text.setText(Html.fromHtml(data.data.get(0).content));
            }
        });

    }
}