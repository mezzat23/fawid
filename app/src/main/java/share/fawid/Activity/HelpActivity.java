package share.fawid.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

import cz.msebera.android.httpclient.Header;
import share.fawid.Api.APIModel;
import share.fawid.Helper.Dialogs;
import share.fawid.Helper.Gdata;
import share.fawid.Models.Contact_us;
import share.fawid.R;

public class HelpActivity extends AppCompatActivity {

    private CardView back;
    private CardView reg;
    private ProgressBar progress;
    private LinearLayout lin;
    private TextView address;
    private TextView email;
    private TextView phone;
    private EditText name;
    private EditText email1;
    private EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        initView();
        onclick();
        getdata();
    }

    private void initView() {
        back = findViewById(R.id.back);
        reg = findViewById(R.id.reg);
        progress = findViewById(R.id.progress);

        lin = findViewById(R.id.lin);
        address = findViewById(R.id.address);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        name = findViewById(R.id.name);
        email1 = findViewById(R.id.email1);
        text = findViewById(R.id.text);
    }

    private void onclick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean is_good = true;
                if (text.getText().toString().trim().equals("")) {
                    text.setError(getString(R.string.required));
                    is_good = false;
                }
                if (name.getText().toString().trim().equals("")) {
                    name.setError(getString(R.string.required));
                    is_good = false;
                }
                if (email1.getText().toString().trim().equals("")) {
                    email1.setError(getString(R.string.required));
                    is_good = false;
                }
                if (!Gdata.emailValidator(email1.getText().toString()) && !email1.getText().toString().equals("")) {
                    email1.setError(getString(R.string.email_incorrect));
                    is_good = false;
                }
                if (is_good) {
                    sendmessage();
                }
            }
        });
    }

    private void getdata() {
        String url = "contact";

        APIModel.getMethod(HelpActivity.this, url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
                lin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Contact_us>() {
                }.getType();
                Contact_us data = new Gson().fromJson(responseString, dataType);
                address.setText(data.data.get(0).address);
                email.setText(data.data.get(0).email);
                phone.setText(data.data.get(0).phone);

            }
        });

    }

    private void sendmessage() {
        progress.setVisibility(View.VISIBLE);
        RequestParams requestParams = new RequestParams();
        requestParams.put("message", text.getText().toString());
        requestParams.put("email", email1.getText().toString());
        requestParams.put("name", name.getText().toString());
        APIModel.postMethod(HelpActivity.this, "contact", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(HelpActivity.this, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        sendmessage();
                    }
                });
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    Dialogs.showToast(new JSONObject(responseString).getString("message"), HelpActivity.this);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                onBackPressed();
                progress.setVisibility(View.GONE);
            }
        });

    }
}