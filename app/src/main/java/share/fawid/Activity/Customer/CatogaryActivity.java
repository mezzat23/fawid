package share.fawid.Activity.Customer;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import share.fawid.Adapter.Providersadapter;
import share.fawid.Api.APIModel;
import share.fawid.Models.Catogarydata;
import share.fawid.Models.Home;
import share.fawid.R;

public class CatogaryActivity extends AppCompatActivity {

    private CardView back;
    private EditText edit;
    private FrameLayout filter;
    private FrameLayout sort;
    private RecyclerView list;
    private ProgressBar progress;
    ArrayList<Home.RecentlyBean> recentlyBeans = new ArrayList<>();
    Providersadapter recentadapter;
    public int page = 0;
    private boolean mLoading = false;
    LinearLayoutManager layoutManage;
    Handler handler = new Handler(Looper.myLooper());
    private TextView txtcat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catogary);
        initView();
        onclick();
        getdata();
        makescroll();
    }

    private void initView() {
        back = findViewById(R.id.back);
        edit = findViewById(R.id.edit);
        filter = findViewById(R.id.filter);
        sort = findViewById(R.id.sort);
        list = findViewById(R.id.list);
        progress = findViewById(R.id.progress);
        layoutManage = new LinearLayoutManager(CatogaryActivity.this);
        list.setLayoutManager(layoutManage);
        recentadapter = new Providersadapter(recentlyBeans);
        list.setAdapter(recentadapter);
        txtcat = findViewById(R.id.txtcat);
        txtcat.setText(getIntent().getStringExtra("name"));
    }

    private void onclick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (handler != null)
                    handler.removeCallbacksAndMessages(null);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (edit.getText().toString().equals("")){
//                    subCategoriesBeans.clear();
//                    subCatogaryadapter.notifyDataSetChanged();
//                }else {
//                    gethome();
//                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        page = 0;
                        getdata();

                    }
                }, 1000);

            }
        });
    }

    private void makescroll() {
        list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int totalItemCount = layoutManage.getItemCount();
                int visibleItemCount = layoutManage.findLastVisibleItemPosition();
                if (!mLoading && visibleItemCount >= totalItemCount - 1 && page > 0) {
                    mLoading = true;
                    getdata();
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void getdata() {
        String url = "search";
        RequestParams requestParams = new RequestParams();
        requestParams.put("cat_id", getIntent().getStringExtra("id"));
        if (!edit.getText().toString().equals("")) {
            requestParams.put("word", edit.getText().toString());

        }
        requestParams.put("page_id", page);
        APIModel.postMethod(CatogaryActivity.this, url, requestParams, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Catogarydata>() {
                }.getType();
                Catogarydata data = new Gson().fromJson(responseString, dataType);
                if (page == 0) {
                    recentlyBeans.clear();
                }
                if (data.data.size() > 0) {
                    page++;
                    mLoading = false;
                }
                recentlyBeans.addAll(data.data);
                recentadapter.notifyDataSetChanged();
            }
        });

    }

}
