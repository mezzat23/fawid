package share.fawid.Activity.Customer;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import share.fawid.Fragment.Customer.AccountFragment;
import share.fawid.Fragment.Customer.HomeFragment;
import share.fawid.Fragment.Customer.SavedFragment;
import share.fawid.Fragment.Customer.SearchFragment;
import share.fawid.Helper.LoginSession;
import share.fawid.R;

public class HomeActivity extends AppCompatActivity {

    private FrameLayout home;
    public ImageView imghome;
    public TextView txthome;
    private FrameLayout search;
    public ImageView imgsearch;
    public TextView txtsearch;
    private FrameLayout saved;
    public ImageView imgsaved;
    public TextView txtsaved;
    private FrameLayout account;
    public ImageView imgaccount;
    public TextView txtaccount;
    private FrameLayout container;
    public FrameLayout linhome;
    public FrameLayout linsearch;
    public FrameLayout linsave;
    public FrameLayout linaccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initView();
        onclick();
    }

    private void initView() {
        home = findViewById(R.id.home);
        imghome = findViewById(R.id.imghome);
        txthome = findViewById(R.id.txthome);
        search = findViewById(R.id.search);
        imgsearch = findViewById(R.id.imgsearch);
        txtsearch = findViewById(R.id.txtsearch);
        saved = findViewById(R.id.saved);
        imgsaved = findViewById(R.id.imgsaved);
        txtsaved = findViewById(R.id.txtsaved);
        account = findViewById(R.id.account);
        imgaccount = findViewById(R.id.imgaccount);
        txtaccount = findViewById(R.id.txtaccount);
        Fragment fragment = new HomeFragment();
        replaceFragment(fragment);
        container = findViewById(R.id.container);
        linhome = findViewById(R.id.linhome);
        linsearch = findViewById(R.id.linsearch);
        linsave = findViewById(R.id.linsave);
        linaccount = findViewById(R.id.linaccount);
    }

    public void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    public void replaceFragment1(Fragment fragment, Bundle bundle) {
        String backStateName = fragment.getClass().getName();
        FragmentManager manager = getSupportFragmentManager();
//        if (fragment.getArguments() !=null){
//            fragment.getArguments().clear();
//        }
        fragment.setArguments(bundle);
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    private void onclick() {
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new HomeFragment();
                replaceFragment(fragment);
            }
        });
        account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new AccountFragment();
                replaceFragment(fragment);
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new SearchFragment();
                replaceFragment(fragment);
            }
        });
        saved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginSession.isLogin){
                    Fragment fragment = new SavedFragment();
                    replaceFragment(fragment);
                }else {
                    LoginSession.plsGoLogin(HomeActivity.this);
                }
            }
        });
    }

}
