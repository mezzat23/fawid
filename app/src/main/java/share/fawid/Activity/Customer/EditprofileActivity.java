package share.fawid.Activity.Customer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import share.fawid.Adapter.Cityadapter;
import share.fawid.Api.APIModel;
import share.fawid.Helper.Dialogs;
import share.fawid.Helper.LoginSession;
import share.fawid.Helper.camera.Camera;
import share.fawid.Models.City;
import share.fawid.Models.Login;
import share.fawid.R;

import static share.fawid.Helper.LoginSession.loginFile;
import static share.fawid.Helper.camera.Camera.getRealPathFromURI;
import static share.fawid.Helper.camera.Camera.myBitmap;

public class EditprofileActivity extends AppCompatActivity {

    private CardView back;
    private EditText fullname;
    private EditText phone;
    private LinearLayout city;
    private EditText email;
    private TextView female;
    private TextView male;
    private LinearLayout upload;
    private ImageView img;
    private LinearLayout changepassword;
    private CardView save;
    private ProgressBar progress;
    private TextView txtcity;
    public int city_id = 0;
    private EditText edit;
    private RecyclerView list;
    Dialog dialog;
    Cityadapter cityadapter;
    ArrayList<City.DataBean> cities = new ArrayList<>();
    ArrayList<City.DataBean> cities1 = new ArrayList<>();
    String imgstring = "", type = "";
    private EditText oldpassword;
    private EditText newpassword;
    private EditText repeatpassword;
    private CardView change;
    int x = 0;
    private ImageView imgpass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        initView();
        onclick();
        getdata();
    }

    private void initView() {
        back = findViewById(R.id.back);
        fullname = findViewById(R.id.fullname);
        phone = findViewById(R.id.phone);
        city = findViewById(R.id.city);
        email = findViewById(R.id.email);
        female = findViewById(R.id.female);
        male = findViewById(R.id.male);
        upload = findViewById(R.id.upload);
        img = findViewById(R.id.img);
        changepassword = findViewById(R.id.changepassword);
        save = findViewById(R.id.save);
        progress = findViewById(R.id.progress);
        txtcity = findViewById(R.id.txt);
        Camera.activity = this;
        if (getString(R.string.lang).equals("ar")) {
            fullname.setGravity(Gravity.CENTER | Gravity.RIGHT);
            phone.setGravity(Gravity.CENTER | Gravity.RIGHT);
            email.setGravity(Gravity.CENTER | Gravity.RIGHT);
        }
    }

    private void onclick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getcity();
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Camera.cameraOperation();
            }
        });
        changepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdialogeditpassword();
            }
        });
        male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                male.setTextColor(Color.parseColor("#ffffff"));
                female.setTextColor(Color.parseColor("#7F8FA6"));
                male.setBackgroundColor(Color.parseColor("#074E88"));
                female.setBackgroundColor(Color.parseColor("#ffffff"));
                type = "male";
            }
        });
        female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                female.setTextColor(Color.parseColor("#ffffff"));
                male.setTextColor(Color.parseColor("#7F8FA6"));
                female.setBackgroundColor(Color.parseColor("#074E88"));
                male.setBackgroundColor(Color.parseColor("#ffffff"));
                type = "female";
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean is_good = true;
                if (fullname.getText().toString().trim().equals("")) {
                    fullname.setError(getString(R.string.required));
                    is_good = false;
                }
                if (phone.getText().toString().trim().equals("")) {
                    phone.setError(getString(R.string.required));
                    is_good = false;
                }

                if (city_id == 0) {
                    Dialogs.showToast(getString(R.string.select_city), EditprofileActivity.this);
                    is_good = false;
                }
                if (is_good) {
                    reg(EditprofileActivity.this);
                }
            }
        });
    }

    private void getdata() {
        String url = "uprofile/" + LoginSession.setdata(this).data.get(0).id;
        APIModel.getMethod(EditprofileActivity.this, url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Login>() {
                }.getType();
                Login data = new Gson().fromJson(responseString, dataType);
                fullname.setText(data.data.get(0).full_name);
                phone.setText(data.data.get(0).phone.replaceAll("966", ""));
                txtcity.setText(data.data.get(0).city_name);
                fullname.setText(data.data.get(0).full_name);
                try {
                    Picasso.get().load(data.data.get(0).image).into(img);
                } catch (Exception e) {

                }
                city_id = Integer.parseInt(data.data.get(0).city_id);
                try {
                    if (data.data.get(0).gender.equals("male")) {
                        male.setTextColor(Color.parseColor("#ffffff"));
                        female.setTextColor(Color.parseColor("#7F8FA6"));
                        male.setBackgroundColor(Color.parseColor("#074E88"));
                        female.setBackgroundColor(Color.parseColor("#ffffff"));
                        type = "male";
                    } else if (data.data.get(0).gender.equals("female")) {
                        female.setTextColor(Color.parseColor("#ffffff"));
                        male.setTextColor(Color.parseColor("#7F8FA6"));
                        female.setBackgroundColor(Color.parseColor("#074E88"));
                        male.setBackgroundColor(Color.parseColor("#ffffff"));
                        type = "female";
                    }
                } catch (Exception e) {

                }

            }
        });

    }

    private void getcity() {
        APIModel.getMethod(EditprofileActivity.this, "cites", new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<City>() {
                }.getType();
                City data = new Gson().fromJson(responseString, dataType);
                cities.clear();
                cities1.clear();
                cities.addAll(data.data);
                cities1.addAll(data.data);
                showdialogcity();

            }
        });
    }

    private void showdialogcity() {
        dialog = new Dialog(EditprofileActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_city);
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int width = display.getWidth();
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        back = dialog.findViewById(R.id.back);
        list = dialog.findViewById(R.id.list);
        save = dialog.findViewById(R.id.save);
        edit = dialog.findViewById(R.id.edit);
        LinearLayoutManager layoutManage1 = new LinearLayoutManager(EditprofileActivity.this);
        list.setLayoutManager(layoutManage1);
        cityadapter = new Cityadapter(cities, EditprofileActivity.this, 0);
        list.setAdapter(cityadapter);
        if (city_id > 0) {
            for (int i = 0; i < cities.size(); i++) {
                if (city_id == Integer.parseInt(cities.get(i).id)) {
                    cityadapter.pos = i;
                }
            }
        }
        cityadapter.notifyDataSetChanged();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit.getText().toString().equals("")) {
                    cities.clear();
                    cities.addAll(cities1);
                    cityadapter.notifyDataSetChanged();
                } else {
                    cities.clear();
                    for (int i = 0; i < cities1.size(); i++) {
                        if (cities1.get(i).name.contains(edit.getText().toString())) {
                            cities.add(cities1.get(i));
                        }
                    }
                    cityadapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                city_id = Integer.parseInt(cities.get(cityadapter.pos).id);
                txtcity.setText(cities.get(cityadapter.pos).name);
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void reg(final Context context) {
        progress.setVisibility(View.VISIBLE);
        RequestParams requestParams = new RequestParams();
        requestParams.put("user_id", LoginSession.setdata(this).data.get(0).id);
        requestParams.put("phone", "966" + phone.getText().toString());
        requestParams.put("full_name", fullname.getText().toString());
        requestParams.put("email", email.getText().toString());
        requestParams.put("gender", type);
        try {
            requestParams.put("image", new File(imgstring));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        requestParams.put("city", city_id);
        requestParams.put("user_id", LoginSession.setdata(this).data.get(0).id);
        Log.e("dd", requestParams.toString());
        APIModel.postMethod((Activity) context, "edituser", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(context, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        reg(context);
                    }
                });

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                loginFile = context.getSharedPreferences("LOGIN_FILE", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = loginFile.edit();
                editor.putString("json", responseString);
                editor.putInt("type", 1);
                editor.apply();
                LoginSession.setdata(context);
                Intent i = new Intent(EditprofileActivity.this, HomeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                progress.setVisibility(View.GONE);
            }
        });
    }

    @SuppressLint("MissingSuperCall")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // if the result is capturing Image
        if (requestCode == Camera.CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
//-----------------------------------------------------------------------------------------------------------------------------------
                myBitmap = (Bitmap) data.getExtras().get("data");
                Uri tempUri = Camera.getImageUri(EditprofileActivity.this, myBitmap);
//                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File finalFile = new File(getRealPathFromURI(tempUri));
//                myBitmap = BitmapFactory.decodeFile(sourceFile.getAbsolutePath());
                imgstring = String.valueOf(finalFile);
                img.setImageBitmap(myBitmap);
            } else if (resultCode == RESULT_CANCELED) {

                // user cancelled Image capture
                Toast.makeText(EditprofileActivity.this,
                        R.string.camera_closed, Toast.LENGTH_SHORT)
                        .show();

            } else {
                // failed to capture image
                Toast.makeText(EditprofileActivity.this,
                        R.string.failed_to_open_camera, Toast.LENGTH_SHORT)
                        .show();
            }

        } else if (requestCode == Camera.PICK_PHOTO_FOR_AVATAR && resultCode == RESULT_OK) {
            if (data == null) {
                Toast.makeText(EditprofileActivity.this,
                        " Failed to select picture", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            Uri selectedImageUri = data.getData();
            try {
                myBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            File finalFile = new File(getRealPathFromURI(selectedImageUri));
            imgstring = String.valueOf(finalFile);
            //Setting the Bitmap to ImageView

        }

        try {

            img.setImageBitmap(myBitmap);

        } catch (OutOfMemoryError a) {
            a.printStackTrace();
        } catch (NullPointerException a) {
            a.printStackTrace();
        } catch (RuntimeException a) {
            a.printStackTrace();
        }


    }

    private void showdialogeditpassword() {
        final Dialog dialog = new Dialog(EditprofileActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_changepassword);
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int width = display.getWidth();
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        oldpassword = dialog.findViewById(R.id.oldpassword);
        newpassword = dialog.findViewById(R.id.newpassword);
        repeatpassword = dialog.findViewById(R.id.repeatpassword);
        change = dialog.findViewById(R.id.cahnge);
        imgpass = dialog.findViewById(R.id.imgpass);
        if (getString(R.string.lang).equals("ar")) {
            oldpassword.setGravity(Gravity.CENTER | Gravity.RIGHT);
            newpassword.setGravity(Gravity.CENTER | Gravity.RIGHT);
            repeatpassword.setGravity(Gravity.CENTER | Gravity.RIGHT);
        }
        imgpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (x == 0) {
                    oldpassword.setTransformationMethod(null);
                    imgpass.setBackgroundResource(R.drawable.ic_eye);
                    x = 1;
                } else {
                    x = 0;
                    oldpassword.setTransformationMethod(new PasswordTransformationMethod());
                    imgpass.setBackgroundResource(R.drawable.ic_eye1);
                }
            }
        });
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean is_good = true;
                if (oldpassword.getText().toString().trim().equals("")) {
                    oldpassword.setError(getString(R.string.required));
                    is_good = false;
                }
                if (newpassword.getText().toString().trim().equals("")) {
                    newpassword.setError(getString(R.string.required));
                    is_good = false;
                }
                if (repeatpassword.getText().toString().trim().equals("")) {
                    repeatpassword.setError(getString(R.string.required));
                    is_good = false;
                }
                if (!newpassword.getText().toString().trim().equals(repeatpassword.getText().toString().trim())) {
                    repeatpassword.setError(getString(R.string.password_not));
                    is_good = false;
                }
                if (is_good) {
                    editpass(EditprofileActivity.this, dialog);
                }
            }
        });
        dialog.show();

    }

    private void editpass(final Context context, final Dialog dialog) {
        progress.setVisibility(View.VISIBLE);
        String url = String.format("editprovider?user_id=%s&current_password=%s&newpassword=%s", LoginSession.setdata(this).data.get(0).id, oldpassword.getText().toString(), newpassword.getText().toString());
        APIModel.getMethod((Activity) context, url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(context, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        editpass(context, dialog);
                    }
                });

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Intent i = new Intent(EditprofileActivity.this, HomeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                dialog.dismiss();
                progress.setVisibility(View.GONE);
            }
        });
    }

}
