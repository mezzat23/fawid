package share.fawid.Activity.Customer;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import share.fawid.Adapter.Catogarysearchadapter;
import share.fawid.Adapter.Cityadapter;
import share.fawid.Adapter.Searchadapter;
import share.fawid.Api.APIModel;
import share.fawid.Models.Catogaries;
import share.fawid.Models.Catogarydata;
import share.fawid.Models.Home;
import share.fawid.R;

public class SearchresultsActivity extends AppCompatActivity {

    private CardView back;
    private EditText edit;
    private FrameLayout filter;
    private FrameLayout sort;
    private RecyclerView list;
    private ProgressBar progress;
    ArrayList<Home.RecentlyBean> recentlyBeans = new ArrayList<>();
    Searchadapter recentadapter;
    public int page = 0;
    private boolean mLoading = false;
    LinearLayoutManager layoutManage;
    Handler handler = new Handler(Looper.myLooper());
    public int city_id = 0, cat_id = 0;
    private CardView save;
    Dialog dialog;
    Cityadapter cityadapter;
    ArrayList<Catogaries.DataBean> catogaries = new ArrayList<>();
    ArrayList<Catogaries.DataBean> catogaries1 = new ArrayList<>();
    Catogarysearchadapter catogarysearchadapter;
    private CardView card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchresults);
        initView();
        onclick();

    }

    private void initView() {
        back = findViewById(R.id.back);
        edit = findViewById(R.id.edit);
        filter = findViewById(R.id.filter);
        sort = findViewById(R.id.sort);
        list = findViewById(R.id.list);
        progress = findViewById(R.id.progress);
        layoutManage = new LinearLayoutManager(SearchresultsActivity.this);
        list.setLayoutManager(layoutManage);
        recentadapter = new Searchadapter(recentlyBeans);
        list.setAdapter(recentadapter);
        edit.setText(getIntent().getStringExtra("word"));
        getdata();
        makescroll();
        card = findViewById(R.id.card);
    }

    private void onclick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getcat();
            }
        });
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (handler != null)
                    handler.removeCallbacksAndMessages(null);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (edit.getText().toString().equals("")){
//                    subCategoriesBeans.clear();
//                    subCatogaryadapter.notifyDataSetChanged();
//                }else {
//                    gethome();
//                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        page = 0;
                        getdata();

                    }
                }, 1000);

            }
        });
    }

    private void getcat() {
        APIModel.getMethod(SearchresultsActivity.this, "categories", new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Catogaries>() {
                }.getType();
                Catogaries data = new Gson().fromJson(responseString, dataType);
                catogaries.clear();
                catogaries1.clear();
                catogaries.addAll(data.data);
                catogaries1.addAll(data.data);
                showdialogcatogres();

            }
        });
    }

    private void showdialogcatogres() {
        dialog = new Dialog(SearchresultsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_cat);
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int width = display.getWidth();
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        back = dialog.findViewById(R.id.back);
        list = dialog.findViewById(R.id.list);
        save = dialog.findViewById(R.id.save);
        edit = dialog.findViewById(R.id.edit);
        LinearLayoutManager layoutManage1 = new LinearLayoutManager(SearchresultsActivity.this);
        list.setLayoutManager(layoutManage1);
        catogarysearchadapter = new Catogarysearchadapter(catogaries, SearchresultsActivity.this);
        list.setAdapter(catogarysearchadapter);
        if (cat_id > 0) {
            for (int i = 0; i < catogaries.size(); i++) {
                if (cat_id == Integer.parseInt(catogaries.get(i).id)) {
                    catogarysearchadapter.pos = i;
                }
            }
        }
        catogarysearchadapter.notifyDataSetChanged();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit.getText().toString().equals("")) {
                    catogaries.clear();
                    catogaries.addAll(catogaries1);
                    catogarysearchadapter.notifyDataSetChanged();
                } else {
                    catogaries.clear();
                    for (int i = 0; i < catogaries1.size(); i++) {
                        if (catogaries1.get(i).title.contains(edit.getText().toString())) {
                            catogaries.add(catogaries1.get(i));
                        }
                    }
                    catogarysearchadapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cat_id = Integer.parseInt(catogaries.get(catogarysearchadapter.pos).id);
                edit.setText(catogaries.get(catogarysearchadapter.pos).title);
                getdata();
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void makescroll() {
        list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int totalItemCount = layoutManage.getItemCount();
                int visibleItemCount = layoutManage.findLastVisibleItemPosition();
                if (!mLoading && visibleItemCount >= totalItemCount - 1 && page > 0) {
                    mLoading = true;
                    getdata();
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void getdata() {
        String url = "search";
        RequestParams requestParams = new RequestParams();
        if (getIntent().getIntExtra("cat", 0) != 0) {
            requestParams.put("cat_id", getIntent().getIntExtra("cat", 0));
        }
        if (getIntent().getIntExtra("city", 0) != 0) {
            requestParams.put("city", getIntent().getIntExtra("city", 0));
        }
        if (!edit.getText().toString().equals("")) {
            requestParams.put("word", edit.getText().toString());

        }
        requestParams.put("page_id", page);

        Log.e("data", requestParams.toString());
        APIModel.postMethod(SearchresultsActivity.this, url, requestParams, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Catogarydata>() {
                }.getType();
                Catogarydata data = new Gson().fromJson(responseString, dataType);
                if (page == 0) {
                    recentlyBeans.clear();
                }
                if (data.data.size() > 0) {
                    page++;
                    mLoading = false;
                }
                recentlyBeans.addAll(data.data);
                recentadapter.notifyDataSetChanged();
            }
        });

    }
}
