package share.fawid.Activity.Customer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

import cz.msebera.android.httpclient.Header;
import share.fawid.Adapter.ViewPagerAdapter;
import share.fawid.Api.APIModel;
import share.fawid.Fragment.Customer.DetailsFragment;
import share.fawid.Fragment.Customer.GallaryFragment;
import share.fawid.Fragment.Customer.ReviewsFragment;
import share.fawid.Helper.Dialogs;
import share.fawid.Helper.Language;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Matgerprofile;
import share.fawid.R;

public class DetailsActivity extends AppCompatActivity {

    private CardView back;
    private ImageView saved;
    private ImageView img;
    private TextView name;
    private TextView service;
    public CardView contact;
    private TabLayout tabs;
    public ViewPager pager;
    public Matgerprofile matgerprofile;
    private ProgressBar progress;
    private LinearLayout lin;
    Matgerprofile data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        initView();
        onclick();
        getdata();
    }

    private void initView() {
        back = findViewById(R.id.back);
        saved = findViewById(R.id.saved);
        img = findViewById(R.id.img);
        name = findViewById(R.id.name);
        service = findViewById(R.id.service);
        contact = findViewById(R.id.contact);
        tabs = findViewById(R.id.tabs);
        pager = findViewById(R.id.pager);
        try {
            Picasso.get().load(getIntent().getStringExtra("img")).into(img);
        } catch (Exception e) {

        }
        name.setText(getIntent().getStringExtra("name"));
        setupViewPager(pager);
        tabs.post(new Runnable() {
            @Override
            public void run() {
                tabs.setupWithViewPager(pager);
            }
        });
        progress = findViewById(R.id.progress);
        lin = findViewById(R.id.lin);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new DetailsFragment(), getString(R.string.details));
        adapter.addFragment(new GallaryFragment(), getString(R.string.gallary));
        adapter.addFragment(new ReviewsFragment(), getString(R.string.reviews));
        viewPager.setAdapter(adapter);
    }

    private void onclick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        saved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginSession.isLogin) {
                    makesave(DetailsActivity.this, progress);
                } else {
                    LoginSession.plsGoLogin(DetailsActivity.this);
                }
            }
        });
        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginSession.isLogin){
                    Language language = new Language();
                    language.contactdialog(DetailsActivity.this, data);
                }else {
                    LoginSession.plsGoLogin(DetailsActivity.this);
                }
            }
        });
    }

    private void makesave(final Context context, final ProgressBar progress) {
        String url = "members/" + LoginSession.setdata(context).data.get(0).id + "/" + getIntent().getStringExtra("id");
        APIModel.putMethod((Activity) context, url, null, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(context, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        makesave(context, progress);
                    }
                });

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject jo = new JSONObject(responseString);
                    Dialogs.showToast(jo.getString("message"), context);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    private void getdata() {
        String url = "pprofile/" + getIntent().getStringExtra("id");
        APIModel.getMethod(DetailsActivity.this, url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
                lin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Matgerprofile>() {
                }.getType();
                data = new Gson().fromJson(responseString, dataType);

            }
        });

    }

}
