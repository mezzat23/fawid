package share.fawid.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;
import share.fawid.Activity.ForgetpassActivity;
import share.fawid.Activity.Provider.HomeActivity;
import share.fawid.Activity.TermsActivity;
import share.fawid.Api.APIModel;
import share.fawid.Helper.LoginSession;
import share.fawid.R;

import static share.fawid.Helper.LoginSession.loginFile;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    View view;
    private EditText phone;
    private EditText password;
    private ProgressBar progress;
    private TextView forget;
    private CardView login;
    private LinearLayout terms;
    private ImageView imgpass;
int x = 0 ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_login, container, false);
            initView();
            onclick();
        }
        return view;
    }

    private void initView() {
        phone = view.findViewById(R.id.phone);
        password = view.findViewById(R.id.password);
        progress = view.findViewById(R.id.progress);
        forget = view.findViewById(R.id.forget);
        login = view.findViewById(R.id.login);
        terms = view.findViewById(R.id.terms);
        if (getString(R.string.lang).equals("ar")) {
            phone.setGravity(Gravity.CENTER | Gravity.RIGHT);
            password.setGravity(Gravity.CENTER | Gravity.RIGHT);
        }
        imgpass = view.findViewById(R.id.imgpass);
    }

    private void onclick() {
        imgpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (x == 0){
                    password.setTransformationMethod(null);
                    imgpass.setBackgroundResource(R.drawable.ic_eye);
                    x = 1 ;
                }else {
                    x = 0 ;
                    password.setTransformationMethod(new PasswordTransformationMethod());
                    imgpass.setBackgroundResource(R.drawable.ic_eye1);
                }
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean is_good = true;
                if (phone.getText().toString().trim().equals("")) {
                    phone.setError(getString(R.string.required));
                    is_good = false;
                }
                if (password.getText().toString().trim().equals("")) {
                    password.setError(getString(R.string.required));
                    is_good = false;
                }
                if (is_good) {
                    login(getActivity());
                }
            }
        });
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), TermsActivity.class);
                startActivity(i);
            }
        });
        forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ForgetpassActivity.class);
                startActivity(i);
            }
        });
    }

    private void login(final Context context) {
        progress.setVisibility(View.VISIBLE);
        RequestParams requestParams = new RequestParams();
        requestParams.put("phone", "966" + phone.getText().toString());
        requestParams.put("password", password.getText().toString());
        requestParams.put("platform", "2");
        requestParams.put("registrationid", FirebaseInstanceId.getInstance().getToken());
        Log.e("dd", requestParams.toString());
        APIModel.postMethod((Activity) context, "login", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(context, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        login(context);
                    }
                });

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                loginFile = context.getSharedPreferences("LOGIN_FILE", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = loginFile.edit();
                editor.putString("json", responseString);
                editor.putInt("type", 1);
                editor.apply();
                LoginSession.setdata(context);
                if (LoginSession.setdata(getActivity()).data.get(0).type == 1) {
                    Intent i = new Intent(getActivity(), HomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    getActivity().finish();
                } else {
                    Intent i = new Intent(getActivity(), share.fawid.Activity.Customer.HomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    getActivity().finish();
                }
                progress.setVisibility(View.GONE);
            }
        });
    }
}
