package share.fawid.Fragment.Provider;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import share.fawid.Activity.Provider.HomeActivity;
import share.fawid.Adapter.ViewPagerAdapter;
import share.fawid.Api.APIModel;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Wallet;
import share.fawid.R;

public class WalletFragment extends Fragment {
    View view;
    public TextView total;
    public TextView date;
    public TextView clicks;
    public TextView priceclicks;
    private TabLayout tabs;
    private ViewPager pager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_wallet, container, false);
            initView();
            getdata();
        }
        return view;
    }

    private void initView() {
        total = view.findViewById(R.id.total);
        date = view.findViewById(R.id.date);
        clicks = view.findViewById(R.id.clicks);
        priceclicks = view.findViewById(R.id.priceclicks);
        tabs = view.findViewById(R.id.tabs);
        pager = view.findViewById(R.id.pager);
        setupViewPager(pager);
        tabs.post(new Runnable() {
            @Override
            public void run() {
                tabs.setupWithViewPager(pager);
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new HistoryFragment(), getString(R.string.History));
        adapter.addFragment(new PackagesFragment(), getString(R.string.Packages));
        viewPager.setAdapter(adapter);
    }

    private void getdata() {
        String url = "wallet/" + LoginSession.setdata(getActivity()).data.get(0).id;

        APIModel.getMethod(getActivity(), url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
            }

            @Override
            public void onFinish() {
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                total.setText("0");
                clicks.setText(getString(R.string.total_clicks) + " " +"0");
                priceclicks.setText(getString(R.string.Entitlements) + " " + "0" + " " + getString(R.string.sar));
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                String currentDateandTime = sdf.format(new Date());
                date.setText(currentDateandTime);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Wallet>() {
                }.getType();
                Wallet data = new Gson().fromJson(responseString, dataType);
                total.setText(data.data.get(0).balance);
                clicks.setText(getString(R.string.total_clicks) + " " + data.data.get(0).clicks);
                priceclicks.setText(getString(R.string.Entitlements) + " " + data.data.get(0).clicks_price + " " + getString(R.string.sar));
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                String currentDateandTime = sdf.format(new Date());
                date.setText(currentDateandTime);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        getdata();
        ((HomeActivity) getActivity()).linhome.setVisibility(View.INVISIBLE);
        ((HomeActivity) getActivity()).linmerchant.setVisibility(View.INVISIBLE);
        ((HomeActivity) getActivity()).linwallet.setVisibility(View.VISIBLE);
        getdata();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    FragmentManager fm = getFragmentManager();
                    fm.popBackStack();
                    return true;
                }
                return false;
            }
        });

    }
}