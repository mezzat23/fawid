package share.fawid.Fragment.Provider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;

import cz.msebera.android.httpclient.Header;
import share.fawid.Activity.Provider.HomeActivity;
import share.fawid.Api.APIModel;
import share.fawid.Helper.Dialogs;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Matgerprofile;
import share.fawid.R;

import static share.fawid.Helper.LoginSession.loginFile;

/**
 * A simple {@link Fragment} subclass.
 */
public class LinksFragment extends Fragment {
    View view;
    private EditText facebook;
    private EditText Twitter;
    private EditText Snapchat;
    private EditText Instagram;
    private EditText Linkedin;
    private EditText youtube;
    private EditText website;
    private CardView reg;
    private ProgressBar progress;
    private EditText whatsapp;
    private EditText skype;
    private EditText tambler;
    private EditText phone;
    int click = 0 ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_links, container, false);
            initView();
            getdata();
            onclick();
        }
        return view;
    }

    private void initView() {
        facebook = view.findViewById(R.id.facebook);
        Twitter = view.findViewById(R.id.Twitter);
        Snapchat = view.findViewById(R.id.Snapchat);
        Instagram = view.findViewById(R.id.Instagram);
        Linkedin = view.findViewById(R.id.Linkedin);
        youtube = view.findViewById(R.id.youtube);
        website = view.findViewById(R.id.website);
        reg = view.findViewById(R.id.reg);
        progress = view.findViewById(R.id.progress);
        whatsapp = view.findViewById(R.id.whatsapp);
        skype = view.findViewById(R.id.skype);
        tambler = view.findViewById(R.id.tambler);
        phone = view.findViewById(R.id.phone);
        if (getString(R.string.lang).equals("ar")){
            facebook.setGravity(Gravity.CENTER | Gravity.RIGHT);
            Twitter.setGravity(Gravity.CENTER | Gravity.RIGHT);
            Snapchat.setGravity(Gravity.CENTER | Gravity.RIGHT);
            Instagram.setGravity(Gravity.CENTER | Gravity.RIGHT);
            Linkedin.setGravity(Gravity.CENTER | Gravity.RIGHT);
            youtube.setGravity(Gravity.CENTER | Gravity.RIGHT);
            website.setGravity(Gravity.CENTER | Gravity.RIGHT);
            whatsapp.setGravity(Gravity.CENTER | Gravity.RIGHT);
            skype.setGravity(Gravity.CENTER | Gravity.RIGHT);
            tambler.setGravity(Gravity.CENTER | Gravity.RIGHT);
            phone.setGravity(Gravity.CENTER | Gravity.RIGHT);
        }
    }

    private void onclick() {
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!facebook.getText().toString().trim().equals("")){
                    click++;
                }
                if (!Twitter.getText().toString().trim().equals("")){
                    click++;
                }
                if (!Snapchat.getText().toString().trim().equals("")){
                    click++;
                }
                if (!Instagram.getText().toString().trim().equals("")){
                    click++;
                }
                if (!Linkedin.getText().toString().trim().equals("")){
                    click++;
                }
                if (!youtube.getText().toString().trim().equals("")){
                    click++;
                }
                if (!website.getText().toString().trim().equals("")){
                    click++;
                }
                if (!whatsapp.getText().toString().trim().equals("")){
                    click++;
                }
                if (!skype.getText().toString().trim().equals("")){
                    click++;
                }
                if (!tambler.getText().toString().trim().equals("")){
                    click++;
                }
                if (!phone.getText().toString().trim().equals("")){
                    click++;
                }
                if (click >= 3){
                    reg(getActivity());
                }else {
                    Dialogs.showToast(getString(R.string.add_3contacts),getActivity());
                }
            }
        });
    }

    private void reg(final Context context) {
        progress.setVisibility(View.VISIBLE);
        RequestParams requestParams = new RequestParams();
        if (!facebook.getText().toString().trim().equals("")) {
            requestParams.put("facebook", facebook.getText().toString());
        }
        if (!Twitter.getText().toString().trim().equals("")) {
            requestParams.put("twitter", Twitter.getText().toString());
        }
        if (!Linkedin.getText().toString().trim().equals("")) {
            requestParams.put("linkedin", Linkedin.getText().toString());
        }
        if (!youtube.getText().toString().trim().equals("")) {
            requestParams.put("youtube", youtube.getText().toString());
        }
        if (!website.getText().toString().trim().equals("")) {
            requestParams.put("website", website.getText().toString());
        }
        if (!Snapchat.getText().toString().trim().equals("")) {
            requestParams.put("snapchat", Snapchat.getText().toString());
        }
        if (!phone.getText().toString().trim().equals("")) {
            requestParams.put("mobile", phone.getText().toString());
        }
        if (!Instagram.getText().toString().trim().equals("")) {
            requestParams.put("insatgram", Instagram.getText().toString());
        }
        if (!skype.getText().toString().trim().equals("")) {
            requestParams.put("skype", skype.getText().toString());
        }
        if (!whatsapp.getText().toString().trim().equals("")) {
            requestParams.put("whatsapp", whatsapp.getText().toString());
        }
        if (!tambler.getText().toString().trim().equals("")) {
            requestParams.put("tambler", tambler.getText().toString());
        }
        requestParams.put("provider_id", LoginSession.setdata(getActivity()).data.get(0).id);
        Log.e("dd", requestParams.toString());
        APIModel.postMethod((Activity) context, "editprovider", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(context, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        reg(context);
                    }
                });

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                loginFile = context.getSharedPreferences("LOGIN_FILE", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = loginFile.edit();
                editor.putString("json", responseString);
                editor.putInt("type", 2);
                editor.apply();
                LoginSession.setdata(context);
                Intent i = new Intent(getActivity(), HomeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                getActivity().finish();
                progress.setVisibility(View.GONE);
            }
        });
    }

    private void getdata() {
        String url = "pprofile/" + LoginSession.setdata(getActivity()).data.get(0).id;
        APIModel.getMethod(getActivity(), url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Matgerprofile>() {
                }.getType();
                Matgerprofile data = new Gson().fromJson(responseString, dataType);
                facebook.setText(data.data.get(0).facebookuser);
                Twitter.setText(data.data.get(0).twitteruser);
                Instagram.setText(data.data.get(0).insatgramuser);
                Linkedin.setText(data.data.get(0).linkedin);
                youtube.setText(data.data.get(0).youtubeuser);
                Snapchat.setText(data.data.get(0).snapchatuser);
                website.setText(data.data.get(0).website);
                whatsapp.setText(data.data.get(0).whatsapp);
                skype.setText(data.data.get(0).skype);
                tambler.setText(data.data.get(0).tambler);
                phone.setText(data.data.get(0).mobile);
            }
        });

    }
}
