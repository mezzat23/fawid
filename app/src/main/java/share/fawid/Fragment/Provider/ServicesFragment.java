package share.fawid.Fragment.Provider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import share.fawid.Activity.Provider.HomeActivity;
import share.fawid.Adapter.Jobadapter;
import share.fawid.Api.APIModel;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Catogaries;
import share.fawid.Models.Matgerprofile;
import share.fawid.R;

import static share.fawid.Helper.LoginSession.loginFile;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServicesFragment extends Fragment {
    View view;
    private EditText edit;
    private RecyclerView list;
    private CardView reg;
    Jobadapter jobadapter;
    ArrayList<Catogaries.DataBean> catogaries = new ArrayList<>();
    ArrayList<Catogaries.DataBean> catogaries1 = new ArrayList<>();
    ArrayList<String> cats = new ArrayList<>();
    ArrayList<String> subcats = new ArrayList<>();
    private ProgressBar progress;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_services, container, false);
            initView();
            getcatogary();
            onclick();
        }
        return view;
    }

    private void initView() {
        edit = view.findViewById(R.id.edit);
        list = view.findViewById(R.id.list);
        reg = view.findViewById(R.id.reg);
        LinearLayoutManager layoutManage1 = new LinearLayoutManager(getActivity());
        list.setLayoutManager(layoutManage1);
        jobadapter = new Jobadapter(catogaries);
        list.setAdapter(jobadapter);
        progress = view.findViewById(R.id.progress);
    }

    private void onclick() {
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit.getText().toString().equals("")) {
                    catogaries.clear();
                    catogaries.addAll(catogaries1);
                    jobadapter.notifyDataSetChanged();
                } else {
                    catogaries.clear();
                    for (int i = 0; i < catogaries1.size(); i++) {
                        if (catogaries1.get(i).title.contains(edit.getText().toString())) {
                            catogaries.add(catogaries1.get(i));
                        }
                    }
                    jobadapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < catogaries.size(); i++) {
//                    if (catogaries.get(i).sub_cats.size() > 0){
                    if (catogaries.get(i).check) {
                        cats.add(catogaries.get(i).id);
                    }
                    for (int j = 0; j < catogaries.get(i).sub_cats.size(); j++) {
                        if (catogaries.get(i).sub_cats.get(j).check == true) {
                            subcats.add(catogaries.get(i).sub_cats.get(j).id);
                        }
                    }
                    for (int j = 0; j < catogaries.get(i).sub_cats.size(); j++) {
                        for (int ii = 0; ii < cats.size(); ii++) {
                            if (catogaries.get(i).id.equals(cats.get(ii))) {
                                break;
                            }
                            if (ii == cats.size() - 1) {
                                if (catogaries.get(i).sub_cats.get(j).check == true) {
                                    cats.add(catogaries.get(i).id);
                                    break;
                                }
                            }
                        }

                    }

//                    }
//                    else {
//
//                    }
                }
                reg(getActivity());


            }
        });
    }

    private void getcatogary() {
        APIModel.getMethod(getActivity(), "categories", new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Catogaries>() {
                }.getType();
                Catogaries data = new Gson().fromJson(responseString, dataType);
                catogaries.clear();
                catogaries1.clear();
                catogaries.addAll(data.data);
                catogaries1.addAll(data.data);
                getdata();
            }
        });
    }

    private void getdata() {
        String url = "pprofile/" + LoginSession.setdata(getActivity()).data.get(0).id;
        APIModel.getMethod(getActivity(), url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Matgerprofile>() {
                }.getType();
                Matgerprofile data = new Gson().fromJson(responseString, dataType);
                for (int i = 0; i < catogaries.size(); i++) {
                    for (int ii = 0; ii < catogaries.get(i).sub_cats.size(); ii++) {
                        for (int j = 0; j < data.data.get(0).main_cats.size(); j++) {
                            for (int jj = 0; jj < data.data.get(0).main_cats.get(j).subcats.size(); jj++) {
                                if (catogaries.get(i).sub_cats.get(ii).id.equals(data.data.get(0).main_cats.get(j).subcats.get(jj).id)) {
                                    catogaries.get(i).sub_cats.get(ii).check = true;
                                }
                            }
                        }
                    }
                    for (int j = 0; j < data.data.get(0).main_cats.size(); j++) {
                        if (catogaries.get(i).id.equals(data.data.get(0).main_cats.get(j).cat_id)) {
                            catogaries.get(i).check = true;
                        }
                    }


                }
                jobadapter.notifyDataSetChanged();


            }
        });

    }

    private void reg(final Context context) {
        progress.setVisibility(View.VISIBLE);
        RequestParams requestParams = new RequestParams();
        if (cats.size() > 0) {
            requestParams.put("cat_id", cats);
        }
        if (subcats.size() > 0) {
            requestParams.put("sub_cat_id", subcats);
        }
        requestParams.put("provider_id", LoginSession.setdata(getActivity()).data.get(0).id);
        Log.e("parms", requestParams.toString());
        APIModel.postMethod((Activity) context, "editprovider", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(context, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        reg(context);
                    }
                });

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                loginFile = context.getSharedPreferences("LOGIN_FILE", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = loginFile.edit();
                editor.putString("json", responseString);
                editor.putInt("type", 2);
                editor.apply();
                LoginSession.setdata(context);
                Intent i = new Intent(getActivity(), HomeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                getActivity().finish();
                progress.setVisibility(View.GONE);
            }
        });
    }
}
