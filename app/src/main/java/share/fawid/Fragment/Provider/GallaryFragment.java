package share.fawid.Fragment.Provider;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import share.fawid.Adapter.Imagesadapter;
import share.fawid.Api.APIModel;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Matgerprofile;
import share.fawid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class GallaryFragment extends Fragment {
    View view;
    private RecyclerView list;
    private ProgressBar progress;
    Imagesadapter imageaddadapter;
    ArrayList<Matgerprofile.DataBean.ImagesBean> images = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_gallaty, container, false);
            initView();
        }
        return view;
    }

    private void initView() {
        list = view.findViewById(R.id.list);
        progress = view.findViewById(R.id.progress);
        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(),3);
//        layoutManager.setFlexDirection(FlexDirection.ROW);
//        layoutManager.setJustifyContent(JustifyContent.FLEX_END);
        list.setLayoutManager(layoutManager);
        imageaddadapter = new Imagesadapter(images);
        list.setAdapter(imageaddadapter);
    }
    private void getdata(){
        String url = "pprofile/" + LoginSession.setdata(getActivity()).data.get(0).id;
        APIModel.getMethod(getActivity(), url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.e("res",responseString);
                Type dataType = new TypeToken<Matgerprofile>() {
                }.getType();
                Matgerprofile data = new Gson().fromJson(responseString, dataType);
                images.clear();
                images.addAll(data.data.get(0).images);
                imageaddadapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        getdata();
    }
}
