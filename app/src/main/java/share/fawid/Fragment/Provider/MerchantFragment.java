package share.fawid.Fragment.Provider;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;

import cz.msebera.android.httpclient.Header;
import share.fawid.Activity.Provider.EditmerchantActivity;
import share.fawid.Activity.Provider.HomeActivity;
import share.fawid.Adapter.ViewPagerAdapter;
import share.fawid.Api.APIModel;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Matgerprofile;
import share.fawid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MerchantFragment extends Fragment {
    View view;
    private CardView back;
    private ImageView edit;
    private ImageView img;
    private TextView name;
    private TextView service;
    private CardView contact;
    private TabLayout tabs;
    private ViewPager pager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_merchant, container, false);
            initView();
            onclick();
        }
        return view;
    }

    private void initView() {
        back = view.findViewById(R.id.back);
        edit = view.findViewById(R.id.edit);
        img = view.findViewById(R.id.img);
        name = view.findViewById(R.id.name);
        service = view.findViewById(R.id.service);
        contact = view.findViewById(R.id.contact);
        tabs = view.findViewById(R.id.tabs);
        pager = view.findViewById(R.id.pager);
        setupViewPager(pager);
        tabs.post(new Runnable() {
            @Override
            public void run() {
                tabs.setupWithViewPager(pager);
            }
        });
        contact.setVisibility(View.GONE);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new DetailsFragment(), getString(R.string.details));
        adapter.addFragment(new GallaryFragment(), getString(R.string.gallary));
        adapter.addFragment(new ReviewsFragment(), getString(R.string.reviews));
        viewPager.setAdapter(adapter);
    }

    private void onclick() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                fm.popBackStack();
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), EditmerchantActivity.class);
                startActivity(i);
            }
        });
    }

    private void getdata() {
        String url = "pprofile/" + LoginSession.setdata(getActivity()).data.get(0).id;
        APIModel.getMethod(getActivity(), url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {

            }

            @Override
            public void onFinish() {
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Matgerprofile>() {
                }.getType();
                Matgerprofile data = new Gson().fromJson(responseString, dataType);
                name.setText(data.data.get(0).full_name);
                try {
                    Picasso.get().load(data.data.get(0).image).into(img);
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getdata();
        ((HomeActivity) getActivity()).linhome.setVisibility(View.INVISIBLE);
        ((HomeActivity) getActivity()).linmerchant.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).linwallet.setVisibility(View.INVISIBLE);
        getdata();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    FragmentManager fm = getFragmentManager();
                    fm.popBackStack();
                    return true;
                }
                return false;
            }
        });

    }
}
