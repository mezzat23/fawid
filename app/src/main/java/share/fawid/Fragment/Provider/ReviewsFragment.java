package share.fawid.Fragment.Provider;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import share.fawid.Adapter.Ratesadapter;
import share.fawid.Api.APIModel;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Rates;
import share.fawid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsFragment extends Fragment {
    View view;
    private TextView contacts;
    private TextView Reviews;
    private TextView Location;
    private TextView numrate;
    private TextView txt5;
    private SeekBar seek5;
    private TextView txt4;
    private SeekBar seek4;
    private TextView txt3;
    private SeekBar seek3;
    private TextView txt2;
    private SeekBar seek2;
    private TextView txt51;
    private SeekBar seek1;
    private CardView rate;
    private RecyclerView list;
    private LinearLayout lin;
    private ProgressBar progress;
    ArrayList<Rates.DataBean.CommentsBean> recentlyBeans = new ArrayList<>();
    Ratesadapter recentadapter;
    public int page = 0;
    private boolean mLoading = false;
    LinearLayoutManager layoutManage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_reviews, container, false);
            initView();
            getdata();
            makescroll();
        }
        return view;
    }

    private void initView() {
        contacts = view.findViewById(R.id.contacts);
        Reviews = view.findViewById(R.id.Reviews);
        Location = view.findViewById(R.id.Location);
        numrate = view.findViewById(R.id.numrate);
        txt5 = view.findViewById(R.id.txt5);
        seek5 = view.findViewById(R.id.seek5);
        txt4 = view.findViewById(R.id.txt4);
        seek4 = view.findViewById(R.id.seek4);
        txt3 = view.findViewById(R.id.txt3);
        seek3 = view.findViewById(R.id.seek3);
        txt2 = view.findViewById(R.id.txt2);
        seek2 = view.findViewById(R.id.seek2);
        txt51 = view.findViewById(R.id.txt51);
        seek1 = view.findViewById(R.id.seek1);
        rate = view.findViewById(R.id.rate);
        list = view.findViewById(R.id.list);
        lin = view.findViewById(R.id.lin);
        progress = view.findViewById(R.id.progress);
        layoutManage = new LinearLayoutManager(getActivity());
        list.setLayoutManager(layoutManage);
        recentadapter = new Ratesadapter(recentlyBeans);
        list.setAdapter(recentadapter);
        rate.setVisibility(View.GONE);
    }

    private void makescroll() {
        list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int totalItemCount = layoutManage.getItemCount();
                int visibleItemCount = layoutManage.findLastVisibleItemPosition();
                if (!mLoading && visibleItemCount >= totalItemCount - 1 && page > 0) {
                    mLoading = true;
                    getdata();
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void getdata() {
        String url = "rates/" + LoginSession.setdata(getActivity()).data.get(0).id + "/" + page;

        APIModel.getMethod(getActivity(), url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
                lin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Rates>() {
                }.getType();
                Rates data = new Gson().fromJson(responseString, dataType);
                if (page == 0) {
                    recentlyBeans.clear();
                }
                if (data.data.size() > 0) {
                    if (page == 0){
                        page = 2 ;
                        numrate.setText(data.data.get(0).avarage);
                        txt5.setText(data.data.get(0)._$5star_num);
                        txt4.setText(data.data.get(0)._$4star_num);
                        txt3.setText(data.data.get(0)._$3star_num);
                        txt2.setText(data.data.get(0)._$2star_num);
                        txt51.setText(data.data.get(0)._$1star_num);
                        seek5.setProgress(Integer.parseInt(data.data.get(0)._$5star_percent));
                        seek4.setProgress(Integer.parseInt(data.data.get(0)._$4star_percent));
                        seek3.setProgress(Integer.parseInt(data.data.get(0)._$3star_percent));
                        seek2.setProgress(Integer.parseInt(data.data.get(0)._$2star_percent));
                        seek1.setProgress(Integer.parseInt(data.data.get(0)._$1star_percent));
                    }else {
                        page++;
                    }
                    mLoading = false;
                }

                recentlyBeans.addAll(data.data.get(0).comments);
                recentadapter.notifyDataSetChanged();
            }
        });

    }
}
