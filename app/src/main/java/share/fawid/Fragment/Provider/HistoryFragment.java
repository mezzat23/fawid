package share.fawid.Fragment.Provider;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import share.fawid.Adapter.Walletadapter;
import share.fawid.Api.APIModel;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Wallet;
import share.fawid.R;

public class HistoryFragment extends Fragment {
    View view;
    private RecyclerView list;
    private ProgressBar progress;
    Walletadapter walletadapter;
    ArrayList<Wallet.DataBean.ShipingBean> wallets = new ArrayList<>();
    public int page = 0;
    private boolean mLoading = false;
    LinearLayoutManager layoutManage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_history, container, false);
            initView();
            getdata();
        }
        return view;
    }

    private void initView() {
        list = view.findViewById(R.id.list);
        progress = view.findViewById(R.id.progress);
        layoutManage = new LinearLayoutManager(getActivity());
        list.setLayoutManager(layoutManage);
        walletadapter = new Walletadapter(wallets);
        list.setAdapter(walletadapter);
    }

    private void makescroll() {
        list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int totalItemCount = layoutManage.getItemCount();
                int visibleItemCount = layoutManage.findLastVisibleItemPosition();
                if (!mLoading && visibleItemCount >= totalItemCount - 1 && page > 0) {
                    mLoading = true;
                    getdata();
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void getdata() {
        String url = "wallet/" + LoginSession.setdata(getActivity()).data.get(0).id + "/" + page;

        APIModel.getMethod(getActivity(), url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Wallet>() {
                }.getType();
                Wallet data = new Gson().fromJson(responseString, dataType);
                if (page == 0) {
                    wallets.clear();

                }
                if (data.data.get(0).shiping.size() > 0) {
                    page++;
                    mLoading = false;
                }
                wallets.addAll(data.data.get(0).shiping);
                walletadapter.notifyDataSetChanged();
            }
        });

    }
}