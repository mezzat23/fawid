package share.fawid.Fragment.Provider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.libraries.places.api.Places;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;

import cz.msebera.android.httpclient.Header;
import share.fawid.Activity.MaplocationActivity;
import share.fawid.Activity.Provider.HomeActivity;
import share.fawid.Adapter.Imageadd1adapter;
import share.fawid.Api.APIModel;
import share.fawid.Helper.LoginSession;
import share.fawid.Helper.camera.Camera;
import share.fawid.Models.Addimage;
import share.fawid.Models.Matgerprofile;
import share.fawid.R;

import static android.app.Activity.RESULT_OK;
import static share.fawid.Helper.LoginSession.loginFile;

/**
 * A simple {@link Fragment} subclass.
 */
public class InfoFragment extends Fragment {
    View view;
    private EditText des;
    private EditText address;
    private EditText address2;
    private EditText nearstaddress;
    private RadioGroup groub;
    private RadioButton individual;
    private RadioButton company;
    private RecyclerView list;
    private CardView reg;
    private LinearLayout lin;
    private ProgressBar progress;
    private LinearLayout map;
    String type = "";
    public ArrayList<Addimage> bitmap = new ArrayList<>();
    public ArrayList<String> imgs = new ArrayList<>();
    public Imageadd1adapter imageaddadapter;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 458;
    public String lat = "", lng = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_info, container, false);
            initView();
            onclick();
            getdata();
        }
        return view;
    }

    private void initView() {
        Places.initialize(getActivity(), "AIzaSyAgRHIRXw8irYIvIP6aIoGdyDzmQFAD4bo");
        des = view.findViewById(R.id.des);
        address = view.findViewById(R.id.address);
        address2 = view.findViewById(R.id.address2);
        nearstaddress = view.findViewById(R.id.nearstaddress);
        groub = view.findViewById(R.id.groub);
        individual = view.findViewById(R.id.individual);
        company = view.findViewById(R.id.company);
        list = view.findViewById(R.id.list);
        reg = view.findViewById(R.id.reg);
        lin = view.findViewById(R.id.lin);
        progress = view.findViewById(R.id.progress);
        map = view.findViewById(R.id.map);
        Camera.activity = getActivity();
        bitmap.clear();
        Addimage addimage = new Addimage();
        addimage.check = false;
        bitmap.add(addimage);
        LinearLayoutManager layoutManage1 = new GridLayoutManager(getActivity(), 5);
        layoutManage1.setReverseLayout(true);
        list.setLayoutManager(layoutManage1);
        imageaddadapter = new Imageadd1adapter(bitmap, this);
        list.setAdapter(imageaddadapter);
        if (getString(R.string.lang).equals("ar")) {
            des.setGravity(Gravity.CENTER | Gravity.RIGHT);
            address.setGravity(Gravity.CENTER | Gravity.RIGHT);
            address2.setGravity(Gravity.CENTER | Gravity.RIGHT);
            nearstaddress.setGravity(Gravity.CENTER | Gravity.RIGHT);
        }
    }

    private void getdata() {
        String url = "pprofile/" + LoginSession.setdata(getActivity()).data.get(0).id;
        APIModel.getMethod(getActivity(), url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
                lin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Matgerprofile>() {
                }.getType();
                Matgerprofile data = new Gson().fromJson(responseString, dataType);
                des.setText(data.data.get(0).pio);
                address.setText(data.data.get(0).address);
                address2.setText(data.data.get(0).address2);
                nearstaddress.setText(data.data.get(0).nearest_place);
                if (data.data.get(0).member_type.equals("1")) {
                    individual.setChecked(true);
                    type = "1";
                } else if (data.data.get(0).member_type.equals("2")) {
                    company.setChecked(true);
                    type = "2";
                }
                for (int i = 0; i < data.data.get(0).images.size(); i++) {
                    Addimage addimage = new Addimage();
                    addimage.id = data.data.get(0).images.get(i).id;
                    addimage.image = data.data.get(0).images.get(i).image;
                    addimage.check = true;
                    bitmap.add(addimage);
                    imgs.add(data.data.get(0).images.get(i).image);
                }
                try {
                    if (!data.data.get(0).map_lat.equals("") && !data.data.get(0).map_lat.equals(null)) {
                        lat = data.data.get(0).map_lat;
                        lng = data.data.get(0).map_lng;
                    }
                } catch (Exception e) {

                }
                Collections.reverse(bitmap);
                Collections.reverse(imgs);
                imageaddadapter.notifyDataSetChanged();


            }
        });

    }

    public void getimage() {
        Camera.cameraOperation();
    }

    private void onclick() {
        groub.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.individual) {
                    type = "1";
                } else if (checkedId == R.id.company) {
                    type = "2";
                }
            }
        });
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
//
//// Start the autocomplete intent.
//                Intent intent = new Autocomplete.IntentBuilder(
//                        AutocompleteActivityMode.FULLSCREEN, fields)
//                        .setTypeFilter(TypeFilter.ADDRESS)
//                        .build(getActivity());
//                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                Intent i = new Intent(getActivity(), MaplocationActivity.class);
                if (!lat.equals("")) {
                    i.putExtra("lat", lat);
                    i.putExtra("lng", lng);
                }
                getActivity().startActivityForResult(i, 111);
            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reg(getActivity());
            }
        });
    }

    private void reg(final Context context) {
        progress.setVisibility(View.VISIBLE);
        RequestParams requestParams = new RequestParams();

        if (!des.getText().toString().equals("")) {
            requestParams.put("desc", des.getText().toString());
        }
        if (!lat.equals("")) {
            requestParams.put("map_lat", lat);
        }
        if (!lng.equals("")) {
            requestParams.put("map_lng", lng);
        }
        for (int i = 0; i < imgs.size(); i++) {
            try {
                if (!imgs.contains("http")) {
                    requestParams.put("image_file[" + i + "]", new File(imgs.get(i)));

                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
        if (!address.getText().toString().equals("")) {
            requestParams.put("address", address.getText().toString());
        }
        if (!address2.getText().toString().equals("")) {
            requestParams.put("address2", address2.getText().toString());
        }
        if (!nearstaddress.getText().toString().equals("")) {
            requestParams.put("nearest_place", nearstaddress.getText().toString());
        }
        if (!type.equals("")) {
            requestParams.put("member_type", type);
        }
        requestParams.put("provider_id", LoginSession.setdata(getActivity()).data.get(0).id);
        Log.e("dd", requestParams.toString());
        APIModel.postMethod((Activity) context, "editprovider", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(context, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        reg(context);
                    }
                });

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                loginFile = context.getSharedPreferences("LOGIN_FILE", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = loginFile.edit();
                editor.putString("json", responseString);
                editor.putInt("type", 2);
                editor.apply();
                LoginSession.setdata(context);
                Intent i = new Intent(getActivity(), HomeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                getActivity().finish();
                progress.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            lat = data.getStringExtra("lat");
            lng = data.getStringExtra("lng");
        }
    }
}
