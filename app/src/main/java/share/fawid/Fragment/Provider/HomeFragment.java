package share.fawid.Fragment.Provider;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;
import share.fawid.Activity.AboutusActivity;
import share.fawid.Activity.HelpActivity;
import share.fawid.Activity.NotificationsActivity;
import share.fawid.Activity.Provider.EditprofileActivity;
import share.fawid.Activity.Provider.HomeActivity;
import share.fawid.Activity.Provider.RegisterconfirmActivity;
import share.fawid.Activity.Provider.StatisticsActivity;
import share.fawid.Activity.TermsActivity;
import share.fawid.Api.APIModel;
import share.fawid.Helper.Language;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Login;
import share.fawid.Models.Matgerprofile;
import share.fawid.R;

import static share.fawid.Helper.LoginSession.loginFile;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    View view;
    private LinearLayout lin;
    private TextView num;
    private CircleImageView img;
    private TextView Visits;
    private TextView Review;
    private TextView contact;
    private CardView editprofile;
    private TextView name;
    private TextView cityPhoneTxt;
    private LinearLayout Statistics;
    private LinearLayout languages;
    private LinearLayout helpInfo;
    private LinearLayout terms;
    private LinearLayout DeactivateAccount;
    private LinearLayout aboutUs;
    private LinearLayout signOut;
    private ProgressBar progress;
    private CardView notification;
    private TextView txtactive;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_home2, container, false);
            initView();
            onclick();
        }
        return view;
    }

    private void initView() {
        lin = view.findViewById(R.id.lin);
        num = view.findViewById(R.id.num);
        img = view.findViewById(R.id.img);
        Visits = view.findViewById(R.id.Visits);
        Review = view.findViewById(R.id.Review);
        contact = view.findViewById(R.id.contact);
        editprofile = view.findViewById(R.id.editprofile);
        name = view.findViewById(R.id.name);
        cityPhoneTxt = view.findViewById(R.id.cityPhoneTxt);
        Statistics = view.findViewById(R.id.Statistics);
        languages = view.findViewById(R.id.languages);
        helpInfo = view.findViewById(R.id.helpInfo);
        terms = view.findViewById(R.id.terms);
        DeactivateAccount = view.findViewById(R.id.Deactivate_Account);
        aboutUs = view.findViewById(R.id.aboutUs);
        signOut = view.findViewById(R.id.signOut);
        progress = view.findViewById(R.id.progress);
        notification = view.findViewById(R.id.notification);
        txtactive = view.findViewById(R.id.txtactive);
    }

    private void getdata() {
        String url = "pprofile/" + LoginSession.setdata(getActivity()).data.get(0).id;
        APIModel.getMethod(getActivity(), url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                lin.setVisibility(View.GONE);
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
                lin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Matgerprofile>() {
                }.getType();
                Matgerprofile data = new Gson().fromJson(responseString, dataType);
                name.setText(data.data.get(0).full_name);
                Review.setText(data.data.get(0).reviews + "");
                contact.setText(data.data.get(0).clicks + "");
                String x = "";
                if (data.data.get(0).cites.size() > 0) {
                    x = x + data.data.get(0).cites.get(0).city_name + " | ";
                }
                cityPhoneTxt.setText(x + data.data.get(0).phone);
                try {
                    Picasso.get().load(data.data.get(0).image).into(img);
                } catch (Exception e) {

                }
                if (data.data.get(0).active_staus.equals("no")) {
                    txtactive.setText(getString(R.string.activate_account));
                } else {
                    txtactive.setText(getString(R.string.deactivate_account));
                }
            }
        });

    }

    private void active() {
        String url = "";
        if (LoginSession.setdata(getActivity()).data.get(0).active_staus.equals("no")) {
            url = "resendcode/";
        } else {
            url = "disactive/";
        }
        url = url + LoginSession.setdata(getActivity()).data.get(0).id;
        APIModel.getMethod(getActivity(), url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
                lin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.e("res", responseString);
                if (LoginSession.setdata(getActivity()).data.get(0).active_staus.equals("no")) {
                    Intent i = new Intent(getActivity(), RegisterconfirmActivity.class);
                    startActivity(i);
                } else {
                    loginFile = getActivity().getSharedPreferences("LOGIN_FILE", Context.MODE_PRIVATE);
                    String x = loginFile.getString("json", "");
                    Login data = null;
                    Type dataType = new TypeToken<Login>() {
                    }.getType();
                    data = new Gson().fromJson(x, dataType);
                    data.data.get(0).active_staus = "no";
                    SharedPreferences.Editor editor = loginFile.edit();
                    editor.putString("json", new Gson().toJson(data));
                    editor.putInt("type", 2);
                    editor.apply();
                    LoginSession.setdata(getActivity());
                    txtactive.setText(getString(R.string.activate_account));
                }


            }
        });

    }

    private void onclick() {
        languages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Language.showdialog(getActivity());
            }
        });
        editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), EditprofileActivity.class);
                startActivity(i);
            }
        });
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), NotificationsActivity.class);
                startActivity(i);
            }
        });
        helpInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), HelpActivity.class);
                startActivity(i);
            }
        });
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), TermsActivity.class);
                startActivity(i);
            }
        });
        aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AboutusActivity.class);
                startActivity(i);
            }
        });
        Statistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), StatisticsActivity.class);
                startActivity(i);
            }
        });
        DeactivateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(
                        getActivity());
                alert.setTitle(R.string.Error);
                if (LoginSession.setdata(getActivity()).data.get(0).active_staus.equals("no")) {
                    alert.setMessage(getString(R.string.do_active));
                } else {
                    alert.setMessage(getString(R.string.do_deactive));
                }
                alert.setPositiveButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alert.setNegativeButton(R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                active();
                            }
                        });
                alert.show();

            }
        });
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginSession.clearData(getActivity());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getdata();
        ((HomeActivity) getActivity()).linhome.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).linmerchant.setVisibility(View.INVISIBLE);
        ((HomeActivity) getActivity()).linwallet.setVisibility(View.INVISIBLE);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().finish();
                    return true;
                }
                return false;
            }
        });

    }
}
