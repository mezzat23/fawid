package share.fawid.Fragment.Customer;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import share.fawid.Activity.AboutusActivity;
import share.fawid.Activity.Customer.EditprofileActivity;
import share.fawid.Activity.Customer.HomeActivity;
import share.fawid.Activity.HelpActivity;
import share.fawid.Activity.LoginActivity;
import share.fawid.Activity.NotificationsActivity;
import share.fawid.Activity.TermsActivity;
import share.fawid.Helper.Language;
import share.fawid.Helper.LoginSession;
import share.fawid.R;


public class AccountFragment extends Fragment {

    View view;
    private FrameLayout btnNotifications;
    private TextView notiCount;
    private TextView name;
    private TextView cityPhoneTxt;
    private LinearLayout myAccount;
    private LinearLayout languages;
    private LinearLayout helpInfo;
    private LinearLayout myReviews;
    private LinearLayout pushNotification;
    private SwitchCompat switchNoti;
    private LinearLayout terms;
    private LinearLayout aboutUs;
    private LinearLayout signOut;
    private TextView txtsign;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_account, container, false);
            initView();
            onclick();
        }
        return view;
    }

    private void initView() {
        btnNotifications = view.findViewById(R.id.btnNotifications);
        notiCount = view.findViewById(R.id.notiCount);
        name = view.findViewById(R.id.name);
        cityPhoneTxt = view.findViewById(R.id.cityPhoneTxt);
        myAccount = view.findViewById(R.id.myAccount);
        languages = view.findViewById(R.id.languages);
        helpInfo = view.findViewById(R.id.helpInfo);
        myReviews = view.findViewById(R.id.myReviews);
        pushNotification = view.findViewById(R.id.pushNotification);
        switchNoti = view.findViewById(R.id.switchNoti);
        terms = view.findViewById(R.id.terms);
        aboutUs = view.findViewById(R.id.aboutUs);
        signOut = view.findViewById(R.id.signOut);
        txtsign = view.findViewById(R.id.txtsign);
        switchNoti.setChecked(LoginSession.getnotifications(getActivity()));
    }

    private void onclick() {
        switchNoti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginSession.addnotifications(getActivity(),!LoginSession.getnotifications(getActivity()));
                switchNoti.setChecked(LoginSession.getnotifications(getActivity()));
            }
        });
        myAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginSession.isLogin) {
                    Intent i = new Intent(getActivity(), EditprofileActivity.class);
                    startActivity(i);
                } else {
                    LoginSession.plsGoLogin(getActivity());
                }
            }
        });
        languages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Language.showdialog(getActivity());
            }
        });
        btnNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), NotificationsActivity.class);
                startActivity(i);
            }
        });
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginSession.isLogin) {
                    LoginSession.clearData(getActivity());
                } else {
                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);
                }
            }
        });
        helpInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), HelpActivity.class);
                startActivity(i);
            }
        });
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), TermsActivity.class);
                startActivity(i);
            }
        });
        aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AboutusActivity.class);
                startActivity(i);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (LoginSession.isLogin) {
            txtsign.setText(getString(R.string.sign_out));
            name.setText(LoginSession.setdata(getActivity()).data.get(0).full_name);
            cityPhoneTxt.setText(LoginSession.setdata(getActivity()).data.get(0).city_name + " | " + LoginSession.setdata(getActivity()).data.get(0).phone);
        } else {
            name.setVisibility(View.GONE);
            cityPhoneTxt.setVisibility(View.GONE);
            txtsign.setText(getString(R.string.login));
        }
        ((HomeActivity) getActivity()).linhome.setVisibility(View.INVISIBLE);
        ((HomeActivity) getActivity()).linaccount.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).linsave.setVisibility(View.INVISIBLE);
        ((HomeActivity) getActivity()).linsearch.setVisibility(View.INVISIBLE);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    FragmentManager fm = getFragmentManager();
                    fm.popBackStack();
                    return true;
                }
                return false;
            }
        });

    }
}
