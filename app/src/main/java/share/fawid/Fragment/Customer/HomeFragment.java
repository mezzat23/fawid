package share.fawid.Fragment.Customer;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import me.relex.circleindicator.CircleIndicator;
import share.fawid.Activity.Customer.HomeActivity;
import share.fawid.Activity.Customer.Searchresults1Activity;
import share.fawid.Adapter.Catogaryadapter;
import share.fawid.Adapter.Recentadapter;
import share.fawid.Adapter.Slideshomeadapter;
import share.fawid.Api.APIModel;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Home;
import share.fawid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    View view;
    private LinearLayout lin;
    private TextView name;
    private TextView city;
    private RecyclerView cat;
    private ViewPager pager;
    private CircleIndicator indicator;
    private RecyclerView list;
    private ProgressBar progress;
    Catogaryadapter catogaryadapter;
    Slideshomeadapter slideshomeadapter;
    Recentadapter recentadapter;
    ArrayList<Home.CategoriesBean> categoriesBeans = new ArrayList<>();
    ArrayList<Home.RecentlyBean> recentlyBeans = new ArrayList<>();
    ArrayList<Home.SlidersBean> slidersBeans = new ArrayList<>();
    private TextView seeall;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_home, container, false);
            initView();
            gethome();
            onclick();
        }
        return view;
    }

    private void initView() {
        lin = view.findViewById(R.id.lin);
        name = view.findViewById(R.id.name);
        city = view.findViewById(R.id.city);
        cat = view.findViewById(R.id.cat);
        pager = view.findViewById(R.id.pager);
        indicator = view.findViewById(R.id.indicator);
        list = view.findViewById(R.id.list);
        progress = view.findViewById(R.id.progress);
        slideshomeadapter = new Slideshomeadapter(getActivity(), slidersBeans);
        pager.setAdapter(slideshomeadapter);
        indicator.setViewPager(pager);
        slideshomeadapter.registerDataSetObserver(indicator.getDataSetObserver());
        LinearLayoutManager layoutManage = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        cat.setLayoutManager(layoutManage);
        catogaryadapter = new Catogaryadapter(categoriesBeans);
        cat.setAdapter(catogaryadapter);
        LinearLayoutManager layoutManage1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        list.setLayoutManager(layoutManage1);
        recentadapter = new Recentadapter(recentlyBeans);
        list.setAdapter(recentadapter);
        if (getString(R.string.lang).equals("ar")) {
            pager.setRotationY(180);
        }
        seeall = view.findViewById(R.id.seeall);
    }

    private void onclick() {
        seeall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Searchresults1Activity.class);
                i.putExtra("json",new Gson().toJson(recentlyBeans));
                i.putExtra("name",getString(R.string.recent_added));
                startActivity(i);
            }
        });
    }

    public void gethome() {
        String url = "home";
//        if (cityid > 0) {
//            url = url + "?cityId=" + cityid;
//        }
        APIModel.getMethod(getActivity(), url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
                lin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Home>() {
                }.getType();
                Home data = new Gson().fromJson(responseString, dataType);
                slidersBeans.clear();
                categoriesBeans.clear();
                recentlyBeans.clear();
                slidersBeans.addAll(data.sliders);
                categoriesBeans.addAll(data.categories);
                recentlyBeans.addAll(data.recently);
                catogaryadapter.notifyDataSetChanged();
                recentadapter.notifyDataSetChanged();
                slideshomeadapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (LoginSession.isLogin) {
            name.setText(String.format(getString(R.string.hi), LoginSession.setdata(getActivity()).data.get(0).full_name));
            city.setText(LoginSession.setdata(getActivity()).data.get(0).city_name);
        } else {

        }
        ((HomeActivity) getActivity()).linhome.setVisibility(View.VISIBLE);
        ((HomeActivity) getActivity()).linaccount.setVisibility(View.INVISIBLE);
        ((HomeActivity) getActivity()).linsave.setVisibility(View.INVISIBLE);
        ((HomeActivity) getActivity()).linsearch.setVisibility(View.INVISIBLE);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().finish();
                    return true;
                }
                return false;
            }
        });

    }
}
