package share.fawid.Fragment.Customer;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import share.fawid.Activity.Customer.HomeActivity;
import share.fawid.Activity.Customer.Searchresults1Activity;
import share.fawid.Activity.Customer.SearchresultsActivity;
import share.fawid.Adapter.Catogarysearchadapter;
import share.fawid.Adapter.Cityadapter;
import share.fawid.Adapter.Recentadapter;
import share.fawid.Api.APIModel;
import share.fawid.Models.Catogaries;
import share.fawid.Models.City;
import share.fawid.Models.Home;
import share.fawid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {
    View view;
    private TextView city;
    private EditText name;
    private LinearLayout service;
    private LinearLayout city1;
    private EditText des;
    private CardView search;
    private TextView seeall;
    private RecyclerView list1;
    private TextView txtservice;
    private TextView txtcity;
    public int city_id = 0, cat_id = 0;
    private CardView back;
    private EditText edit;
    private RecyclerView list;
    private CardView save;
    Dialog dialog;
    Cityadapter cityadapter;
    ArrayList<City.DataBean> cities = new ArrayList<>();
    ArrayList<City.DataBean> cities1 = new ArrayList<>();
    Catogarysearchadapter catogarysearchadapter;
    ArrayList<Catogaries.DataBean> catogaries = new ArrayList<>();
    ArrayList<Catogaries.DataBean> catogaries1 = new ArrayList<>();
    private ProgressBar progress;
    Recentadapter recentadapter ;
    ArrayList<Home.RecentlyBean> recentlyBeans = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_search, container, false);
            initView();
            onclick();
            gethome();
        }
        return view;
    }

    private void initView() {
        city = view.findViewById(R.id.city);
        name = view.findViewById(R.id.name);
        service = view.findViewById(R.id.service);
        city1 = view.findViewById(R.id.city1);
        des = view.findViewById(R.id.des);
        search = view.findViewById(R.id.search);
        seeall = view.findViewById(R.id.seeall);
        list1 = view.findViewById(R.id.list);
        txtservice = view.findViewById(R.id.txtservice);
        txtcity = view.findViewById(R.id.txtcity);
        progress = view.findViewById(R.id.progress);
        LinearLayoutManager layoutManage1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        list1.setLayoutManager(layoutManage1);
        recentadapter = new Recentadapter(recentlyBeans);
        list1.setAdapter(recentadapter);
    }

    private void onclick() {
        city1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getcity();
            }
        });
        service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getcat();
            }
        });
        seeall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                seeall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getActivity(), Searchresults1Activity.class);
                        i.putExtra("json",new Gson().toJson(recentlyBeans));
                        i.putExtra("name",getString(R.string.recent_search));
                        startActivity(i);
                    }
                });
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), SearchresultsActivity.class);
                i.putExtra("city", city_id);
                i.putExtra("cat", cat_id);
                i.putExtra("word", name.getText().toString());
                startActivity(i);
            }
        });
    }

    private void getcity() {
        APIModel.getMethod(getActivity(), "cites", new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<City>() {
                }.getType();
                City data = new Gson().fromJson(responseString, dataType);
                cities.clear();
                cities1.clear();
                cities.addAll(data.data);
                cities1.addAll(data.data);
                showdialogcity();

            }
        });
    }

    private void getcat() {
        APIModel.getMethod(getActivity(), "categories", new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Catogaries>() {
                }.getType();
                Catogaries data = new Gson().fromJson(responseString, dataType);
                catogaries.clear();
                catogaries1.clear();
                catogaries.addAll(data.data);
                catogaries1.addAll(data.data);
                showdialogcatogres();

            }
        });
    }

    private void showdialogcity() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_city);
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int width = display.getWidth();
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        back = dialog.findViewById(R.id.back);
        list = dialog.findViewById(R.id.list);
        save = dialog.findViewById(R.id.save);
        edit = dialog.findViewById(R.id.edit);
        LinearLayoutManager layoutManage1 = new LinearLayoutManager(getActivity());
        list.setLayoutManager(layoutManage1);
        cityadapter = new Cityadapter(cities, getActivity(), 0);
        list.setAdapter(cityadapter);
        if (city_id > 0) {
            for (int i = 0; i < cities.size(); i++) {
                if (city_id == Integer.parseInt(cities.get(i).id)) {
                    cityadapter.pos = i;
                }
            }
        }
        cityadapter.notifyDataSetChanged();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit.getText().toString().equals("")) {
                    cities.clear();
                    cities.addAll(cities1);
                    cityadapter.notifyDataSetChanged();
                } else {
                    cities.clear();
                    for (int i = 0; i < cities1.size(); i++) {
                        if (cities1.get(i).name.contains(edit.getText().toString())) {
                            cities.add(cities1.get(i));
                        }
                    }
                    cityadapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                city_id = Integer.parseInt(cities.get(cityadapter.pos).id);
                txtcity.setText(cities.get(cityadapter.pos).name);
                city.setText(cities.get(cityadapter.pos).name);
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void showdialogcatogres() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_cat);
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int width = display.getWidth();
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        back = dialog.findViewById(R.id.back);
        list = dialog.findViewById(R.id.list);
        save = dialog.findViewById(R.id.save);
        edit = dialog.findViewById(R.id.edit);
        LinearLayoutManager layoutManage1 = new LinearLayoutManager(getActivity());
        list.setLayoutManager(layoutManage1);
        catogarysearchadapter = new Catogarysearchadapter(catogaries, getActivity());
        list.setAdapter(catogarysearchadapter);
        if (cat_id > 0) {
            for (int i = 0; i < catogaries.size(); i++) {
                if (cat_id == Integer.parseInt(catogaries.get(i).id)) {
                    catogarysearchadapter.pos = i;
                }
            }
        }
        catogarysearchadapter.notifyDataSetChanged();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit.getText().toString().equals("")) {
                    catogaries.clear();
                    catogaries.addAll(catogaries1);
                    catogarysearchadapter.notifyDataSetChanged();
                } else {
                    catogaries.clear();
                    for (int i = 0; i < catogaries1.size(); i++) {
                        if (catogaries1.get(i).title.contains(edit.getText().toString())) {
                            catogaries.add(catogaries1.get(i));
                        }
                    }
                    catogarysearchadapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cat_id = Integer.parseInt(catogaries.get(catogarysearchadapter.pos).id);
                txtservice.setText(catogaries.get(catogarysearchadapter.pos).title);
                dialog.dismiss();
            }
        });


        dialog.show();
    }
    public void gethome() {
        String url = "home";
//        if (cityid > 0) {
//            url = url + "?cityId=" + cityid;
//        }
        APIModel.getMethod(getActivity(), url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Home>() {
                }.getType();
                Home data = new Gson().fromJson(responseString, dataType);
                recentlyBeans.clear();
                recentlyBeans.addAll(data.recently);
                recentadapter.notifyDataSetChanged();
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity)getActivity()).linhome.setVisibility(View.INVISIBLE);
        ((HomeActivity)getActivity()).linaccount.setVisibility(View.INVISIBLE);
        ((HomeActivity)getActivity()).linsave.setVisibility(View.INVISIBLE);
        ((HomeActivity)getActivity()).linsearch.setVisibility(View.VISIBLE);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    FragmentManager fm = getFragmentManager();
                    fm.popBackStack();
                    return true;
                }
                return false;
            }
        });

    }

}
