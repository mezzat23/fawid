package share.fawid.Fragment.Customer;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import share.fawid.Activity.Customer.HomeActivity;
import share.fawid.Adapter.Favadapter;
import share.fawid.Api.APIModel;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Catogarydata;
import share.fawid.Models.Home;
import share.fawid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SavedFragment extends Fragment {
    View view;
    private EditText edit;
    private RecyclerView list;
    private ProgressBar progress;
    ArrayList<Home.RecentlyBean> recentlyBeans = new ArrayList<>();
    Favadapter recentadapter ;
    public int page = 0;
    private boolean mLoading = false;
    LinearLayoutManager layoutManage;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_saved, container, false);
            initView();
            getdata();
            makescroll();
        }
        return view;
    }

    private void initView() {
        edit = view.findViewById(R.id.edit);
        list = view.findViewById(R.id.list);
        progress = view.findViewById(R.id.progress);
        layoutManage = new LinearLayoutManager(getActivity());
        list.setLayoutManager(layoutManage);
        recentadapter = new Favadapter(recentlyBeans);
        list.setAdapter(recentadapter);
    }
    private void makescroll(){
        list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int totalItemCount = layoutManage.getItemCount();
                int visibleItemCount = layoutManage.findLastVisibleItemPosition();
                if (!mLoading && visibleItemCount >= totalItemCount - 1 && page > 0) {
                    mLoading = true;
                    getdata();
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }
    private void getdata(){
        String url = "members/"+ LoginSession.setdata(getActivity()).data.get(0).id + "/" + page ;

        APIModel.getMethod(getActivity(), url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Catogarydata>() {
                }.getType();
                Catogarydata data = new Gson().fromJson(responseString, dataType);
                if (page == 0) {
                    recentlyBeans.clear();
                }
                if (data.data.size() > 0) {
                    page++;
                    mLoading = false;
                }
                recentlyBeans.addAll(data.data);
                recentadapter.notifyDataSetChanged();
            }
        });

    }
    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity)getActivity()).linhome.setVisibility(View.INVISIBLE);
        ((HomeActivity)getActivity()).linaccount.setVisibility(View.INVISIBLE);
        ((HomeActivity)getActivity()).linsave.setVisibility(View.VISIBLE);
        ((HomeActivity)getActivity()).linsearch.setVisibility(View.INVISIBLE);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    FragmentManager fm = getFragmentManager();
                    fm.popBackStack();
                    return true;
                }
                return false;
            }
        });

    }
}
