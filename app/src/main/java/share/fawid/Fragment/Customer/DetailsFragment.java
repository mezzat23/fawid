package share.fawid.Fragment.Customer;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;

import cz.msebera.android.httpclient.Header;
import share.fawid.Api.APIModel;
import share.fawid.Helper.GPSTracker;
import share.fawid.Helper.Gdata;
import share.fawid.Helper.OpenGps;
import share.fawid.Models.Matgerprofile;
import share.fawid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = "dd";
    public GoogleMap mMap;
    private Marker mMarcadorActual;
    private FusedLocationProviderClient mFusedLocationClient;
    View view;
    private TextView title;
    private TextView des;
    private TextView experience;
    private TextView services;
    private TextView Branch;
    private TextView detailsloc;
    private LinearLayout lin;
    private ProgressBar progress;
    private int REQUEST_LOCATION = 123;
    Matgerprofile data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_details, container, false);
            initView();
            onclick();
        }
        return view;
    }

    private void initView() {
        title = view.findViewById(R.id.title);
        des = view.findViewById(R.id.des);
        experience = view.findViewById(R.id.experience);
        services = view.findViewById(R.id.services);
        Branch = view.findViewById(R.id.Branch);
        detailsloc = view.findViewById(R.id.detailsloc);
        lin = view.findViewById(R.id.lin);
        progress = view.findViewById(R.id.progress);
        SupportMapFragment mapFragment =
                (SupportMapFragment)
                        getChildFragmentManager().findFragmentById(R.id.workerMap);
        mapFragment.getMapAsync(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        OpenGps.checkgps(getActivity());
        setRequestLocation();
    }

    private void getdata() {
        String url = "pprofile/" + getActivity().getIntent().getStringExtra("id");
        APIModel.getMethod(getActivity(), url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
                lin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Matgerprofile>() {
                }.getType();
                data = new Gson().fromJson(responseString, dataType);
                title.setText(getString(R.string.welcome_in) + " " + data.data.get(0).full_name);
                des.setText(data.data.get(0).pio);
                services.setText(data.data.get(0).count_cats);
                experience.setText(data.data.get(0).cites.size() + "");
                Branch.setText(data.data.get(0).reviews + "");
//                    des.setText( data.data.get(0).pio);
                try {
                    Gdata.getAddress1(Double.parseDouble(data.data.get(0).map_lat), Double.parseDouble(data.data.get(0).map_lng), detailsloc);
                    LatLng latLng = new LatLng(Double.parseDouble(data.data.get(0).map_lat), Double.parseDouble(data.data.get(0).map_lng));
                    BitmapDescriptor map_client = BitmapDescriptorFactory.fromResource(R.drawable.fawid_logo);

                    MarkerOptions options = new MarkerOptions()
                            .position(latLng)
                            ;
                    Marker marker = mMap.addMarker(options);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));//                                showmap(cat_id);

                } catch (Exception e) {
                    if (data.data.get(0).cites.size()>0){
                        detailsloc.setText(data.data.get(0).cites.get(0).city_name);
                    }
                    view.findViewById(R.id.workerMap).setVisibility(View.GONE);
                }

            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.clear();
        mMap.setMyLocationEnabled(true);
//        mMap.setPadding(40,40,40,40);
//        mFusedLocationClient.getLastLocation()
//                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
//                    @Override
//                    public void onSuccess(Location location) {
//                        // Got last known location. In some rare situations this can be null.
//                        if (location != null) {
//                            mMap.clear();
//                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//                            MarkerOptions options = new MarkerOptions()
//                                    .position(latLng)
//                                    .title("my location");
//
//// move the camera zoom to the location
//                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));//                                showmap(cat_id);
//                            // Logic to handle location object
//                        }
//                    }
//                });

    }

    private void setRequestLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),

                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,

                            Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION);

        } else {


        }
    }

    @Override

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == REQUEST_LOCATION) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mMap.setPadding(20, (120), 0, 0);
                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

                System.out.println("Location permissions granted, starting location");
                GPSTracker gpsTracker = new GPSTracker(getActivity());
                LatLng latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                MarkerOptions options = new MarkerOptions()
                        .position(latLng)
                        .title("my location");

// move the camera zoom to the location
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

//                LocationManager.enableLocationSupport(getApplicationContext());

            }

        }

    }

    private void onclick() {

    }

    @Override
    public void onResume() {
        super.onResume();
        getdata();
    }
}
