package share.fawid.Fragment.Customer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import share.fawid.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificaionFragment extends Fragment {

    public NotificaionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notificaion, container, false);
    }
}
