package share.fawid.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import share.fawid.Activity.Provider.RegisterconfirmActivity;
import share.fawid.Activity.Provider.RegisterproviderActivity;
import share.fawid.Activity.TermsActivity;
import share.fawid.Adapter.Cityadapter;
import share.fawid.Api.APIModel;
import share.fawid.Helper.Dialogs;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.City;
import share.fawid.R;

import static share.fawid.Helper.LoginSession.loginFile;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterclientFragment extends Fragment {
    View view;
    private EditText fullname;
    private EditText phone;
    private LinearLayout city;
    private EditText password;
    private CardView reg;
    private CardView regprovider;
    private ProgressBar progress;
    public TextView txtcity;
    public int city_id = 0;
    private CardView back;
    private EditText edit;
    private RecyclerView list;
    private CardView save;
    Dialog dialog;
    Cityadapter cityadapter;
    ArrayList<City.DataBean> cities = new ArrayList<>();
    ArrayList<City.DataBean> cities1 = new ArrayList<>();
    private CheckBox check;
    private TextView terms;
    boolean is_check = false;
    private ImageView imgpass;
int x = 0 ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_registerclient, container, false);
            initView();
            onclick();
        }
        return view;
    }

    private void initView() {
        fullname = view.findViewById(R.id.fullname);
        phone = view.findViewById(R.id.phone);
        city = view.findViewById(R.id.city);
        password = view.findViewById(R.id.password);
        reg = view.findViewById(R.id.reg);
        regprovider = view.findViewById(R.id.regprovider);
        progress = view.findViewById(R.id.progress);
        txtcity = view.findViewById(R.id.txtcity);
        if (getString(R.string.lang).equals("ar")) {
            fullname.setGravity(Gravity.CENTER | Gravity.RIGHT);
            phone.setGravity(Gravity.CENTER | Gravity.RIGHT);
            password.setGravity(Gravity.CENTER | Gravity.RIGHT);
        }
        check = view.findViewById(R.id.check);
        terms = view.findViewById(R.id.terms);
        imgpass = view.findViewById(R.id.imgpass);
    }

    private void onclick() {
        imgpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (x == 0){
                    password.setTransformationMethod(null);
                    imgpass.setBackgroundResource(R.drawable.ic_eye);
                    x = 1 ;
                }else {
                    x = 0 ;
                    password.setTransformationMethod(new PasswordTransformationMethod());
                    imgpass.setBackgroundResource(R.drawable.ic_eye1);
                }
            }
        });
        regprovider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), RegisterproviderActivity.class);
                startActivity(i);
            }
        });
        check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                is_check = isChecked;
            }
        });
        city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getcity();
            }
        });
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), TermsActivity.class);
                startActivity(i);
            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean is_good = true;
                if (fullname.getText().toString().trim().equals("")) {
                    fullname.setError(getString(R.string.required));
                    is_good = false;
                }
                if (is_check == false) {
                    Dialogs.showToast(getString(R.string.terms_agree), getActivity());
                    is_good = false;
                }
                if (phone.getText().toString().trim().equals("")) {
                    phone.setError(getString(R.string.required));
                    is_good = false;
                }
                if (phone.getText().length() < 9) {
                    phone.setError(getString(R.string.number_9digits));
                    is_good = false;
                }
                if (phone.getText().length() > 10) {
                    phone.setError(getString(R.string.number_9digits));
                    is_good = false;
                }
                if (phone.getText().length() == 10 && !phone.getText().toString().substring(0, 1).equals("0")) {
                    phone.setError(getString(R.string.number_9digits));
                    is_good = false;
                }
                if (password.getText().toString().trim().equals("")) {
                    password.setError(getString(R.string.required));
                    is_good = false;
                }
                if (city_id == 0) {
                    Dialogs.showToast(getString(R.string.select_city), getActivity());
                    is_good = false;
                }
                if (is_good) {
                    reg(getActivity());
                }
            }
        });
    }

    private void getcity() {
        APIModel.getMethod(getActivity(), "cites", new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<City>() {
                }.getType();
                City data = new Gson().fromJson(responseString, dataType);
                cities.clear();
                cities1.clear();
                cities.addAll(data.data);
                cities1.addAll(data.data);
                showdialogcity();

            }
        });
    }

    private void showdialogcity() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_city);
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int width = display.getWidth();
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        back = dialog.findViewById(R.id.back);
        list = dialog.findViewById(R.id.list);
        save = dialog.findViewById(R.id.save);
        edit = dialog.findViewById(R.id.edit);
        LinearLayoutManager layoutManage1 = new LinearLayoutManager(getActivity());
        list.setLayoutManager(layoutManage1);
        cityadapter = new Cityadapter(cities, getActivity(), 0);
        list.setAdapter(cityadapter);
        if (city_id > 0) {
            for (int i = 0; i < cities.size(); i++) {
                if (city_id == Integer.parseInt(cities.get(i).id)) {
                    cityadapter.pos = i;
                }
            }
        }
        cityadapter.notifyDataSetChanged();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edit.getText().toString().equals("")) {
                    cities.clear();
                    cities.addAll(cities1);
                    cityadapter.notifyDataSetChanged();
                } else {
                    cities.clear();
                    for (int i = 0; i < cities1.size(); i++) {
                        if (cities1.get(i).name.contains(edit.getText().toString())) {
                            cities.add(cities1.get(i));
                        }
                    }
                    cityadapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                city_id = Integer.parseInt(cities.get(cityadapter.pos).id);
                txtcity.setText(cities.get(cityadapter.pos).name);
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private void reg(final Context context) {
        progress.setVisibility(View.VISIBLE);
        RequestParams requestParams = new RequestParams();
        requestParams.put("phone", "966" + phone.getText().toString());
        requestParams.put("password", password.getText().toString());
        requestParams.put("full_name", fullname.getText().toString());
        requestParams.put("city", city_id);
        requestParams.put("platform", "2");
        requestParams.put("registrationid", FirebaseInstanceId.getInstance().getToken());
        Log.e("dd", requestParams.toString());
        APIModel.postMethod((Activity) context, "members", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                APIModel.handleFailure(context, statusCode, responseString, new APIModel.RefreshTokenListener() {
                    @Override
                    public void onRefresh() {
                        reg(context);
                    }
                });

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                loginFile = context.getSharedPreferences("LOGIN_FILE", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = loginFile.edit();
                editor.putString("json", responseString);
                editor.putInt("type", 1);
                editor.apply();
                LoginSession.setdata(context);
                Intent i = new Intent(getActivity(), RegisterconfirmActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                getActivity().finish();
                progress.setVisibility(View.GONE);
            }
        });
    }
}
