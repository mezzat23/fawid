package share.fawid.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Rates {

    /**
     * status : 200
     * error : false
     * message : تم بنجاح
     * data : [{"all_comments":3,"avarage":4.333333333333333,"5star_num":1,"5star_percent":33.33333333333333,"4star_num":2,"4star_percent":66.66666666666666,"3star_num":0,"3star_percent":0,"2star_num":0,"2star_percent":0,"1star_num":0,"1star_percent":0,"comments":[{"comment":"شركة ممتازة انصح بالتعامل معها","rate":"4","created":"2020-05-19 05:06:57"},{"comment":"good work","rate":"4","created":"2020-05-19 05:21:31"},{"comment":"شركة مميزة تستحق التجربة","rate":"5","created":"2020-06-10 06:14:24"}]}]
     */

    public String status;
    public boolean error;
    public String message;
    public List<DataBean> data = new ArrayList<>();

    public static class DataBean {
        /**
         * all_comments : 3
         * avarage : 4.333333333333333
         * 5star_num : 1
         * 5star_percent : 33.33333333333333
         * 4star_num : 2
         * 4star_percent : 66.66666666666666
         * 3star_num : 0
         * 3star_percent : 0
         * 2star_num : 0
         * 2star_percent : 0
         * 1star_num : 0
         * 1star_percent : 0
         * comments : [{"comment":"شركة ممتازة انصح بالتعامل معها","rate":"4","created":"2020-05-19 05:06:57"},{"comment":"good work","rate":"4","created":"2020-05-19 05:21:31"},{"comment":"شركة مميزة تستحق التجربة","rate":"5","created":"2020-06-10 06:14:24"}]
         */

        public String all_comments;
        public String avarage;
        @SerializedName("5star_num")
        public String _$5star_num;
        @SerializedName("5star_percent")
        public String _$5star_percent;
        @SerializedName("4star_num")
        public String _$4star_num;
        @SerializedName("4star_percent")
        public String _$4star_percent;
        @SerializedName("3star_num")
        public String _$3star_num;
        @SerializedName("3star_percent")
        public String _$3star_percent;
        @SerializedName("2star_num")
        public String _$2star_num;
        @SerializedName("2star_percent")
        public String _$2star_percent;
        @SerializedName("1star_num")
        public String _$1star_num;
        @SerializedName("1star_percent")
        public String _$1star_percent;
        public List<CommentsBean> comments;

        public static class CommentsBean {
            /**
             * comment : شركة ممتازة انصح بالتعامل معها
             * rate : 4
             * created : 2020-05-19 05:06:57
             */

            public String comment;
            public String rate;
            public String created;
        }
    }
}
