package share.fawid.Models;

import java.util.ArrayList;
import java.util.List;

public class Matgerprofile {


    /**
     * status : 200
     * error : false
     * message : بيانات
     * data : [{"id":"153","full_name":"Mahmoud Ezzat","phone":"966541234789","clicks":0,"reviews":0,"type":"1","active_staus":"yes","image":"https://fawid.net/files/1591633090Title (10).jpg","created":"2020-06-08 16:18:10","address":"uyuyuyu","address2":"uuuuuu","nearest_place":"uuyuyu","email":"mmezzat23@gmail.com","facebook":null,"twitter":null,"linkedin":null,"snapchat":null,"youtube":null,"whatsapp":null,"insatgram":null,"website":null,"skype":null,"tambler":null,"facebookuser":"","twitteruser":"","linkedinuser":"","snapchatuser":"","youtubeuser":"","whatsappuser":"","insatgramuser":"","skypeuser":"","tambleruser":"","member_type":"1","mobile":null,"pio":"uhuhuu","map_lat":"30.0965417","map_lng":"31.3267461","count_cats":1,"cites":[{"city_name":"الخبر","city_id":"1513"},{"city_name":"الاحساء","city_id":"1512"},{"city_name":"الدمام","city_id":"1511"},{"city_name":"جدة","city_id":"1510"},{"city_name":"المدينة المنورة","city_id":"1509"},{"city_name":"الرياض","city_id":"1507"},{"city_name":"مكة","city_id":"1508"}],"main_cats":[{"main_cat_name":"السباكة","cat_id":"1","subcats":[{"id":"112","mem_id":"153","subcat":"30","created":"2020-06-08 16:18:10"}]}],"images":[{"id":"134","image":"https://fawid.net/files/f5eea47916b4652dbc525a2c4310cf29.jpg"},{"id":"135","image":"https://fawid.net/files/81feba4f4b83b2910e3952a28e60ae9c.jpg"}]}]
     */

    public String status;
    public boolean error;
    public String message;
    public List<DataBean> data = new ArrayList<>();

    public static class DataBean {
        /**
         * id : 153
         * full_name : Mahmoud Ezzat
         * phone : 966541234789
         * clicks : 0
         * reviews : 0
         * type : 1
         * active_staus : yes
         * image : https://fawid.net/files/1591633090Title (10).jpg
         * created : 2020-06-08 16:18:10
         * address : uyuyuyu
         * address2 : uuuuuu
         * nearest_place : uuyuyu
         * email : mmezzat23@gmail.com
         * facebook : null
         * twitter : null
         * linkedin : null
         * snapchat : null
         * youtube : null
         * whatsapp : null
         * insatgram : null
         * website : null
         * skype : null
         * tambler : null
         * facebookuser :
         * twitteruser :
         * linkedinuser :
         * snapchatuser :
         * youtubeuser :
         * whatsappuser :
         * insatgramuser :
         * skypeuser :
         * tambleruser :
         * member_type : 1
         * mobile : null
         * pio : uhuhuu
         * map_lat : 30.0965417
         * map_lng : 31.3267461
         * count_cats : 1
         * cites : [{"city_name":"الخبر","city_id":"1513"},{"city_name":"الاحساء","city_id":"1512"},{"city_name":"الدمام","city_id":"1511"},{"city_name":"جدة","city_id":"1510"},{"city_name":"المدينة المنورة","city_id":"1509"},{"city_name":"الرياض","city_id":"1507"},{"city_name":"مكة","city_id":"1508"}]
         * main_cats : [{"main_cat_name":"السباكة","cat_id":"1","subcats":[{"id":"112","mem_id":"153","subcat":"30","created":"2020-06-08 16:18:10"}]}]
         * images : [{"id":"134","image":"https://fawid.net/files/f5eea47916b4652dbc525a2c4310cf29.jpg"},{"id":"135","image":"https://fawid.net/files/81feba4f4b83b2910e3952a28e60ae9c.jpg"}]
         */

        public String id;
        public String full_name;
        public String phone;
        public String clicks;
        public String reviews;
        public String type;
        public String active_staus;
        public String image;
        public String created;
        public String address;
        public String address2;
        public String nearest_place;
        public String email;
        public String facebook;
        public String twitter;
        public String linkedin;
        public String snapchat;
        public String youtube;
        public String whatsapp;
        public String insatgram;
        public String website;
        public String skype;
        public String tambler;
        public String facebookuser;
        public String twitteruser;
        public String linkedinuser;
        public String snapchatuser;
        public String youtubeuser;
        public String whatsappuser;
        public String insatgramuser;
        public String skypeuser;
        public String tambleruser;
        public String member_type;
        public String mobile;
        public String pio;
        public String map_lat;
        public String map_lng;
        public String count_cats;
        public List<CitesBean> cites = new ArrayList<>();
        public List<MainCatsBean> main_cats = new ArrayList<>();
        public List<ImagesBean> images = new ArrayList<>();

        public static class CitesBean {
            /**
             * city_name : الخبر
             * city_id : 1513
             */

            public String city_name;
            public String city_id;
        }

        public static class MainCatsBean {
            /**
             * main_cat_name : السباكة
             * cat_id : 1
             * subcats : [{"id":"112","mem_id":"153","subcat":"30","created":"2020-06-08 16:18:10"}]
             */

            public String main_cat_name;
            public String cat_id;
            public List<SubcatsBean> subcats = new ArrayList<>();

            public static class SubcatsBean {
                /**
                 * id : 112
                 * mem_id : 153
                 * subcat : 30
                 * created : 2020-06-08 16:18:10
                 */

                public String id;
                public String mem_id;
                public String subcat;
                public String created;
            }
        }

        public static class ImagesBean {
            /**
             * id : 134
             * image : https://fawid.net/files/f5eea47916b4652dbc525a2c4310cf29.jpg
             */

            public int id;
            public String image;
        }
    }
}
