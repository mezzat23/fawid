package share.fawid.Models;

import java.util.ArrayList;
import java.util.List;

public class Catogaries {

    /**
     * status : 200
     * error : false
     * message : الاقسام
     * data : [{"title":"السباكة","id":"1","image":"https://fawid.net/files/4bc1ba718bbe3cda3a5f2485caaae189.jpg","sub_cats":[{"title":"تجهيز الحفلات والمناسبات","id":"30","image":""},{"title":"حاويات مخلفات البناء","id":"31","image":""},{"title":"السطحات ونقليات السيارات","id":"32","image":""}]},{"title":"نقل العفش","id":"20","image":"","sub_cats":[]},{"title":"الدواء","id":"4","image":"https://fawid.net/files/dad125f8981b561cee35a164facf9c0b.png","sub_cats":[]},{"title":"الشحن","id":"5","image":"https://fawid.net/files/f9007a687c01712a886c131f0a3aca55.png","sub_cats":[]},{"title":"البرمجيات","id":"6","image":"https://fawid.net/files/e971809ba353fe7ec6cc2bcd139bc296.png","sub_cats":[]},{"title":"صيانة وفحص السيارات","id":"7","image":"https://fawid.net/files/a22b21d9d85162abefcf4abb9a7c74d3.png","sub_cats":[]},{"title":"النظافة العامة والمتخصصة","id":"8","image":"https://fawid.net/files/854d0b71cf6929c2c0da7a0fc21f46a3.png","sub_cats":[]},{"title":"التبريد والتكييف","id":"9","image":"https://fawid.net/files/0a42a11b4fe1e159c5997e89e9a5b31b.png","sub_cats":[]},{"title":"الكهرباء","id":"14","image":"","sub_cats":[]},{"title":"مكافحة الآفات والحشرات","id":"15","image":"","sub_cats":[]},{"title":"العوازل","id":"16","image":"","sub_cats":[]},{"title":"كشف وصيانة تسريبات المياه","id":"17","image":"","sub_cats":[]},{"title":"النجارة والموبيليا","id":"18","image":"","sub_cats":[]},{"title":"الزراعة والري والتشجير","id":"21","image":"","sub_cats":[]},{"title":"أنظمة الأمن والحماية","id":"22","image":"","sub_cats":[]},{"title":"الدهانات وورق الجدران","id":"23","image":"","sub_cats":[]},{"title":"الجبس والديكور الداخلي","id":"24","image":"","sub_cats":[]},{"title":"الحدادة واللحام","id":"25","image":"","sub_cats":[]},{"title":"صيانة الأجهزة والجوالات","id":"26","image":"","sub_cats":[]},{"title":"التصوير والمونتاج","id":"27","image":"","sub_cats":[]},{"title":"التصميم والطباعة","id":"28","image":"","sub_cats":[]},{"title":"غسيل وتلميع السيارات","id":"29","image":"","sub_cats":[]},{"title":"تجهيز الحفلات والمناسبات","id":"30","image":"","sub_cats":[]},{"title":"حاويات مخلفات البناء","id":"31","image":"","sub_cats":[]},{"title":"السطحات ونقليات السيارات","id":"32","image":"","sub_cats":[]}]
     */

    public String status;
    public boolean error;
    public String message;
    public List<DataBean> data = new ArrayList<>();

    public static class DataBean {
        /**
         * title : السباكة
         * id : 1
         * image : https://fawid.net/files/4bc1ba718bbe3cda3a5f2485caaae189.jpg
         * sub_cats : [{"title":"تجهيز الحفلات والمناسبات","id":"30","image":""},{"title":"حاويات مخلفات البناء","id":"31","image":""},{"title":"السطحات ونقليات السيارات","id":"32","image":""}]
         */

        public String title;
        public String id;
        public String image;
        public boolean check ;
        public List<SubCatsBean> sub_cats = new ArrayList<>();

        public static class SubCatsBean {
            /**
             * title : تجهيز الحفلات والمناسبات
             * id : 30
             * image :
             */

            public String title;
            public String id;
            public String image;
            public boolean check ;
        }
    }
}
