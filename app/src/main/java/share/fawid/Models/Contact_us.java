package share.fawid.Models;

import java.util.ArrayList;
import java.util.List;

public class Contact_us {

    /**
     * status : 200
     * error : false
     * message : تم بنجاح
     * data : [{"working_time":"10-05","workdays":"sun-thr","phone":"966560037222+","email":"mohammedallhidan@gmail.com","mobile":"","address":"PO Box 16122 Riyadh, Suadi Arabia","facebook":"https://www.facebook.com/fawidapp","twitter":"https://twitter.com/fawidapp","instagram":"http://instagram.com/fawidapp","snapchat":"fawid","telegram":"fawid"}]
     */

    public String status;
    public boolean error;
    public String message;
    public List<DataBean> data = new ArrayList<>();

    public static class DataBean {
        /**
         * working_time : 10-05
         * workdays : sun-thr
         * phone : 966560037222+
         * email : mohammedallhidan@gmail.com
         * mobile :
         * address : PO Box 16122 Riyadh, Suadi Arabia
         * facebook : https://www.facebook.com/fawidapp
         * twitter : https://twitter.com/fawidapp
         * instagram : http://instagram.com/fawidapp
         * snapchat : fawid
         * telegram : fawid
         */

        public String working_time;
        public String workdays;
        public String phone;
        public String email;
        public String mobile;
        public String address;
        public String facebook;
        public String twitter;
        public String instagram;
        public String snapchat;
        public String telegram;
    }
}
