package share.fawid.Models;

import java.util.ArrayList;
import java.util.List;

public class Welcome {

    /**
     * status : 200
     * error : false
     * message : تم بنجاح
     * data : [{"title":"الية عمل التطبيق","content":"<p>الية عمل التطبيق الية عمل التطبيق الية عمل التطبيق الية عمل التطبيق الية عمل التطبيق الية عمل التطبيق<\/p>","image":"https://fawid.net/files/6a432d79ea779478869416f552304253.jpg","created":"2020-06-14 05:49:57"},{"title":"تسجيل مزودى الخدمة","content":"<p>تسجيل مزودى الخدمة تسجيل مزودى الخدمة تسجيل مزودى الخدمة تسجيل مزودى الخدمة تسجيل مزودى الخدمة<\/p>","image":"https://fawid.net/files/1b7e0ad131a6cffb73dfe0b4291e433e.jpg","created":"2020-06-14 05:52:42"}]
     */

    public String status;
    public boolean error;
    public String message;
    public List<DataBean> data = new ArrayList<>();

    public static class DataBean {
        /**
         * title : الية عمل التطبيق
         * content : <p>الية عمل التطبيق الية عمل التطبيق الية عمل التطبيق الية عمل التطبيق الية عمل التطبيق الية عمل التطبيق</p>
         * image : https://fawid.net/files/6a432d79ea779478869416f552304253.jpg
         * created : 2020-06-14 05:49:57
         */

        public String title;
        public String content;
        public String image;
        public String created;
    }
}
