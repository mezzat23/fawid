package share.fawid.Models;

import java.util.ArrayList;
import java.util.List;

public class City {

    /**
     * status : 200
     * error : false
     * message : المدن
     * data : [{"name":"الرياض","id":"1455"},{"name":"مكة","id":"1456"},{"name":"المدينة المنورة","id":"1457"}]
     */

    public String status;
    public boolean error;
    public String message;
    public List<DataBean> data = new ArrayList<>();

    public static class DataBean {
        /**
         * name : الرياض
         * id : 1455
         */

        public String name;
        public String id;
        public boolean check = false ;
    }
}
