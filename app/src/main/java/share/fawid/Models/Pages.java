package share.fawid.Models;

import java.util.ArrayList;
import java.util.List;

public class Pages {

    /**
     * status : 200
     * error : false
     * message : تم بنجاح
     * data : [{"title":"فاوض","content":"<p>من خلال فريقنا المحترف والخبير، نوفر لك العديد من الخيارات السهلة والمريحة والمناسبة.. أنت على مسافة نقرة واحدة لتلبية كل احتياجاتك وخدماتك بشتَّى أنواعها!<\/p>","created":"2020-05-11 03:03:10"}]
     */

    public String status;
    public boolean error;
    public String message;
    public List<DataBean> data = new ArrayList<>();

    public static class DataBean {
        /**
         * title : فاوض
         * content : <p>من خلال فريقنا المحترف والخبير، نوفر لك العديد من الخيارات السهلة والمريحة والمناسبة.. أنت على مسافة نقرة واحدة لتلبية كل احتياجاتك وخدماتك بشتَّى أنواعها!</p>
         * created : 2020-05-11 03:03:10
         */

        public String title;
        public String content;
        public String created;
    }
}
