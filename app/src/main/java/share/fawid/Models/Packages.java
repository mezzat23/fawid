package share.fawid.Models;

import java.util.ArrayList;
import java.util.List;

public class Packages {

    /**
     * status : 200
     * error : false
     * message : تم بنجاح
     * data : [{"id":"1","price":"300","name":"الباقة الذهبية","content":""},{"id":"3","price":"149","name":"الباقة البرونزية","content":""},{"id":"7","price":"1","name":"44","content":""}]
     */

    public String status;
    public boolean error;
    public String message;
    public List<DataBean> data = new ArrayList<>();

    public static class DataBean {
        /**
         * id : 1
         * price : 300
         * name : الباقة الذهبية
         * content :
         */

        public String id;
        public String price;
        public String name;
        public String content;
    }
}
