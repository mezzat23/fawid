package share.fawid.Models;

import java.util.ArrayList;
import java.util.List;

public class Wallet {

    /**
     * status : 200
     * error : false
     * message : تم بنجاح
     * data : [{"balance":"979","clicks":60,"clicks_price":"61","shiping":[{"month":"May","created":"2020-05-22 06:11:31","amount":"400"},{"month":"May","created":"2020-05-22 06:15:45","amount":"200"},{"month":"May","created":"2020-05-22 20:19:06","amount":"200"},{"month":"May","created":"2020-05-31 21:17:10","amount":"2"},{"month":"Jun","created":"2020-06-01 11:41:53","amount":"100"},{"month":"Jun","created":"2020-06-01 11:43:07","amount":"100"},{"month":"Jun","created":"2020-06-01 11:46:35","amount":"100"},{"month":"Jun","created":"2020-06-01 11:54:12","amount":"500"},{"month":"Jun","created":"2020-06-01 12:02:57","amount":"100"},{"month":"Jun","created":"2020-06-09 08:34:58","amount":"1"}]}]
     */

    public String status;
    public boolean error;
    public String message;
    public List<DataBean> data = new ArrayList<>();

    public static class DataBean {
        /**
         * balance : 979
         * clicks : 60
         * clicks_price : 61
         * shiping : [{"month":"May","created":"2020-05-22 06:11:31","amount":"400"},{"month":"May","created":"2020-05-22 06:15:45","amount":"200"},{"month":"May","created":"2020-05-22 20:19:06","amount":"200"},{"month":"May","created":"2020-05-31 21:17:10","amount":"2"},{"month":"Jun","created":"2020-06-01 11:41:53","amount":"100"},{"month":"Jun","created":"2020-06-01 11:43:07","amount":"100"},{"month":"Jun","created":"2020-06-01 11:46:35","amount":"100"},{"month":"Jun","created":"2020-06-01 11:54:12","amount":"500"},{"month":"Jun","created":"2020-06-01 12:02:57","amount":"100"},{"month":"Jun","created":"2020-06-09 08:34:58","amount":"1"}]
         */

        public String balance;
        public int clicks;
        public String clicks_price;
        public List<ShipingBean> shiping = new ArrayList<>();

        public static class ShipingBean {
            /**
             * month : May
             * created : 2020-05-22 06:11:31
             * amount : 400
             */

            public String month;
            public String created;
            public String amount;
        }
    }
}
