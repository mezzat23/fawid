package share.fawid.Models;

import java.util.ArrayList;
import java.util.List;

public class Login {

    /**
     * status : 200
     * error : false
     * message : تم بنجاح
     * data : [{"id":"118","full_name":"mahmoud ezaat","phone":"966500642437","type":2,"active_staus":"no","city":"1847"}]
     */

    public String status;
    public boolean error;
    public String message;
    public List<DataBean> data = new ArrayList<>();

    public static class DataBean {
        /**
         * id : 118
         * full_name : mahmoud ezaat
         * phone : 966500642437
         * type : 2
         * active_staus : no
         * city : 1847
         */

        public String id;
        public String full_name;
        public String phone;
        public int type;
        public String image ;
        public String code;
        public String active_staus;
        public String city_name;
        public String city_id ;
        public String email ;
        public String gender ;

    }
}
