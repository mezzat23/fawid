package share.fawid.Models;

import java.util.ArrayList;
import java.util.List;

public class Clicks {

    /**
     * status : 200
     * error : false
     * message : تم بنجاح
     * data : [{"email_clicks":0,"mobile_clicks":0,"website_clicks":0,"tambler_clicks":0,"skype_clicks":0,"insatgram_clicks":0,"whatsapp_clicks":0,"snapchat_clicks":0,"linkedin_clicks":0,"twitter_clicks":0,"youtube_clicks":0,"facebook_clicks":0}]
     */

    public String status;
    public boolean error;
    public String message;
    public List<DataBean> data = new ArrayList<>();

    public static class DataBean {
        /**
         * email_clicks : 0
         * mobile_clicks : 0
         * website_clicks : 0
         * tambler_clicks : 0
         * skype_clicks : 0
         * insatgram_clicks : 0
         * whatsapp_clicks : 0
         * snapchat_clicks : 0
         * linkedin_clicks : 0
         * twitter_clicks : 0
         * youtube_clicks : 0
         * facebook_clicks : 0
         */

        public String email_clicks;
        public String mobile_clicks;
        public String website_clicks;
        public String tambler_clicks;
        public String skype_clicks;
        public String insatgram_clicks;
        public String whatsapp_clicks;
        public String snapchat_clicks;
        public String linkedin_clicks;
        public String twitter_clicks;
        public String youtube_clicks;
        public String facebook_clicks;
    }
}
