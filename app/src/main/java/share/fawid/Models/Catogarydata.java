package share.fawid.Models;

import java.util.List;

public class Catogarydata {

    /**
     * status : 200
     * error : false
     * message : تم بنجاح
     * data : [{"id":"1","full_name":"شركة  السباكين العالمية","phone":"966547372499","type":"1","active_staus":"yes","image":"https://fawid.net/files/a18f77bbbd75e0291719619fe63e854c.jpg","rate":null,"created":"2020-04-25 15:18:19"},{"id":"30","full_name":"شركه شير","phone":"966500000000","type":"1","active_staus":"yes","image":"","rate":null,"created":"2020-05-16 00:55:42"},{"id":"158","full_name":"test","phone":"966538878711","type":"1","active_staus":"no","image":"","rate":null,"created":"2020-06-08 21:30:54"}]
     */

    public String status;
    public boolean error;
    public String message;
    public List<Home.RecentlyBean> data;
}
