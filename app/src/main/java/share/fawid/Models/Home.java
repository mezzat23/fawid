package share.fawid.Models;

import java.util.ArrayList;
import java.util.List;

public class Home {

    /**
     * status : 200
     * error : false
     * message : البيانات
     * categories : [{"title":"السباكة","id":"1"},{"title":"نقل العفش","id":"20"},{"title":"الدواء","id":"4"},{"title":"الشحن","id":"5"},{"title":"البرمجيات","id":"6"},{"title":"صيانة وفحص السيارات","id":"7"},{"title":"النظافة العامة والمتخصصة","id":"8"},{"title":"التبريد والتكييف","id":"9"},{"title":"الكهرباء","id":"14"},{"title":"مكافحة الآفات والحشرات","id":"15"},{"title":"العوازل","id":"16"},{"title":"كشف وصيانة تسريبات المياه","id":"17"},{"title":"النجارة والموبيليا","id":"18"},{"title":"الزراعة والري والتشجير","id":"21"},{"title":"أنظمة الأمن والحماية","id":"22"},{"title":"الدهانات وورق الجدران","id":"23"},{"title":"الجبس والديكور الداخلي","id":"24"},{"title":"الحدادة واللحام","id":"25"},{"title":"صيانة الأجهزة والجوالات","id":"26"},{"title":"التصوير والمونتاج","id":"27"},{"title":"التصميم والطباعة","id":"28"},{"title":"غسيل وتلميع السيارات","id":"29"},{"title":"تجهيز الحفلات والمناسبات","id":"30"},{"title":"حاويات مخلفات البناء","id":"31"},{"title":"السطحات ونقليات السيارات","id":"32"}]
     * sliders : [{"title":"فاوض","content":"وجهتك الأولى التي تربطك مع مزودي الخدمات!","image":"https://fawid.net/files/f8728b90ccfe613cda6e13a7b5e392bd.jpg"},{"title":"فاوض","content":"كل الإمكانات مسخرة لخدمتك.","image":"https://fawid.net/files/f270a50500d6f68f519771239bb3016f.png"}]
     * recommended : [{"name":"شركة  السباكين العالمية","content":null,"image":"https://fawid.net/files/a18f77bbbd75e0291719619fe63e854c.jpg","rate":32}]
     * recently : [{"name":"waleed","content":null,"image":"https://fawid.net/files/","rate":0},{"name":"شركه شير","content":null,"image":"https://fawid.net/files/0","rate":0}]
     */

    public String status;
    public boolean error;
    public String message;
    public List<CategoriesBean> categories = new ArrayList<>();
    public List<SlidersBean> sliders = new ArrayList<>();
    public List<RecommendedBean> recommended = new ArrayList<>();
    public List<RecentlyBean> recently = new ArrayList<>();

    public static class CategoriesBean {
        /**
         * title : السباكة
         * id : 1
         */

        public String title;
        public String id;
        public String image ;
    }

    public static class SlidersBean {
        /**
         * title : فاوض
         * content : وجهتك الأولى التي تربطك مع مزودي الخدمات!
         * image : https://fawid.net/files/f8728b90ccfe613cda6e13a7b5e392bd.jpg
         */

        public String title;
        public String content;
        public String image;
    }

    public static class RecommendedBean {
        /**
         * name : شركة  السباكين العالمية
         * content : null
         * image : https://fawid.net/files/a18f77bbbd75e0291719619fe63e854c.jpg
         * rate : 32
         */

        public String name;
        public String content;
        public String image;
        public int rate;
        public String id ;
        public String phone ;
    }

    public static class RecentlyBean {
        /**
         * name : waleed
         * content : null
         * image : https://fawid.net/files/
         * rate : 0
         */

        public String name;
        public String content;
        public String image;
        public String rate;
        public String id ;
        public String phone ;
    }
}
