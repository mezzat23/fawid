package share.fawid.Models;

import java.util.ArrayList;
import java.util.List;

public class Notification {

    /**
     * status : 200
     * error : false
     * message : تم بنجاح
     * data : [{"id":"31","text":"لقد تم تعديل رصيدك باضافة 2 عبارة عن شحن رصيد  واصبح رصيدك الجديد 900","created":"2020-05-31 21:17:10"},{"id":"34","text":"لقد تم تعديل رصيدك باضافة 100 عبارة عن هدية واصبح رصيدك الجديد 993","created":"2020-06-01 11:41:36"},{"id":"35","text":"لقد تم تعديل رصيدك باضافة 100 عبارة عن شحن رصيد  واصبح رصيدك الجديد 993","created":"2020-06-01 11:41:53"},{"id":"39","text":"لقد تم تعديل رصيدك باضافة 100 عبارة عن شحن رصيد  واصبح رصيدك الجديد 993","created":"2020-06-01 11:43:07"},{"id":"40","text":"لقد تم تعديل رصيدك باضافة 100 عبارة عن شحن رصيد  واصبح رصيدك الجديد 1063","created":"2020-06-01 11:46:35"},{"id":"41","text":"لقد تم تعديل رصيدك باضافة 500 عبارة عن شحن رصيد  واصبح رصيدك الجديد 1393","created":"2020-06-01 11:54:12"},{"id":"42","text":"لقد تم تعديل رصيدك باضافة 100 عبارة عن شحن رصيد  واصبح رصيدك الجديد 993","created":"2020-06-01 12:02:57"},{"id":"74","text":"test","created":"2020-06-08 18:39:34"}]
     */

    public String status;
    public boolean error;
    public String message;
    public List<DataBean> data = new ArrayList<>();

    public static class DataBean {
        /**
         * id : 31
         * text : لقد تم تعديل رصيدك باضافة 2 عبارة عن شحن رصيد  واصبح رصيدك الجديد 900
         * created : 2020-05-31 21:17:10
         */

        public String id;
        public String text;
        public String created;
    }
}
