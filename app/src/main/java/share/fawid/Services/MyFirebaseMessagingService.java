package share.fawid.Services;

/**
 * Created by PC on 04/10/2016.
 */

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

import share.fawid.Activity.NotificationsActivity;
import share.fawid.R;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    String operationId;
    String messageBody, title, type;
    Intent intent;
    double latitude, longitude;
    JSONObject obj;
    int x = 0;
    SharedPreferences storedata;
    private static String filename = "mlogin";
    String id;
    SharedPreferences loginFile;
    SharedPreferences messageshared;
    String NOTIFICATION_CHANNEL_ID = "10001";
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("hhbh",remoteMessage.getData().toString());
        loginFile = getSharedPreferences("not", 0);
        if (loginFile.getBoolean("not", true)) {
            try {
                JSONObject jsonObject = new JSONObject(remoteMessage.getData());
                Random random = new Random();
                int notificationID = random.nextInt(9999 - 1000) + 1000;
                Intent notificationIntent = new Intent(getApplicationContext(), NotificationsActivity.class);
                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                show_not(notificationIntent, jsonObject.getString("title"), jsonObject.getString("body"), notificationID);

//                if (!jsonObject.has("extras")) {
//                    Random random = new Random();
//                    int notificationID = random.nextInt(9999 - 1000) + 1000;
//                    Intent notificationIntent = new Intent(getApplicationContext(), NotActivity.class);
//                    notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    show_not(notificationIntent, jsonObject.getString("title"), jsonObject.getString("body"), notificationID);
//
//
//                } else if (jsonObject.has("extras")) {
//                    JSONObject j = new JSONObject(jsonObject.getString("extras"));
//                   if (j.getString("type").equals("offer")){
//                       Intent notificationIntent = new Intent(getApplicationContext(), DetailsofferActivity.class);
//                       notificationIntent.putExtra("id", (j.getInt("id")));
//                       notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                       show_not(notificationIntent, jsonObject.getString("title"), jsonObject.getString("body"), j.getInt("id"));
//
//                   }else {
//                       Intent notificationIntent = new Intent(getApplicationContext(), DetailshopActivity.class);
//                       notificationIntent.putExtra("id", (j.getInt("id")));
//                       notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                       show_not(notificationIntent, jsonObject.getString("title"), jsonObject.getString("body"), j.getInt("id"));
//
//                   }


//                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
private void show_not(Intent notificationIntent ,String title , String message , int order_id){
    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent,
            PendingIntent.FLAG_UPDATE_CURRENT);
    NotificationCompat.Builder notificationBuilder = null;

    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
    NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(bitmap1)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(contentIntent);
    } else {
        notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(contentIntent);
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build();
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
        notificationChannel.enableLights(true);
        notificationChannel.setShowBadge(true);
        notificationChannel.setLightColor(Color.RED);
        notificationChannel.enableVibration(true);
        notificationChannel.setSound(defaultSoundUri,attributes);
        notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        assert notificationManager != null;
        notificationBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
        notificationManager.createNotificationChannel(notificationChannel);
    }
    assert notificationManager != null;
    Random random = new Random();
    int notificationID = random.nextInt(9999 - 1000) + 1000;
    notificationManager.notify(order_id, notificationBuilder.build());
}

}