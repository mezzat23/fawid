package share.fawid.Api;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.lang.reflect.Type;

import share.fawid.Helper.Dialogs;
import share.fawid.Helper.ErrorResponse;
import share.fawid.Helper.LoginSession;
import share.fawid.R;

import static share.fawid.Helper.LoginSession.loginFile;

public class APIModel {
    public final static int take = 10;
    public final static String BASE_URL = "http://fawid.net/api/";
    public final static String BASE_URL1 = "https://onnety-solutions.com/laeq/public/uploads/post_details/";

    // when app version is old
    public final static int FORCE_UPDATE = 451;
    // when user blocked
    public final static int BLOCK = 456;
    // when token expired
    public final static int REFRESH_TOKEN = 401;
    public final static int SUCCESS = 200;
    public final static int CREATED = 201;
    public final static int Failer = 422;
    public final static int BAD_REQUEST = 400;
    public final static int UNAUTHORIZE = 401;
    public final static int SERVER_ERROR = 500;
    public final static int USER_DELETE = 403;
    public final static int Not_FOUND = 404;
    public final static int Error = 409;
    public static String version = "v1";
    public static String device_type = "1";

    public static void handleFailure(final Context activity, int statusCode, String errorResponse, final RefreshTokenListener listener) {
        Log.e("fail", statusCode + "--" + errorResponse);
        Type dataType = new TypeToken<ErrorResponse>() {
        }.getType();
        ErrorResponse responseBody = new ErrorResponse();
        try {
            if (statusCode != SERVER_ERROR)
                responseBody = new Gson().fromJson(errorResponse, dataType);

        } catch (Exception e) {
        }
        switch (statusCode) {
            case BAD_REQUEST:
                if (!responseBody.getMessage().equals("")) {
                    Dialogs.showToast(responseBody.getMessage(), activity);
                } else {
                    Dialogs.showToast(responseBody.getMsg() != null ?
                            responseBody.getMsg() : "", activity);
                }
                break;
            case Not_FOUND:
                if (!responseBody.getMessage().equals("")) {
                    Dialogs.showToast(responseBody.getMessage(), activity);
                } else {
                    Dialogs.showToast(responseBody.getMsg() != null ?
                            responseBody.getMsg() : "", activity);
                }
                ((Activity) activity).finish();
                break;
            case Failer:
                if (!responseBody.getMessage().equals("")) {
                    Dialogs.showToast(responseBody.getMessage(), activity);
                } else {
                    Dialogs.showToast(responseBody.getMsg() != null ?
                            responseBody.getMsg() : "", activity);
                }
                break;
            case Error:
                if (!responseBody.getMessage().equals("")) {
                    Dialogs.showToast(responseBody.getMessage(), activity);
                } else {
                    Dialogs.showToast(responseBody.getMsg() != null ?
                            responseBody.getMsg() : "", activity);
                }
                break;
            case USER_DELETE:
                loginFile = activity.getSharedPreferences("LOGIN_FILE", 0);
                if (loginFile.getString("type", "").equals("3")) {
                    LoginSession.clearData((Activity) activity);
                } else {
                    LoginSession.clearData((Activity) activity);
                }
                Toast.makeText(activity, R.string.user_deleted, Toast.LENGTH_SHORT).show();
                break;
            case REFRESH_TOKEN:

                break;
            default:
                Dialogs.showToast(activity.getString(R.string.no_network), (Activity) activity);
        }
    }

    public static void userResponses(final Activity activity, int code) {
        switch (code) {
            case FORCE_UPDATE:
                //  IntentClass.goToActivityAndClear(activity, ForceUpdateActivity.class, null);
                break;
            case BLOCK:
                //  IntentClass.goToActivityAndClear(activity, BlockActivity.class, null);
                break;
            case REFRESH_TOKEN:
//                SharedPref sharedPref = new SharedPref(activity);
//  sharedPref.setToken(response.body().getTokenResponse().getToken());
                try {
//                    func.notify();
                } catch (Exception e) {
                }
                break;

        }
    }

    public static AsyncHttpClient getMethod(Activity currentActivity, String url, TextHttpResponseHandler textHttpResponseHandler) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(30000);
        client.addHeader("version", version + "");
        client.addHeader("lang", currentActivity.getString(R.string.lang));
        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
//        if (LoginSession.isLogin) {
//            Log.e("DD", LoginSession.setdata(currentActivity).token);
//            client.addHeader("Authorization", "Bearer " +LoginSession.setdata(currentActivity).token);
//        }
        client.get(BASE_URL + url, textHttpResponseHandler);
        Log.e("url", BASE_URL + url);
        return client;
    }

    public static AsyncHttpClient postMethod(Activity currentActivity, String url, RequestParams params, TextHttpResponseHandler textHttpResponseHandler) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(500000);
        client.addHeader("version", version + "");
        client.addHeader("lang", currentActivity.getString(R.string.lang));
        if (params.toString().contains("avatar") || params.toString().contains("image")) {
        } else {
            client.addHeader("Content-Type", "application/x-www-form-urlencoded");
        }        client.addHeader("Accept", "application/json");
//        if (LoginSession.isLogin) {
//            client.addHeader("Authorization","Bearer " + LoginSession.setdata(currentActivity).token);
//        }
        client.post(BASE_URL + url, params, textHttpResponseHandler);

        return client;
    }

    public static AsyncHttpClient postMethod1(Activity currentActivity, String url, RequestParams params, Long lenth, TextHttpResponseHandler textHttpResponseHandler) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(50000);
        client.addHeader("version", version + "");
        client.addHeader("lang", currentActivity.getString(R.string.lang));
        if (params.toString().contains("attachments") || params.toString().contains("image")) {

        } else {
            client.addHeader("Content-Type", "application/x-www-form-urlencoded");
        }
//        if (LoginSession.isLogin) {
//            client.addHeader("Authorization", "Bearer " +LoginSession.setdata(currentActivity).token);
//        }
        client.post(BASE_URL + url, params, textHttpResponseHandler);

        return client;
    }

    public static AsyncHttpClient putMethod(Activity currentActivity, String url, RequestParams params, TextHttpResponseHandler textHttpResponseHandler) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(30000);
        client.addHeader("version", version + "");
        client.addHeader("lang", currentActivity.getString(R.string.lang));
        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
        SharedPreferences loginFile;
        client.put(BASE_URL + url, params, textHttpResponseHandler);
        return client;
    }

    public static AsyncHttpClient deleteMethod(Activity currentActivity, String url, TextHttpResponseHandler textHttpResponseHandler) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(30000);
        client.addHeader("version", version + "");
        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
//        if (LoginSession.isLogin) {
//            client.addHeader("Authorization", "Bearer " +LoginSession.setdata(currentActivity).token);
//        }
        client.addHeader("lang", currentActivity.getString(R.string.lang));
        client.delete(BASE_URL + url, textHttpResponseHandler);

        return client;
    }


    public static AsyncHttpClient tokenPost(Activity currentActivity, String url, RequestParams params, TextHttpResponseHandler textHttpResponseHandler) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(30000);
        client.addHeader("version", version + "");
        client.addHeader("Content-Type", "application/x-www-form-urlencoded");
//        if (LoginSession.isLogin) {
//            client.addHeader("Authorization", "Bearer " +LoginSession.setdata(currentActivity).token);
//        }
        client.addHeader("lang", currentActivity.getString(R.string.lang));
        client.post(BASE_URL + url, params, textHttpResponseHandler);

        return client;
    }

    public interface RefreshTokenListener {
        void onRefresh();
    }

}