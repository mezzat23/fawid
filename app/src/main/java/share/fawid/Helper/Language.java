package share.fawid.Helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.SpannableStringBuilder;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import share.fawid.Activity.Provider.HomeActivity;
import share.fawid.Activity.Provider.RegisterconfirmActivity;
import share.fawid.Activity.WelcomeActivity;
import share.fawid.Adapter.Socialadapter;
import share.fawid.Api.APIModel;
import share.fawid.Models.Matgerprofile;
import share.fawid.Models.Social;
import share.fawid.R;

import static androidx.core.util.Preconditions.checkNotNull;


public class Language {
    private CardView back;
    private ImageView btnCall;
    private ImageView btnMessage;
    private ImageView btnFacebook;
    private ImageView btnTwitter;
    private ImageView btnInstgram;
    private ImageView btnSkype;
    private ImageView btnWhatsapp;
    private ImageView btnGroupMe;
    private ImageView btnSnapChat;
    private ImageView btnTumblr;
    private ImageView btnReddit;
    private ImageView btnLinkedIn;
    private ImageView btnPeriscope;
    private ImageView btnMessenger;
    private ImageView btnYoutube;
    private ImageView btnKiK;
    ArrayList<Social> socials = new ArrayList<>();

    public static void showdialog(final Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_lang);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int width = display.getWidth();
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setLayout((LinearLayout.LayoutParams.MATCH_PARENT), LinearLayout.LayoutParams.WRAP_CONTENT);
        ((Activity) context).getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        LinearLayout arabic = (LinearLayout) dialog.findViewById(R.id.arabic);
        LinearLayout english = (LinearLayout) dialog.findViewById(R.id.english);
        CheckBox checkenglish = dialog.findViewById(R.id.checkenglish);
        CheckBox checkarabic = dialog.findViewById(R.id.checkarabic);
        arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lang = "ar";
                try {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                    Resources res = context.getResources();
                    DisplayMetrics dm = res.getDisplayMetrics();
                    Configuration conf = res.getConfiguration();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        conf.setLocale(new Locale(lang.toLowerCase()));
                    } else {
                        conf.locale = new Locale(lang.toLowerCase());
                    }
                    res.updateConfiguration(conf, dm);
                    prefs.edit().putString("lang", lang).commit();
                } catch (NullPointerException a) {
                    a.printStackTrace();
                } catch (RuntimeException a) {
                    a.printStackTrace();
                }
                SharedPreferences sharedPreferences = context.getSharedPreferences("country", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.commit();
                if (LoginSession.getwelcome((Activity) context) == true) {
                    if (LoginSession.isLogin) {
                        if (LoginSession.setdata(context).data.get(0).type == 1) {
                            if (LoginSession.setdata(context).data.get(0).active_staus.equals("yes")) {
                                Intent i = new Intent(context, HomeActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            } else {
                                Intent i = new Intent(context, RegisterconfirmActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            }
                        } else {
                            if (LoginSession.setdata(context).data.get(0).active_staus.equals("yes")) {
                                Intent i = new Intent(context, share.fawid.Activity.Customer.HomeActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            } else {
                                Intent i = new Intent(context, share.fawid.Activity.Customer.HomeActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            }
                        }

                    } else {
                        Intent i = new Intent(context, share.fawid.Activity.Customer.HomeActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(i);
                        ((Activity) context).finish();
                    }
                } else {
                    Intent i = new Intent(context, WelcomeActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(i);
                    ((Activity) context).finish();
                }

                dialog.dismiss();
            }
        });
        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lang = "en";
                try {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                    Resources res = context.getResources();
                    DisplayMetrics dm = res.getDisplayMetrics();
                    Configuration conf = res.getConfiguration();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        conf.setLocale(new Locale(lang.toLowerCase()));
                    } else {
                        conf.locale = new Locale(lang.toLowerCase());
                    }
                    res.updateConfiguration(conf, dm);
                    prefs.edit().putString("lang", lang).commit();
                } catch (NullPointerException a) {
                    a.printStackTrace();
                } catch (RuntimeException a) {
                    a.printStackTrace();
                }
                SharedPreferences sharedPreferences = context.getSharedPreferences("country", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.commit();
                if (LoginSession.getwelcome((Activity) context) == true) {
                    if (LoginSession.isLogin) {
                        if (LoginSession.setdata(context).data.get(0).type == 1) {
                            if (LoginSession.setdata(context).data.get(0).active_staus.equals("yes")) {
                                Intent i = new Intent(context, HomeActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            } else {
                                Intent i = new Intent(context, RegisterconfirmActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            }
                        } else {
                            if (LoginSession.setdata(context).data.get(0).active_staus.equals("yes")) {
                                Intent i = new Intent(context, share.fawid.Activity.Customer.HomeActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            } else {
                                Intent i = new Intent(context, RegisterconfirmActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            }
                        }

                    } else {
                        Intent i = new Intent(context, share.fawid.Activity.Customer.HomeActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(i);
                        ((Activity) context).finish();
                    }
                } else {
                    Intent i = new Intent(context, WelcomeActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(i);
                    ((Activity) context).finish();
                }


                dialog.dismiss();
            }
        });
        checkarabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lang = "ar";
                try {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                    Resources res = context.getResources();
                    DisplayMetrics dm = res.getDisplayMetrics();
                    Configuration conf = res.getConfiguration();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        conf.setLocale(new Locale(lang.toLowerCase()));
                    } else {
                        conf.locale = new Locale(lang.toLowerCase());
                    }
                    res.updateConfiguration(conf, dm);
                    prefs.edit().putString("lang", lang).commit();
                } catch (NullPointerException a) {
                    a.printStackTrace();
                } catch (RuntimeException a) {
                    a.printStackTrace();
                }
                SharedPreferences sharedPreferences = context.getSharedPreferences("country", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.commit();
                if (LoginSession.getwelcome((Activity) context) == true) {
                    if (LoginSession.isLogin) {
                        if (LoginSession.setdata(context).data.get(0).type == 1) {
                            if (LoginSession.setdata(context).data.get(0).active_staus.equals("yes")) {
                                Intent i = new Intent(context, HomeActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            } else {
                                Intent i = new Intent(context, RegisterconfirmActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            }
                        } else {
                            if (LoginSession.setdata(context).data.get(0).active_staus.equals("yes")) {
                                Intent i = new Intent(context, share.fawid.Activity.Customer.HomeActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            } else {
                                Intent i = new Intent(context, RegisterconfirmActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            }
                        }

                    } else {
                        Intent i = new Intent(context, share.fawid.Activity.Customer.HomeActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(i);
                        ((Activity) context).finish();
                    }
                } else {
                    Intent i = new Intent(context, WelcomeActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(i);
                    ((Activity) context).finish();
                }

                dialog.dismiss();
            }
        });
        checkenglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lang = "en";
                try {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                    Resources res = context.getResources();
                    DisplayMetrics dm = res.getDisplayMetrics();
                    Configuration conf = res.getConfiguration();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        conf.setLocale(new Locale(lang.toLowerCase()));
                    } else {
                        conf.locale = new Locale(lang.toLowerCase());
                    }
                    res.updateConfiguration(conf, dm);
                    prefs.edit().putString("lang", lang).commit();
                } catch (NullPointerException a) {
                    a.printStackTrace();
                } catch (RuntimeException a) {
                    a.printStackTrace();
                }
                SharedPreferences sharedPreferences = context.getSharedPreferences("country", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.commit();
                if (LoginSession.getwelcome((Activity) context) == true) {
                    if (LoginSession.isLogin) {
                        if (LoginSession.setdata(context).data.get(0).type == 1) {
                            if (LoginSession.setdata(context).data.get(0).active_staus.equals("yes")) {
                                Intent i = new Intent(context, HomeActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            } else {
                                Intent i = new Intent(context, RegisterconfirmActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            }
                        } else {
                            if (LoginSession.setdata(context).data.get(0).active_staus.equals("yes")) {
                                Intent i = new Intent(context, share.fawid.Activity.Customer.HomeActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            } else {
                                Intent i = new Intent(context, RegisterconfirmActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(i);
                                ((Activity) context).finish();
                            }
                        }

                    } else {
                        Intent i = new Intent(context, share.fawid.Activity.Customer.HomeActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(i);
                        ((Activity) context).finish();
                    }
                } else {
                    Intent i = new Intent(context, WelcomeActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(i);
                    ((Activity) context).finish();
                }


                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @SuppressLint("RestrictedApi")
    public static SpannableStringBuilder trimSpannable(SpannableStringBuilder spannable) {
        checkNotNull(spannable);
        int trimStart = 0;
        int trimEnd = 0;

        String text = spannable.toString();

        while (text.length() > 0 && text.startsWith("\n")) {
            text = text.substring(1);
            trimStart += 1;
        }

        while (text.length() > 0 && text.endsWith("\n")) {
            text = text.substring(0, text.length() - 1);
            trimEnd += 1;
        }

        return spannable.delete(0, trimStart).delete(spannable.length() - trimEnd, spannable.length());
    }

    public void contactdialog(final Context context, final Matgerprofile matgerprofile) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_contact_us1);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int width = display.getWidth();
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setLayout((LinearLayout.LayoutParams.MATCH_PARENT), LinearLayout.LayoutParams.WRAP_CONTENT);
        ((Activity) context).getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        back = dialog.findViewById(R.id.back);
        RecyclerView list = dialog.findViewById(R.id.list);
        LinearLayoutManager linearLayoutManager = new GridLayoutManager(context,4);
        list.setLayoutManager(linearLayoutManager);
        Socialadapter socialadapter = new Socialadapter(socials,dialog,context);
        list.setAdapter(socialadapter);
        try {
            if (!matgerprofile.data.get(0).mobile.equals("")  && !matgerprofile.data.get(0).mobile.equals(null)){
                Social social = new Social();
                social.url = matgerprofile.data.get(0).mobile;
                social.id = matgerprofile.data.get(0).id;
                social.member_id = LoginSession.setdata(context).data.get(0).id ;
                social.type = "mobile";
                social.img = R.drawable.ic_call;
                socials.add(social);
            }
        }catch (Exception e){

        }
        try {
            if (!matgerprofile.data.get(0).email.equals("")  && !matgerprofile.data.get(0).email.equals(null)){
                Social social = new Social();
                social.url = matgerprofile.data.get(0).email;
                social.id = matgerprofile.data.get(0).id;
                social.type = "email";
                social.member_id = LoginSession.setdata(context).data.get(0).id ;
                social.img = R.drawable.ic_message;
                socials.add(social);
            }
        }catch (Exception e){

        }
        try {
            if (!matgerprofile.data.get(0).facebook.equals("")  && !matgerprofile.data.get(0).facebook.equals(null)){
                Social social = new Social();
                social.url = matgerprofile.data.get(0).facebook;
                social.id = matgerprofile.data.get(0).id;
                social.type = "facebook";
                social.member_id = LoginSession.setdata(context).data.get(0).id ;
                social.img = R.drawable.ic_facebook;
                socials.add(social);
            }
        }catch (Exception e){

        }
        try {
            if (!matgerprofile.data.get(0).insatgram.equals("")  && !matgerprofile.data.get(0).insatgram.equals(null)){
                Social social = new Social();
                social.url = matgerprofile.data.get(0).insatgram;
                social.id = matgerprofile.data.get(0).id;
                social.type = "insatgram";
                social.member_id = LoginSession.setdata(context).data.get(0).id ;
                social.img = R.drawable.ic_instagram;
                socials.add(social);
            }
        }catch (Exception e){

        }
        try {
            if (!matgerprofile.data.get(0).twitter.equals("")  && !matgerprofile.data.get(0).twitter.equals(null)){
                Social social = new Social();
                social.url = matgerprofile.data.get(0).twitter;
                social.id = matgerprofile.data.get(0).id;
                social.type = "twitter";
                social.member_id = LoginSession.setdata(context).data.get(0).id ;
                social.img = R.drawable.ic_twitter;
                socials.add(social);
            }
        }catch (Exception e){

        }
        try {
            if (!matgerprofile.data.get(0).skype.equals("")  && !matgerprofile.data.get(0).skype.equals(null)){
                Social social = new Social();
                social.url = matgerprofile.data.get(0).skype;
                social.id = matgerprofile.data.get(0).id;
                social.type = "skype";
                social.member_id = LoginSession.setdata(context).data.get(0).id ;
                social.img = R.drawable.ic_skype;
                socials.add(social);
            }
        }catch (Exception e){

        }
        try {
            if (!matgerprofile.data.get(0).whatsapp.equals("")  && !matgerprofile.data.get(0). whatsapp.equals(null)){
                Social social = new Social();
                social.url = matgerprofile.data.get(0).whatsapp;
                social.id = matgerprofile.data.get(0).id;
                social.type = "whatsapp";
                social.member_id = LoginSession.setdata(context).data.get(0).id ;
                social.img = R.drawable.ic_whatsapp;
                socials.add(social);
            }
        }catch (Exception e){

        }
        try {
            if (!matgerprofile.data.get(0).tambler.equals("")  && !matgerprofile.data.get(0).tambler.equals(null)){
                Social social = new Social();
                social.url = matgerprofile.data.get(0).tambler;
                social.id = matgerprofile.data.get(0).id;
                social.type = "tambler";
                social.member_id = LoginSession.setdata(context).data.get(0).id ;
                social.img = R.drawable.ic_tumblr;
                socials.add(social);
            }
        }catch (Exception e){

        }
        try {
            if (!matgerprofile.data.get(0).linkedin.equals("")  && !matgerprofile.data.get(0).linkedin.equals(null)){
                Social social = new Social();
                social.url = matgerprofile.data.get(0).linkedin;
                social.id = matgerprofile.data.get(0).id;
                social.type = "linkedin";
                social.member_id = LoginSession.setdata(context).data.get(0).id ;
                social.img = R.drawable.ic_linkedin;
                socials.add(social);
            }
        }catch (Exception e){

        }
        try {
            if (!matgerprofile.data.get(0).snapchat.equals("")  && !matgerprofile.data.get(0).snapchat.equals(null)){
                Social social = new Social();
                social.url = matgerprofile.data.get(0).snapchat;
                social.id = matgerprofile.data.get(0).id;
                social.type = "snapchat";
                social.member_id = LoginSession.setdata(context).data.get(0).id ;
                social.img = R.drawable.ic_snapchat;
                socials.add(social);
            }
        }catch (Exception e){

        }
        try {
            if (!matgerprofile.data.get(0).youtube.equals("")  && !matgerprofile.data.get(0).youtube.equals(null)){
                Social social = new Social();
                social.url = matgerprofile.data.get(0).youtube;
                social.id = matgerprofile.data.get(0).id;
                social.type = "youtube";
                social.member_id = LoginSession.setdata(context).data.get(0).id ;
                social.img = R.drawable.ic_youtube;
                socials.add(social);
            }
        }catch (Exception e){

        }
        socialadapter.notifyDataSetChanged();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void onclick(final Context context, final String type, final String id) {
        RequestParams requestParams = new RequestParams();
        requestParams.put("user_id", LoginSession.setdata(context).data.get(0).id);
        requestParams.put("provider_id", id);
        requestParams.put("type", type);
        APIModel.postMethod((Activity) context, "clicks", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

            }
        });
    }

}
