package share.fawid.Helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import share.fawid.Activity.LoginActivity;
import share.fawid.Activity.SplashActivity;
import share.fawid.Models.Login;
import share.fawid.R;


/**
 * Created by pc on 01/06/2017.
 */

public class LoginSession {
    public static String token = "";
    public static int city_id ;
    public static String city = "";

    public static int expired;
    public static boolean isLogin;
    public static SharedPreferences loginFile;
    public static SharedPreferences cart;
    public static int id;
    public static String email;
    public static String mobile;
    public static String responsible_person_name;
    public static String trade_name;
    public static String about;
    public static String lat;
    public static String lng;
    public static String image;
    public static String Categories;
    public static boolean iscountry = false ;
    public static boolean iscity = false ;

    //    public static void getData(Activity activity) {
//
//        loginFile = activity.getSharedPreferences("LOGIN_FILE", 0);
//
//        fName = loginFile.getString("NAME", "");
//        userName = loginFile.getString("USERNAME", "");
//        phone = loginFile.getString("PHONE", "");
//        email = loginFile.getString("EMAIL", "");
//        img = loginFile.getString("IMAGE", "");
//        password = loginFile.getString("password", "");
//        isLogin = loginFile.getBoolean("LOGIN", false);
//        lat = loginFile.getString("lat", "");
//        lon = loginFile.getString("lon", "");
//        token = loginFile.getString("token", "");
//        expired = loginFile.getString("expired", "");
//
//    }
    public static Login setdata(Context activity) {

        loginFile = activity.getSharedPreferences("LOGIN_FILE", 0);
        String x = loginFile.getString("json", "");
        Log.e("data",x);
        Login data = null;
        if (!x.equals("")) {
            Type dataType = new TypeToken<Login>() {
            }.getType();
            data = new Gson().fromJson(x, dataType);
            isLogin = true;
        } else {
            isLogin = false;
        }
        return data;
    }
    public static void Addwelcome(Activity activity, boolean expired) {
        loginFile = activity.getSharedPreferences("welcome", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = loginFile.edit();
        editor.putBoolean("welcome", expired);
        editor.apply();

    }

    public static boolean getwelcome(Activity activity) {
        loginFile = activity.getSharedPreferences("welcome", Context.MODE_PRIVATE);
        boolean x = loginFile.getBoolean("welcome", false);
        return x;
    }
    public static Login setdataprovider(Context activity) {

        loginFile = activity.getSharedPreferences("LOGIN_FILE", 0);
        String x = loginFile.getString("json", "");
        Login data = null;
        if (!x.equals("")) {
            Type dataType = new TypeToken<Login>() {
            }.getType();
            data = new Gson().fromJson(x, dataType);
            isLogin = true;
        } else {
            isLogin = false;
        }
        return data;
    }


    public static void clearData(Activity activity) {
        loginFile = activity.getSharedPreferences("LOGIN_FILE", 0);
        SharedPreferences.Editor editor = loginFile.edit();
        editor.clear();
        editor.commit();
        Intent i = new Intent(activity, SplashActivity.class);
        LoginSession.isLogin = false ;
        i.putExtra("login","");
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(i);
        activity.finish();

    }

    public static void AddToSharedPreferences(Activity activity, String Name, String USERNAME, String phone, String Email, String IMAGE, String password, String lat, String lon, String token, String expired) {
        loginFile = activity.getSharedPreferences("LOGIN_FILE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = loginFile.edit();
        editor.putString("NAME", Name);
        editor.putString("USERNAME", USERNAME);
        editor.putString("PHONE", phone);
        editor.putString("EMAIL", Email);
        editor.putString("IMAGE", IMAGE);
        editor.putString("password", password);
        editor.putBoolean("LOGIN", true);
        editor.putString("lat", lat);
        editor.putString("lon", lon);
        editor.putString("token", token);
        editor.putString("expired", expired);

        editor.apply();

    }

    public static void AddTotokenSharedPreferences(Activity activity, String token, String expired) {
        loginFile = activity.getSharedPreferences("LOGIN_FILE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = loginFile.edit();
        editor.putString("token", token);
        editor.putString("expired", expired);

        editor.apply();

    }
    public static void addnotifications(Activity activity, boolean check) {
        loginFile = activity.getSharedPreferences("not", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = loginFile.edit();
        editor.putBoolean("check", check);
        editor.apply();

    }
    public static boolean getnotifications(Activity activity) {
        loginFile = activity.getSharedPreferences("not", 0);
        boolean x = loginFile.getBoolean("check", true);
        return x ;
    }
    public static void AddTotokenSharedpassword(Activity activity, String password) {
        loginFile = activity.getSharedPreferences("LOGIN_FILE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = loginFile.edit();
        editor.putString("password", password);

        editor.apply();

    }

    public static void plsGoLogin(final Activity activity) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(
                activity);
        alert.setTitle(R.string.Error);
        alert.setMessage(R.string.plz_login_first);
        alert.setPositiveButton(R.string.cancel,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alert.setNegativeButton(R.string.login,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(activity, LoginActivity.class);
                        activity.startActivity(i);
                    }
                });
        alert.show();
    }
}
