package share.fawid.Helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.widget.Toast;


/**
 * <h1>Implement reusable dialogs</h1>
 * Dialogs class for all dialogs and toasts
 * <p>
 *
 * @author kemo94
 * @version 1.0
 * @since 2017-08-9
 */

public abstract class Dialogs {


    public static Dialog progressDialog;

    public static Dialog noInternetDialog;



    public static void showToast(String message, Activity activity) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    public static void showToast(String message, Context context) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }


}
