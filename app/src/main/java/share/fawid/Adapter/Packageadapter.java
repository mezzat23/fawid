package share.fawid.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import share.fawid.Fragment.Provider.PackagesFragment;
import share.fawid.Models.Packages;
import share.fawid.R;


/**
 * Created by سيد on 04/06/2017.
 */
public class Packageadapter extends RecyclerView.Adapter<Packageadapter.MyViewHolder> {

    private List<Packages.DataBean> horizontalList;
    Context context;
    PopupMenu popup;
    Dialog dialog;
    String xx = "";
    public int pos = -1;
    Fragment fragment;
    int type = 0;
    PackagesFragment packagesFragment ;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CardView card;
        private CheckBox check;
        private TextView title;
        private TextView price;
        private TextView content;
        public MyViewHolder(View view) {
            super(view);
            card = view.findViewById(R.id.card);
            check = view.findViewById(R.id.check);
            title = view.findViewById(R.id.title);
            price = view.findViewById(R.id.price);
            content = view.findViewById(R.id.content);
        }
    }


    public Packageadapter(List<Packages.DataBean> horizontalList, PackagesFragment packagesFragment) {
        this.horizontalList = horizontalList;
        this.packagesFragment = packagesFragment ;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_package, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.title.setText(horizontalList.get(position).name);
        holder.price.setText(horizontalList.get(position).price + " " + context.getString(R.string.sar));
        holder.content.setText(horizontalList.get(position).content);
        if (pos == position){
            holder.check.setChecked(true);
            holder.card.setCardBackgroundColor(Color.parseColor("#074E88"));
            holder.content.setTextColor(Color.parseColor("#074E88"));
        }else {
            holder.check.setChecked(false);
            holder.card.setCardBackgroundColor(Color.parseColor("#D8D8D8"));
            holder.content.setTextColor(Color.parseColor("#7F8FA6"));
        }
        holder.card.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                pos = position ;
                notifyDataSetChanged();
                packagesFragment.price = horizontalList.get(position).price;
                packagesFragment.startSDK();
            }
        });

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

}
