package share.fawid.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import share.fawid.Models.Notification;
import share.fawid.R;


/**
 * Created by سيد on 04/06/2017.
 */
public class Notificationsadapter extends RecyclerView.Adapter<Notificationsadapter.MyViewHolder> {

    private List<Notification.DataBean> horizontalList;
    Context context;
    PopupMenu popup;
    Dialog dialog;
    String xx = "";
    public int pos = -1;
    Fragment fragment;
    int type = 0;
   
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout lin;
        private TextView date;
        private TextView des;


        public MyViewHolder(View view) {
            super(view);
            lin = view.findViewById(R.id.lin);
            date = view.findViewById(R.id.date);
            des = view.findViewById(R.id.des);
        }
    }


    public Notificationsadapter(List<Notification.DataBean> horizontalList) {
        this.horizontalList = horizontalList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_not, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.des.setText(horizontalList.get(position).text);
        holder.date.setText(horizontalList.get(position).created.substring(0,horizontalList.get(position).created.indexOf(" ")) );
       if (position%2 == 0){
           holder.lin.setBackgroundColor(Color.parseColor("#ffffff"));
       }else {
           holder.lin.setBackgroundColor(Color.parseColor("#F2F6F6"));
       }

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

}
