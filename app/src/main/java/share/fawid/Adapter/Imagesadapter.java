package share.fawid.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import share.fawid.Activity.ImageActivity;
import share.fawid.Helper.BitmapTransform;
import share.fawid.Models.Matgerprofile;
import share.fawid.R;


/**
 * Created by سيد on 04/06/2017.
 */
public class Imagesadapter extends RecyclerView.Adapter<Imagesadapter.MyViewHolder> {

    private List<Matgerprofile.DataBean.ImagesBean> horizontalList;
    Context context;
    PopupMenu popup;
    Dialog dialog;
    String xx = "";
    public int pos = -1;
    Activity activity;
    int type = 0;
    private static final int MAX_WIDTH = 1024;
    private static final int MAX_HEIGHT = 768;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        public MyViewHolder(View view) {
            super(view);
            img = view.findViewById(R.id.img);
        }
    }


    public Imagesadapter(List<Matgerprofile.DataBean.ImagesBean> horizontalList ) {
        this.horizontalList = horizontalList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_img_gallary, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));
       try {
           Picasso.get().load(horizontalList.get(position).image).transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                   .resize(size, size)
                   .centerInside().into(holder.img);

       }catch (Exception e){

       }
       holder.img.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent i = new Intent(context, ImageActivity.class);
               i.putExtra("img",horizontalList.get(position).image);
               context.startActivity(i);
           }
       });
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }


}
