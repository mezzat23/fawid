package share.fawid.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import share.fawid.Api.APIModel;
import share.fawid.Fragment.Provider.InfoFragment;
import share.fawid.Models.Addimage;
import share.fawid.R;


/**
 * Created by سيد on 04/06/2017.
 */
public class Imageadd1adapter extends RecyclerView.Adapter<Imageadd1adapter.MyViewHolder> {

    private List<Addimage> horizontalList;
    Context context;
    PopupMenu popup;
    Dialog dialog;
    String xx = "";
    public int pos = -1;
    Fragment activity;
    int type = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private ImageView add;
        private CardView card;
        private ImageView end;

        public MyViewHolder(View view) {
            super(view);
            img = view.findViewById(R.id.img);
            add = view.findViewById(R.id.add);
            card = view.findViewById(R.id.card);
            end = view.findViewById(R.id.end);
        }
    }


    public Imageadd1adapter(List<Addimage> horizontalList, Fragment activity) {
        this.horizontalList = horizontalList;
        this.activity = activity;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_image, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if (horizontalList.get(position).check) {
            if (horizontalList.get(position).id > 0) {
                Picasso.get().load(horizontalList.get(position).image).into(holder.img);
            } else {
                holder.img.setImageBitmap(horizontalList.get(position).bitmap);
            }
            holder.add.setVisibility(View.GONE);
            holder.card.setVisibility(View.VISIBLE);
            holder.end.setVisibility(View.VISIBLE);
        } else {
            holder.card.setVisibility(View.GONE);
            holder.add.setVisibility(View.VISIBLE);
            holder.end.setVisibility(View.GONE);
        }
        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity instanceof InfoFragment) {
                    ((InfoFragment) activity).getimage();
                }
            }
        });
        holder.end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity instanceof InfoFragment) {
                    if (horizontalList.get(position).id > 0) {
                        delete(horizontalList.get(position).id, position, (Activity) context);
                    } else {
                        ((InfoFragment) activity).imgs.remove(position);
                        horizontalList.remove(position);
                        notifyDataSetChanged();
                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

    private void delete(final int id, final int pos, Activity fragment) {
        APIModel.deleteMethod(fragment, "editprovider/" + id, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                ((InfoFragment) activity).imgs.remove(pos);
                horizontalList.remove(pos);
                notifyDataSetChanged();
            }
        });
    }


}
