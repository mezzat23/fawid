package share.fawid.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import share.fawid.Models.Wallet;
import share.fawid.R;


/**
 * Created by سيد on 04/06/2017.
 */
public class Walletadapter extends RecyclerView.Adapter<Walletadapter.MyViewHolder> {

    private List<Wallet.DataBean.ShipingBean> horizontalList;
    Context context;
    PopupMenu popup;
    Dialog dialog;
    String xx = "";
    public int pos = -1;
    Fragment fragment;
    int type = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView month;
        private TextView date;
        private TextView status;
        private TextView amount;

        public MyViewHolder(View view) {
            super(view);
            month = view.findViewById(R.id.month);
            date = view.findViewById(R.id.date);
            status = view.findViewById(R.id.status);
            amount = view.findViewById(R.id.amount);
        }
    }


    public Walletadapter(List<Wallet.DataBean.ShipingBean> horizontalList) {
        this.horizontalList = horizontalList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.month.setText(horizontalList.get(position).month);
        holder.date.setText(horizontalList.get(position).created.substring(0,horizontalList.get(position).created.indexOf(" ")) + "");
        holder.amount.setText(horizontalList.get(position).amount + " " + context.getString(R.string.sar));

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

}
