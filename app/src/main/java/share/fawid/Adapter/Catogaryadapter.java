package share.fawid.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import share.fawid.Activity.Customer.CatogaryActivity;
import share.fawid.Models.Home;
import share.fawid.R;


/**
 * Created by سيد on 04/06/2017.
 */
public class Catogaryadapter extends RecyclerView.Adapter<Catogaryadapter.MyViewHolder> {

    private List<Home.CategoriesBean> horizontalList;
    Context context;
    PopupMenu popup;
    Dialog dialog;
    String xx = "";
    public int pos = -1;
    Fragment fragment;
    int type = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout lin;
        private ImageView img;
        private TextView name;

        public MyViewHolder(View view) {
            super(view);
            lin = view.findViewById(R.id.lin);
            img = view.findViewById(R.id.img);
            name = view.findViewById(R.id.name);
        }
    }


    public Catogaryadapter(List<Home.CategoriesBean> horizontalList) {
        this.horizontalList = horizontalList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cat, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.name.setText(horizontalList.get(position).title);
        try {
            Picasso.get().load(horizontalList.get(position).image).into(holder.img);
        } catch (Exception e) {

        }
        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, CatogaryActivity.class);
                i.putExtra("id",horizontalList.get(position).id);
                i.putExtra("name",horizontalList.get(position).title);
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

}
