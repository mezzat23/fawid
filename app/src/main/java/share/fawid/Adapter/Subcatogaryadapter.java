package share.fawid.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import share.fawid.Models.Catogaries;
import share.fawid.R;


/**
 * Created by سيد on 04/06/2017.
 */
public class Subcatogaryadapter extends RecyclerView.Adapter<Subcatogaryadapter.MyViewHolder> {

    private List<Catogaries.DataBean.SubCatsBean> horizontalList;
    Context context;
    PopupMenu popup;
    Dialog dialog;
    String xx = "";
    public int pos = -1;
    Context fragment;
    int type = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout lin;
        private TextView name;
        private CheckBox check;

        public MyViewHolder(View view) {
            super(view);
            lin = view.findViewById(R.id.lin);
            name = view.findViewById(R.id.txt);
            check = view.findViewById(R.id.check);
        }
    }


    public Subcatogaryadapter(List<Catogaries.DataBean.SubCatsBean> horizontalList) {
        this.horizontalList = horizontalList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_city, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.name.setText(horizontalList.get(position).title);
        holder.check.setChecked(horizontalList.get(position).check);
        holder.check.setOnClickListener(onStateChangedListener(holder.check, position));

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

    private View.OnClickListener onStateChangedListener(final CheckBox radioButton, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horizontalList.get(position).check = !horizontalList.get(position).check;
                notifyDataSetChanged();

            }
        };
    }
}
