package share.fawid.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import share.fawid.Activity.WelcomeActivity;
import share.fawid.Models.Home;
import share.fawid.R;


/**
 * Created by سيد on 04/06/2017.
 */
public class Slideshomeadapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    ArrayList<Home.SlidersBean> slidesDataBeans = new ArrayList<>();
    private ImageView img;
    private TextView title;
    private TextView des;
    WelcomeActivity welcomeActivity;

    public Slideshomeadapter(Context context, ArrayList<Home.SlidersBean> slidesDataBeans ) {
        this.slidesDataBeans = slidesDataBeans;
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return slidesDataBeans.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }

    @Override
    public float getPageWidth(int position) {
        return 1f;
    }
//        public float getPageHeight (int position) {
//            return 0.105f;
//        }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_img, container, false);
        initView(itemView);
        try {
            Picasso.get().load(slidesDataBeans.get(position).image).into(img);
        } catch (Exception e) {
//            img.setBackgroundResource(R.drawable.noimage);
        }
        if (mContext.getString(R.string.lang).equals("ar")) {
            itemView.setRotationY(180);
        }

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    private void initView(View view) {
        img = view.findViewById(R.id.img);
    }

}
