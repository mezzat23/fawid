package share.fawid.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import share.fawid.Models.Catogaries;
import share.fawid.R;


/**
 * Created by سيد on 04/06/2017.
 */
public class Jobadapter extends RecyclerView.Adapter<Jobadapter.MyViewHolder> {

    private List<Catogaries.DataBean> horizontalList;
    Context context;
    PopupMenu popup;
    Dialog dialog;
    String xx = "";
    public int pos = -1;
    Fragment fragment;
    int type = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout lin;
        private TextView name;
        private ImageView arrow;
        private RecyclerView list;
        private CheckBox check;

        public MyViewHolder(View view) {
            super(view);
            lin = view.findViewById(R.id.lin);
            name = view.findViewById(R.id.name);
            arrow = view.findViewById(R.id.arrow);
            list = view.findViewById(R.id.list);
            check = view.findViewById(R.id.check);
        }
    }


    public Jobadapter(List<Catogaries.DataBean> horizontalList) {
        this.horizontalList = horizontalList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_job, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.name.setText(horizontalList.get(position).title);
        holder.check.setChecked(horizontalList.get(position).check);
        holder.check.setOnClickListener(onStateChangedListener(holder.check, position));
        LinearLayoutManager layoutManage1 = new LinearLayoutManager(context);
        holder.list.setLayoutManager(layoutManage1);
        Subcatogaryadapter subcatogaryadapter = new Subcatogaryadapter(horizontalList.get(position).sub_cats);
        holder.list.setAdapter(subcatogaryadapter);
//        if (horizontalList.get(position).sub_cats.size() == 0){
//            holder.check.setVisibility(View.VISIBLE);
//        }else {
//            holder.check.setVisibility(View.GONE);
//        }
        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.list.getVisibility() == View.GONE) {
                    holder.list.setVisibility(View.VISIBLE);
                    holder.arrow.setBackgroundResource(R.drawable.ic_baseline_keyboard_arrow_up_24);
                } else {
                    holder.list.setVisibility(View.GONE);
                    holder.arrow.setBackgroundResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }
    private View.OnClickListener onStateChangedListener(final CheckBox radioButton, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horizontalList.get(position).check = !horizontalList.get(position).check;
                notifyDataSetChanged();

            }
        };
    }
}
