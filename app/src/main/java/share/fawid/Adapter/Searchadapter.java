package share.fawid.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import share.fawid.Activity.Customer.DetailsActivity;
import share.fawid.Api.APIModel;
import share.fawid.Helper.Dialogs;
import share.fawid.Helper.Language;
import share.fawid.Helper.LoginSession;
import share.fawid.Models.Home;
import share.fawid.Models.Matgerprofile;
import share.fawid.R;


/**
 * Created by سيد on 04/06/2017.
 */
public class Searchadapter extends RecyclerView.Adapter<Searchadapter.MyViewHolder> {

    private List<Home.RecentlyBean> horizontalList;
    Context context;
    PopupMenu popup;
    Dialog dialog;
    String xx = "";
    public int pos = -1;
    Fragment fragment;
    int type = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private TextView ratnum;
        private TextView name;
        private TextView des;
        private CardView contact;
        private ImageView save;
        private LinearLayout lin;
        private ProgressBar progress;

        public MyViewHolder(View view) {
            super(view);
            img = view.findViewById(R.id.img);
            ratnum = view.findViewById(R.id.ratnum);
            name = view.findViewById(R.id.name);
            des = view.findViewById(R.id.des);
            contact = view.findViewById(R.id.contact);
            save = view.findViewById(R.id.whatsapp);
            lin = view.findViewById(R.id.lin);
            progress = view.findViewById(R.id.progress);
        }
    }


    public Searchadapter(List<Home.RecentlyBean> horizontalList) {
        this.horizontalList = horizontalList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sarch, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.name.setText(horizontalList.get(position).name);
        holder.ratnum.setText(horizontalList.get(position).rate + "");
        holder.des.setText(horizontalList.get(position).content);
        try {
            Picasso.get().load(horizontalList.get(position).image).into(holder.img);
        } catch (Exception e) {

        }
        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetailsActivity.class);
                i.putExtra("id", horizontalList.get(position).id);
                i.putExtra("name", horizontalList.get(position).name);
                i.putExtra("img", horizontalList.get(position).image);
                context.startActivity(i);
            }
        });
        holder.contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginSession.isLogin) {
                    getdata(context, holder.progress, position, 0);
                } else {
                    LoginSession.plsGoLogin((Activity) context);
                }
            }
        });
        holder.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginSession.isLogin) {
                    getdata(context, holder.progress, position, 1);
                } else {
                    LoginSession.plsGoLogin((Activity) context);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

    private void getdata(final Context context, final ProgressBar progress, final int pos, final int type) {
        String url = "pprofile/" + horizontalList.get(pos).id;
        APIModel.getMethod((Activity) context, url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Type dataType = new TypeToken<Matgerprofile>() {
                }.getType();
                Matgerprofile data = new Gson().fromJson(responseString, dataType);
                if (type == 0) {

                    Language language = new Language();
                    language.contactdialog(context, data);
                } else {
                    try {
                        if (!data.data.get(0).whatsapp.equals(null) && !data.data.get(0).whatsapp.equals("")) {

                            if (!data.data.get(0).whatsapp.startsWith("http://") && !data.data.get(0).whatsapp.startsWith("https://")) {
                                String url = "http://" + data.data.get(0).whatsapp;
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                context.startActivity(browserIntent);
                            } else {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data.data.get(0).whatsapp));
                                context.startActivity(browserIntent);
                            }
                            Language language = new Language();
                            language.onclick(context, "whatsapp", horizontalList.get(pos).id);
                        } else {
                            Dialogs.showToast(context.getString(R.string.not_available), context);
                        }
                    } catch (Exception e) {
                        Dialogs.showToast(context.getString(R.string.not_available), context);
                    }
                }
            }
        });
    }
}
