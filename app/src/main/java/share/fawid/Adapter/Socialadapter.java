package share.fawid.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import share.fawid.Api.APIModel;
import share.fawid.Helper.Language;
import share.fawid.Models.Social;
import share.fawid.R;


/**
 * Created by سيد on 04/06/2017.
 */
public class Socialadapter extends RecyclerView.Adapter<Socialadapter.MyViewHolder> {

    private List<Social> horizontalList;
    Context context;
    PopupMenu popup;
    Dialog dialog;
    String xx = "";
    public int pos = -1;
    Fragment fragment;
    int type = 0;
    Language language ;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout lin;
        private ImageView img;

        public MyViewHolder(View view) {
            super(view);
            lin = view.findViewById(R.id.lin);
            img = view.findViewById(R.id.img);
        }
    }


    public Socialadapter(List<Social> horizontalList , Dialog dialog , Context context) {
        this.horizontalList = horizontalList;
        this.dialog = dialog ;
        this.context = context ;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contact, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
       holder.img.setImageResource(horizontalList.get(position).img);
holder.lin.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
       if (horizontalList.get(position).type.equals("mobile")){
           Intent intent = new Intent(Intent.ACTION_DIAL);
           intent.setData(Uri.parse("tel:" + horizontalList.get(position).url));
           context.startActivity(intent);
       }else if (horizontalList.get(position).type.equals("email")){
           Intent i = new Intent(Intent.ACTION_SEND);
           i.setType("message/rfc822");
           i.putExtra(Intent.EXTRA_EMAIL, new String[]{horizontalList.get(position).url});
           try {
               context.startActivity(Intent.createChooser(i, "Send mail..."));
           } catch (ActivityNotFoundException ex) {
           }
       }else {
           if (!horizontalList.get(position).url.startsWith("http://") && !horizontalList.get(position).url.startsWith("https://")) {
               String url = "http://" + horizontalList.get(position).url;
               Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
               context.startActivity(browserIntent);
           } else {
               Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(horizontalList.get(position).url));
               context.startActivity(browserIntent);
           }
       }
       onclick((Activity) context,position);
       dialog.dismiss();
    }
});
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }
    public void onclick(final Activity context, final int pos) {
        RequestParams requestParams = new RequestParams();
        requestParams.put("user_id", horizontalList.get(pos).member_id);
        requestParams.put("provider_id", horizontalList.get(pos).id);
        requestParams.put("type", horizontalList.get(pos).type);
        APIModel.postMethod((Activity) context, "clicks", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

            }
        });
    }
}
