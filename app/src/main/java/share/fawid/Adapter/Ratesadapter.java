package share.fawid.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import share.fawid.Models.Rates;
import share.fawid.R;


/**
 * Created by سيد on 04/06/2017.
 */
public class Ratesadapter extends RecyclerView.Adapter<Ratesadapter.MyViewHolder> {

    private List<Rates.DataBean.CommentsBean> horizontalList;
    Context context;
    PopupMenu popup;
    Dialog dialog;
    String xx = "";
    public int pos = -1;
    Fragment fragment;
    int type = 0;
  
    public class MyViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout lin;
        private TextView ratnum;
        private TextView name;
        private TextView des;

        public MyViewHolder(View view) {
            super(view);
            lin = view.findViewById(R.id.lin);
            ratnum = view.findViewById(R.id.ratnum);
            name = view.findViewById(R.id.name);
            des = view.findViewById(R.id.des);
        }
    }


    public Ratesadapter(List<Rates.DataBean.CommentsBean> horizontalList) {
        this.horizontalList = horizontalList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rate, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.name.setText(horizontalList.get(position).created);
        holder.des.setText(horizontalList.get(position).comment);
        holder.ratnum.setText(horizontalList.get(position).rate);

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

}
