package share.fawid.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import share.fawid.Activity.Provider.Registerprovider1Activity;
import share.fawid.Models.Addimage;
import share.fawid.R;


/**
 * Created by سيد on 04/06/2017.
 */
public class Imageaddadapter extends RecyclerView.Adapter<Imageaddadapter.MyViewHolder> {

    private List<Addimage> horizontalList;
    Context context;
    PopupMenu popup;
    Dialog dialog;
    String xx = "";
    public int pos = -1;
    Activity activity;
    int type = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img;
        private ImageView add;
        private CardView card;
        private ImageView end;

        public MyViewHolder(View view) {
            super(view);
            img = view.findViewById(R.id.img);
            add = view.findViewById(R.id.add);
            card = view.findViewById(R.id.card);
            end = view.findViewById(R.id.end);
        }
    }


    public Imageaddadapter(List<Addimage> horizontalList, Activity activity) {
        this.horizontalList = horizontalList;
        this.activity = activity;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_image, parent, false);
        context = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if (horizontalList.get(position).check) {
            holder.img.setImageBitmap(horizontalList.get(position).bitmap);
            holder.add.setVisibility(View.GONE);
            holder.card.setVisibility(View.VISIBLE);
            holder.end.setVisibility(View.VISIBLE);
        } else {
            holder.card.setVisibility(View.GONE);
            holder.add.setVisibility(View.VISIBLE);
            holder.end.setVisibility(View.GONE);
        }
        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity instanceof Registerprovider1Activity) {
                    ((Registerprovider1Activity) activity).getimage();
                }
            }
        });
        holder.end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity instanceof Registerprovider1Activity) {
                    ((Registerprovider1Activity) activity).imgs.remove(position);
                    horizontalList.remove(position);
                    notifyDataSetChanged();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }


}
